package com.taboor.qms.admin.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.response.GetServiceSectorListResponse;
import com.taboor.qms.admin.panel.service.ServiceSectorService;

@RestController
@RequestMapping("/serviceSector")
@CrossOrigin(origins = "*")
public class ServiceSectorController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceSectorController.class);

	@Autowired
	ServiceSectorService serviceSectorService;

	@GetMapping("/get/all")
	public @ResponseBody GetServiceSectorListResponse getAll() throws Exception, Throwable {
		logger.info("Calling Get All Service Sectors - API");
		return serviceSectorService.getAllServiceSectors();
	}

}
