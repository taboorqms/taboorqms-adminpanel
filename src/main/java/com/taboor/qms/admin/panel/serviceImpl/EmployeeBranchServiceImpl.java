package com.taboor.qms.admin.panel.serviceImpl;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.service.EmpoyeeBranchService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceCenterEmpBranchMapper;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;

@Service
public class EmployeeBranchServiceImpl implements EmpoyeeBranchService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EmployeeBranchServiceImpl.class);

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public ServiceCenterEmpBranchMapper saveEmployeeBranchMapper(ServiceCenterEmpBranchMapper userBranchMapper)
			throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/employeebranchmapper/save";
		HttpEntity<ServiceCenterEmpBranchMapper> userBranchMapperEntity = new HttpEntity<>(userBranchMapper,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<ServiceCenterEmpBranchMapper> createUserBranchMapperResponse = restTemplate.postForEntity(uri,
				userBranchMapperEntity, ServiceCenterEmpBranchMapper.class);

		if (!createUserBranchMapperResponse.getStatusCode().equals(HttpStatus.OK)
				|| createUserBranchMapperResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.EMPLOYEEBRANCHMAPPER_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.EMPLOYEEBRANCHMAPPER_NOT_CREATED.getErrorDetails());

		return createUserBranchMapperResponse.getBody();
	}

	@Override
	public List<Branch> getBranchByEmp(Long employeeId) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/employeebranchmapper/getBranch/serviceCenterEmp?employeeId="
				+ employeeId;
		ResponseEntity<?> getBranchListResponse = restTemplate.getForEntity(uri, List.class);

		List<Branch> branchList = GenericMapper.convertListToObject(getBranchListResponse.getBody(),
				new TypeReference<List<Branch>>() {
				});
		return branchList;
	}

	@Override
	public List<ServiceCenterEmpBranchMapper> saveEmployeeBranchMapperList(
			List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMapperList, Boolean update)
			throws TaboorQMSServiceException, Exception {

		String uri;
		if (update.equals(true))
			uri = dbConnectorMicroserviceURL + "/tab/employeebranchmapper/updateAll";
		else
			uri = dbConnectorMicroserviceURL + "/tab/employeebranchmapper/saveAll";
		RestUtil.postRequest(uri,
				new HttpEntity<>(serviceCenterEmpBranchMapperList, HttpHeadersUtils.getApplicationJsonHeader()),
				List.class, xyzErrorMessage.EMPLOYEEBRANCHMAPPER_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.EMPLOYEEBRANCHMAPPER_NOT_CREATED.getErrorDetails());

		return null;
	}

	@Override
	public int countBranchByEmp(Long employeeId) throws TaboorQMSServiceException, Exception {
		return RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/employeebranchmapper/countBranch/serviceCenterEmp?employeeId="
						+ employeeId,
				Integer.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
	}

}
