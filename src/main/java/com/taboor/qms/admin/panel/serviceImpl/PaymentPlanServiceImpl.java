package com.taboor.qms.admin.panel.serviceImpl;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.service.PaymentPlanService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.PaymentPlan;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetPaymentPlanListResponse;
import com.taboor.qms.core.response.GetPaymentPlanListResponse.PaymentPlanObject;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;

@Service
public class PaymentPlanServiceImpl implements PaymentPlanService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public GetPaymentPlanListResponse getAllPaymentPlans() throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/paymentplans/get/all";
		ResponseEntity<?> getPaymentPlansResponse = restTemplate.getForEntity(uri, List.class);

		List<PaymentPlanObject> paymentPlanList = GenericMapper.convertListToObject(getPaymentPlansResponse.getBody(),
				new TypeReference<List<PaymentPlanObject>>() {
				});

		return new GetPaymentPlanListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.PAYMENT_PLAN_FETCHED.getMessage(), paymentPlanList);
	}

	@Override
	public GenStatusResponse addOrUpdatePaymentPlan(PaymentPlan paymentPlan)
			throws TaboorQMSServiceException, Exception {
		if (paymentPlan.getPaymentPlanId() == null) // New Plan
			paymentPlan.setCreatedTime(OffsetDateTime.now());
		else { // Update Plan
		}
		paymentPlan = savePaymentPlan(paymentPlan);
		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.PAYMENT_PLAN_ADDED.getMessage());
	}

	private PaymentPlan savePaymentPlan(PaymentPlan paymentPlan) throws TaboorQMSServiceException {
		String uri = dbConnectorMicroserviceURL + "/tab/paymentplans/save";
		HttpEntity<PaymentPlan> updateUserEntity = new HttpEntity<>(paymentPlan,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<PaymentPlan> savePaymentPlanResponse = restTemplate.postForEntity(uri, updateUserEntity,
				PaymentPlan.class);
		if (!savePaymentPlanResponse.getStatusCode().equals(HttpStatus.OK) || savePaymentPlanResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.PAYMENTPLAN_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.PAYMENTPLAN_NOT_CREATED.getErrorDetails());
		return savePaymentPlanResponse.getBody();
	}

	@Override
	public GetPaymentPlanListResponse getListing() throws TaboorQMSServiceException, Exception {

		List<?> resposne = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/paymentplans/getListing", List.class,
				xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorDetails());

		List<PaymentPlanObject> paymentPlanList = GenericMapper.convertListToObject(resposne,
				new TypeReference<List<PaymentPlanObject>>() {
				});

		return new GetPaymentPlanListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.PAYMENT_PLAN_FETCHED.getMessage(), paymentPlanList);
	}

	@Override
	public GetPaymentPlanListResponse getPlanDetails(Long paymentPlanId) throws TaboorQMSServiceException, Exception {
		PaymentPlanObject paymentPlan = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/paymentplans/getDetails?paymentPlanId=" + paymentPlanId,
				PaymentPlanObject.class, xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorDetails());

		return new GetPaymentPlanListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.PAYMENT_PLAN_FETCHED.getMessage(), Arrays.asList(paymentPlan));
	}

	@Override
	public GenStatusResponse deletePaymentPlan(long paymentPlanId) throws TaboorQMSServiceException, Exception {
		int response = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/paymentplans/delete/id?paymentPlanId=" + paymentPlanId,
				Integer.class, xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorDetails());

		if (response != xyzResponseCode.SUCCESS.getCode())
			throw new TaboorQMSServiceException(xyzErrorMessage.PAYMENTPLAN_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.PAYMENTPLAN_NOT_DELETED.getErrorDetails());

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.PAYMENT_PLAN_DELETED.getMessage());
	}

}
