package com.taboor.qms.admin.panel.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.ServiceTAB;

@Service
public interface ServiceTABService {

	public ServiceTAB getServiceTAB(@Valid Long serviceId) throws TaboorQMSServiceException, Exception;

	public ServiceTAB saveServiceTAB(@Valid ServiceTAB serviceTAB) throws TaboorQMSServiceException, Exception;

}
