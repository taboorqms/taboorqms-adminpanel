package com.taboor.qms.admin.panel.serviceImpl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.payload.AddPaymentMethodPayload;
import com.taboor.qms.admin.panel.payload.AddServicePayload;
import com.taboor.qms.admin.panel.payload.AddSupportTicketPayload;
import com.taboor.qms.admin.panel.payload.EasyPaymentObject;
import com.taboor.qms.admin.panel.payload.PaySubscriptionChargesPayload;
import com.taboor.qms.admin.panel.payload.RegisterServiceCenterPayload;
import com.taboor.qms.admin.panel.payload.UpdatePaymentMethodPayload;
import com.taboor.qms.admin.panel.payload.UpdateServiceCenterPayload;
import com.taboor.qms.admin.panel.payload.UpdateSubscriptionPlanPayload;
import com.taboor.qms.admin.panel.payload.UpdateSupportTicketStatusPayload;
import com.taboor.qms.admin.panel.response.GetServiceCenterConfigurationResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterProfileResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterProfileResponse.AverageServiceTimeList;
import com.taboor.qms.admin.panel.response.GetServiceCenterServiceListResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterSubscriptionListResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterSubscriptionResponse;
import com.taboor.qms.admin.panel.response.GetSupportTicketListResponse;
import com.taboor.qms.admin.panel.service.EmpoyeeBranchService;
import com.taboor.qms.admin.panel.service.InvoiceService;
import com.taboor.qms.admin.panel.service.RequiredDocumentService;
import com.taboor.qms.admin.panel.service.ServiceCenterEmpService;
import com.taboor.qms.admin.panel.service.ServiceCenterService;
import com.taboor.qms.admin.panel.service.ServiceTABService;
import com.taboor.qms.admin.panel.service.TaboorBankAccountService;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.model.PaymentMethod;
import com.taboor.qms.core.model.PaymentPlan;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.ServiceCenterServiceTABMapper;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.SupportTicket;
import com.taboor.qms.core.model.TaboorBankAccount;
import com.taboor.qms.core.model.TaboorEmployee;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.payload.AddUserBankCardPayload;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetPaymentPlanListResponse;
import com.taboor.qms.core.response.GetServiceCentreListResponse;
import com.taboor.qms.core.response.GetServiceCentreListResponse.ServiceCentreResponse;
import com.taboor.qms.core.response.GetServiceCentreListingResponse;
import com.taboor.qms.core.response.GetServiceCentreListingResponse.ServiceCenterObject;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.core.response.GetStatisticsResponse.ValueObject;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.core.utils.FileDataObject;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.InvoiceType;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.PlanSubscriptionType;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.core.utils.ServiceCenterPaymentStatus;
import com.taboor.qms.core.utils.StatusHistoryObject;
import com.taboor.qms.core.utils.SubscriptionStatus;
import com.taboor.qms.core.utils.SupportTicketStatus;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import net.bytebuddy.utility.RandomString;

@Service
public class ServiceCenterServiceImpl implements ServiceCenterService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    @Value("${url.microservice.user.management}")
    private String userManagementMicroserviceURL;

    @Autowired
    ServiceCenterEmpService serviceCenterEmpService;

    @Autowired
    ServiceTABService serviceTABService;

    @Autowired
    EmpoyeeBranchService employeeBranchService;

    @Autowired
    RequiredDocumentService requiredDocumentService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    TaboorBankAccountService taboorBankAccountService;

    @Autowired
    private FileDataService fileDataService;

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public ServiceCenter saveServiceCenter(ServiceCenter serviceCenter) throws TaboorQMSServiceException {

//		HttpEntity<ServiceCenter> serviceCenterEntity = new HttpEntity<>(serviceCenter,
//				HttpHeadersUtils.getApplicationJsonHeader());
//		ResponseEntity<ServiceCenter> createServiceCenterResponse = restTemplate.postForEntity(uri, serviceCenterEntity,
//				ServiceCenter.class);
//
//		if (!createServiceCenterResponse.getStatusCode().equals(HttpStatus.OK)
//				|| createServiceCenterResponse.getBody() == null)
//			throw new TaboorQMSServiceException(xyzErrorMessage.SERVICECENTER_NOT_CREATED.getErrorCode() + "::"
//					+ xyzErrorMessage.SERVICECENTER_NOT_CREATED.getErrorDetails());
        serviceCenter = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/servicecenters/save",
                new HttpEntity<>(serviceCenter, HttpHeadersUtils.getApplicationJsonHeader()), ServiceCenter.class,
                xyzErrorMessage.SERVICECENTER_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTER_NOT_CREATED.getErrorDetails());

        return serviceCenter;
    }

    @Override
    public GetValuesResposne registerServiceCenter(@Valid RegisterServiceCenterPayload registerServiceCenterPayload,
            MultipartFile profileImage) throws TaboorQMSServiceException, Exception {

        Boolean manualRegistration;

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            manualRegistration = false;
        } else {
            manualRegistration = true;
        }

        if (profileImage != null) {
            if (!profileImage.isEmpty()) {
                if (profileImage.getSize() > 256 * 1000) {
                    throw new TaboorQMSServiceException(404 + "::" + "Profile Image is size limit exceeds.");
                }
            }
        }

        UserRole getUserRoleResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + RoleName.Service_Center_Admin,
                UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());

        PaymentPlan getPaymentPlanResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/paymentplans/get/id?paymentPlanId="
                + registerServiceCenterPayload.getPaymentPlanId(),
                PaymentPlan.class, xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.PAYMENT_PLAN_NOT_EXISTS.getErrorDetails());

        RegisterUserPayload registerUserPayload = new RegisterUserPayload();
        registerUserPayload.setName(registerServiceCenterPayload.getName());
        registerUserPayload.setEmail(registerServiceCenterPayload.getEmail());
        registerUserPayload.setPassword(registerServiceCenterPayload.getPassword());
        registerUserPayload.setPhoneNumber(registerServiceCenterPayload.getPhoneNumber());
        registerUserPayload.setUserRole(getUserRoleResponse);
        registerUserPayload.setAssignRolePrivileges(true);

        ServiceCenter serviceCenter = new ServiceCenter();
        serviceCenter.setLogoData(new String("").getBytes());

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("RegisterUserPayload", registerUserPayload);
        if (profileImage != null) {
            if (!profileImage.isEmpty()) {
                body.add("UserProfileImage", profileImage.getResource());
                serviceCenter
                        .setLogoData(Base64.getEncoder().encode(IOUtils.toByteArray(profileImage.getInputStream())));
            }
        }

        RegisterUserResponse registerUserResponse = RestUtil.postRequest(
                userManagementMicroserviceURL + "/user/register",
                new HttpEntity<>(body, HttpHeadersUtils.getMultipartFormDataHeader()), RegisterUserResponse.class,
                xyzErrorMessage.USER_NOT_REGISTERED.getErrorCode(),
                xyzErrorMessage.USER_NOT_REGISTERED.getErrorDetails());

        if (registerUserResponse.getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
            throw new TaboorQMSServiceException(
                    xyzErrorMessage.USER_EXISTS.getErrorCode() + "::" + xyzErrorMessage.USER_EXISTS.getErrorDetails());
        }

        serviceCenter.setCountry(registerServiceCenterPayload.getCountry());
        serviceCenter.setServiceCenterName(registerServiceCenterPayload.getName());
        serviceCenter.setServiceCenterArabicName(registerServiceCenterPayload.getArabicName());
        serviceCenter.setServiceSector(registerServiceCenterPayload.getServiceSector());
        serviceCenter.setPhoneNumber(registerServiceCenterPayload.getPhoneNumber());
        serviceCenter.setCreatedTime(OffsetDateTime.now().withNano(0));
//		serviceCenter.setImageUrl(registerUserResponse.getUser().getProfileImageUrl());
        serviceCenter.setEmail(registerUserResponse.getUser().getEmail());

        serviceCenter = saveServiceCenter(serviceCenter);

        ServiceCenterConfiguration serviceCenterConfiguration = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/default",
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorDetails());

        serviceCenterConfiguration.setConfigurationId(null);
        serviceCenterConfiguration.setServiceCenter(serviceCenter);

        serviceCenterConfiguration = RestUtil.postRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/save",
                new HttpEntity<>(serviceCenterConfiguration, HttpHeadersUtils.getApplicationJsonHeader()),
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService.createEmp(registerUserResponse.getUser(),
                serviceCenter, new ArrayList<Long>());

        ServiceCenterSubscription serviceCenterSubscription = new ServiceCenterSubscription();
        serviceCenterSubscription.setBankCard(null);
        serviceCenterSubscription.setChargesPaid(0.0);
        serviceCenterSubscription.setCurrency(registerServiceCenterPayload.getCurrency());
        serviceCenterSubscription.setCreatedOn(OffsetDateTime.now().withNano(0));
        serviceCenterSubscription.setNoOfBranches(registerServiceCenterPayload.getNoOfBranch());
        serviceCenterSubscription.setNoOfSMS(getPaymentPlanResponse.getNoOfSMS());
        serviceCenterSubscription.setNoOfUsersPerBranch(getPaymentPlanResponse.getNoOfUserPerBranch());
        serviceCenterSubscription.setPaymentPlan(getPaymentPlanResponse);
        serviceCenterSubscription.setServiceCenter(serviceCenter);
        serviceCenterSubscription.setSubscriptionStatus(SubscriptionStatus.ACTIVE);
        serviceCenterSubscription.setUpdatedOn(OffsetDateTime.now().withNano(0));

        serviceCenterSubscription.setChargesTotal(registerServiceCenterPayload.getTotalAmount());

        TaboorBankAccount taboorBankAccount = taboorBankAccountService.getDefaultAccount();

        if (manualRegistration) { // Manual Registration
            if (registerServiceCenterPayload.getPlanSubscriptionType() == PlanSubscriptionType.MONTHLY.getStatus()) {
                serviceCenterSubscription.setPlanSubscriptionType(PlanSubscriptionType.MONTHLY);
                serviceCenterSubscription.setNoOfInstallments(0);
                serviceCenterSubscription.setNoOfInvoices(1);
                serviceCenterSubscription.setExpiredOn(OffsetDateTime.now().withNano(0).plusMonths(1));
                serviceCenterSubscription = saveServiceCenterSubscription(serviceCenterSubscription);

                invoiceService.createAutoInvoice(serviceCenterSubscription.getChargesTotal(),
                        registerServiceCenterPayload.getCurrency(), OffsetDateTime.now().withNano(0),
                        OffsetDateTime.now().withNano(0).plusDays(7), OffsetDateTime.now().withNano(0).plusDays(10),
                        serviceCenterSubscription, taboorBankAccount, InvoiceType.MANUAL);
            } else {
                serviceCenterSubscription.setPlanSubscriptionType(PlanSubscriptionType.ANNUALY);
                serviceCenterSubscription.setExpiredOn(OffsetDateTime.now().withNano(0).plusYears(1));

                if (registerServiceCenterPayload.getEasyPayments() == null) {
                    registerServiceCenterPayload.setEasyPayments(new ArrayList<EasyPaymentObject>());
                }
                if (registerServiceCenterPayload.getEasyPayments().isEmpty()) {
                    serviceCenterSubscription.setNoOfInstallments(0);
                    serviceCenterSubscription.setNoOfInvoices(1);
                    serviceCenterSubscription = saveServiceCenterSubscription(serviceCenterSubscription);

                    invoiceService.createAutoInvoice(serviceCenterSubscription.getChargesTotal(),
                            registerServiceCenterPayload.getCurrency(), OffsetDateTime.now().withNano(0),
                            OffsetDateTime.now().withNano(0).plusDays(7), OffsetDateTime.now().withNano(0).plusDays(10),
                            serviceCenterSubscription, taboorBankAccount, InvoiceType.MANUAL);

                } else { // Easy Payment
                    serviceCenterSubscription
                            .setNoOfInstallments(registerServiceCenterPayload.getEasyPayments().size());
                    serviceCenterSubscription.setNoOfInvoices(registerServiceCenterPayload.getEasyPayments().size());
                    serviceCenterSubscription = saveServiceCenterSubscription(serviceCenterSubscription);

                    for (EasyPaymentObject item : registerServiceCenterPayload.getEasyPayments()) {
                        invoiceService.createAutoInvoice(item.getAmountValue(), item.getCurrency(),
                                OffsetDateTime.now().withNano(0), item.getDueDate(),
                                item.getDueDate().plusDays(item.getGraceDays()), serviceCenterSubscription,
                                taboorBankAccount, InvoiceType.MANUAL);
                    }
                }
            }
        } else { // Normal Registration

            if (registerServiceCenterPayload.getPlanSubscriptionType() == PlanSubscriptionType.DEMO.getStatus()) {
                serviceCenterSubscription.setPlanSubscriptionType(PlanSubscriptionType.DEMO);
                serviceCenterSubscription.setNoOfInstallments(0);
                serviceCenterSubscription.setNoOfInvoices(0);
                serviceCenterSubscription.setExpiredOn(OffsetDateTime.now().withNano(0).plusMonths(1));
                serviceCenterSubscription = saveServiceCenterSubscription(serviceCenterSubscription);
            } else if (registerServiceCenterPayload.getPlanSubscriptionType() == PlanSubscriptionType.MONTHLY
                    .getStatus()) {
                serviceCenterSubscription.setPlanSubscriptionType(PlanSubscriptionType.MONTHLY);
                serviceCenterSubscription.setNoOfInstallments(0);
                serviceCenterSubscription.setNoOfInvoices(1);
                serviceCenterSubscription.setExpiredOn(OffsetDateTime.now().withNano(0).plusMonths(1));
                serviceCenterSubscription = saveServiceCenterSubscription(serviceCenterSubscription);

                invoiceService.createAutoInvoice(serviceCenterSubscription.getChargesTotal(),
                        registerServiceCenterPayload.getCurrency(), OffsetDateTime.now().withNano(0),
                        OffsetDateTime.now().withNano(0).plusDays(7), OffsetDateTime.now().withNano(0).plusDays(10),
                        serviceCenterSubscription, taboorBankAccount, InvoiceType.AUTO);

            } else {
                serviceCenterSubscription.setPlanSubscriptionType(PlanSubscriptionType.ANNUALY);
                serviceCenterSubscription.setExpiredOn(OffsetDateTime.now().withNano(0).plusYears(1));
                serviceCenterSubscription.setNoOfInstallments(0);
                serviceCenterSubscription.setNoOfInvoices(12);
                serviceCenterSubscription.setNextPaymentDueDate(OffsetDateTime.now().withNano(0).plusDays(7));
                serviceCenterSubscription = saveServiceCenterSubscription(serviceCenterSubscription);

                for (int i = 1; i <= 12; i++) {
                    invoiceService.createAutoInvoice(serviceCenterSubscription.getChargesTotal() / 12,
                            registerServiceCenterPayload.getCurrency(), OffsetDateTime.now().withNano(0).plusMonths(i),
                            OffsetDateTime.now().withNano(0).plusMonths(i).plusDays(7),
                            OffsetDateTime.now().withNano(0).plusMonths(i).plusDays(10), serviceCenterSubscription,
                            taboorBankAccount, InvoiceType.AUTO);
                }
            }
        }

        serviceCenter.setServiceCenterPaymentStatus(ServiceCenterPaymentStatus.TO_BE_PAID);
        serviceCenter = saveServiceCenter(serviceCenter);

//		List<ServiceCenterServiceTABMapper> list = new ArrayList<ServiceCenterServiceTABMapper>();
//		for (int i = 1; i < 5; i++) {
//			ServiceCenterServiceTABMapper mapper = new ServiceCenterServiceTABMapper();
//			ServiceTAB serviceTAB = new ServiceTAB();
//			serviceTAB.setServiceId((long) i);
//			mapper.setServiceTAB(serviceTAB);
//			mapper.setServiceCenter(serviceCenter);
//			list.add(mapper);
//		}
//		RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/servicecenterservicetabmapper/saveAll",
//				new HttpEntity<>(list, HttpHeadersUtils.getApplicationJsonHeader()), List.class,
//				xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorCode(),
//				xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorDetails());
        RSocketPayload payload = new RSocketPayload();
        payload.setNotificationSubject(RSocketNotificationSubject.NEW_SERVICE_CENTER);
        payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
        payload.setInformation(Arrays.asList(serviceCenter.getServiceCenterName()));

//		RSocketInitializer.fireAndForget(payload);
        RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("ServiceCenterId", String.valueOf(serviceCenter.getServiceCenterId()));
        values.put("ServiceCenterSubscriptionId", serviceCenterSubscription.getSubscriptionId());
        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_REGISTERED.getMessage(), values);
    }

    private ServiceCenterSubscription saveServiceCenterSubscription(ServiceCenterSubscription serviceCenterSubscription)
            throws TaboorQMSServiceException {
//		String uri = dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/save";
//		HttpEntity<ServiceCenterSubscription> serviceCenterSubscriptionEntity = new HttpEntity<>(
//				serviceCenterSubscription, HttpHeadersUtils.getApplicationJsonHeader());
//		ResponseEntity<ServiceCenterSubscription> createServiceCenterSubscriptionEntityResponse = restTemplate
//				.postForEntity(uri, serviceCenterSubscriptionEntity, ServiceCenterSubscription.class);
//
//		if (!createServiceCenterSubscriptionEntityResponse.getStatusCode().equals(HttpStatus.OK)
//				|| createServiceCenterSubscriptionEntityResponse.getBody() == null)
//			throw new TaboorQMSServiceException(xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorCode()
//					+ "::" + xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorDetails());

        serviceCenterSubscription = RestUtil.postRequest(
                dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/save",
                new HttpEntity<>(serviceCenterSubscription, HttpHeadersUtils.getApplicationJsonHeader()),
                ServiceCenterSubscription.class, xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorDetails());
        return serviceCenterSubscription;
    }

    @Override
    public GetPaymentPlanListResponse getSelectedPaymentPlan(Long serviceCenterId)
            throws TaboorQMSServiceException, Exception {
        getById(serviceCenterId);
        return new GetPaymentPlanListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.PAYMENT_PLAN_FETCHED.getMessage(), null);
    }

    private ServiceCenter getById(Long serviceCenterId) throws TaboorQMSServiceException {
        // From H2
        String uri = dbConnectorMicroserviceURL + "/tab/servicecenters/get/id?serviceCenterId=" + serviceCenterId;
        ResponseEntity<ServiceCenter> getServiceCenterResponse = restTemplate.getForEntity(uri, ServiceCenter.class);
        if (!getServiceCenterResponse.getStatusCode().equals(HttpStatus.OK)
                || getServiceCenterResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SERVICE_CENTER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.SERVICE_CENTER_NOT_EXISTS.getErrorDetails());
        }
        return getServiceCenterResponse.getBody();
    }

    @Override
    public GetServiceCenterServiceListResponse getProvidedServices() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());

        String uri = dbConnectorMicroserviceURL
                + "/tab/servicecenterservicetabmapper/getServices/serviceCenter?serviceCenterId="
                + serviceCenterEmployee.getServiceCenter().getServiceCenterId();
        ResponseEntity<?> getServicesResponse = restTemplate.getForEntity(uri, List.class);

        List<ServiceCenterServiceTABMapper> serviceList = GenericMapper.convertListToObject(
                getServicesResponse.getBody(), new TypeReference<List<ServiceCenterServiceTABMapper>>() {
        });

        for (ServiceCenterServiceTABMapper entity : serviceList) {
            entity.setServiceCenter(null);
        }

        return new GetServiceCenterServiceListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_FETCHED.getMessage(), serviceList);
    }

    @Override
    public GetServiceCenterProfileResponse getProfile(GetByIdPayload getByIdPayload)
            throws TaboorQMSServiceException, Exception {

        Long serviceCentreId = getByIdPayload.getId();
        ServiceCenter serviceCenter = null;
        if (serviceCentreId == 0) {
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            Session session = myUserDetails.getSession();
            ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                    .findByUserId(session.getUser().getUserId());
            serviceCentreId = serviceCenterEmployee.getServiceCenter().getServiceCenterId();
            serviceCenter = serviceCenterEmployee.getServiceCenter();
        } else {
            serviceCenter = getById(serviceCentreId);
        }

        GetServiceCenterProfileResponse response = new GetServiceCenterProfileResponse();
        response.setServiceCenter(serviceCenter);

        List<?> servicesTabList = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/servicecenterservicetabmapper/getServices/serviceCenter"
                + "?serviceCenterId=" + serviceCentreId,
                List.class);

        List<ServiceCenterServiceTABMapper> serviceList = GenericMapper.convertListToObject(servicesTabList,
                new TypeReference<List<ServiceCenterServiceTABMapper>>() {
        });

        if (serviceList == null) {
            response.setServicesList(new ArrayList<ServiceCenterServiceTABMapper>());
        } else {
            response.setServicesList(serviceList);
        }

        String uri = dbConnectorMicroserviceURL + "/tab/branches/count/serviceCenterId?serviceCenterId="
                + serviceCentreId;
        ResponseEntity<Integer> getIntegerResponse = restTemplate.getForEntity(uri, Integer.class);
        if (!getIntegerResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        response.setNoOfBranches(getIntegerResponse.getBody().intValue());

        uri = dbConnectorMicroserviceURL + "/tab/branches/countCity/serviceCenterId?serviceCenterId=" + serviceCentreId;
        getIntegerResponse = restTemplate.getForEntity(uri, Integer.class);
        if (!getIntegerResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        response.setNoOfCities(getIntegerResponse.getBody().intValue());

        uri = dbConnectorMicroserviceURL + "/tab/agents/count/serviceCenterId?serviceCenterId=" + serviceCentreId;
        getIntegerResponse = restTemplate.getForEntity(uri, Integer.class);
        if (!getIntegerResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        response.setNoOfAgents(getIntegerResponse.getBody().intValue());

        uri = dbConnectorMicroserviceURL + "/tab/servicecenteremployees/countEmp/serviceCenter?serviceCenterId="
                + serviceCentreId;
        getIntegerResponse = restTemplate.getForEntity(uri, Integer.class);
        if (!getIntegerResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        response.setNoOfUsers(getIntegerResponse.getBody().intValue() - response.getNoOfAgents());

        //////////////////
        List<?> getBranchesResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branches/get/serviceCenter?serviceCentreId="
                + serviceCentreId.longValue(),
                List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<Branch> branchList = GenericMapper.convertListToObject(getBranchesResponse,
                new TypeReference<List<Branch>>() {
        });

        List<AverageServiceTimeList> astList = new ArrayList<AverageServiceTimeList>();

        if (!branchList.isEmpty()) {

            List<Long> branchIdList = new ArrayList<Long>();
            for (Branch branch : branchList) {
                branchIdList.add(branch.getBranchId());
            }
            branchIdList = branchIdList.stream().distinct().collect(Collectors.toList());

            uri = dbConnectorMicroserviceURL + "/tab/queues/getQueues/branchList";
            HttpEntity<?> branchListEntity = new HttpEntity<>(branchIdList,
                    HttpHeadersUtils.getApplicationJsonHeader());
            ResponseEntity<?> getQueueListingResponse = restTemplate.postForEntity(uri, branchListEntity, List.class);

            List<Queue> queueListing = GenericMapper.convertListToObject(getQueueListingResponse.getBody(),
                    new TypeReference<List<Queue>>() {
            });

            for (ServiceCenterServiceTABMapper sctm : serviceList) {

                List<Queue> serviceQueueListing = new ArrayList<Queue>();

                for (Queue queue : queueListing) {
                    if (queue.getService().getServiceName().equals(sctm.getServiceTAB().getServiceName())) {
                        serviceQueueListing.add(queue);
                    }
                }

                double sumAverageServiceTime = 0, averageServiceTime = 0;
                int count = 0;

                for (Queue queue : serviceQueueListing) {
                    averageServiceTime += queue.getAverageServiceTime().getMinute()
                            + queue.getAverageServiceTime().getHour() * 60;
                    count++;
                }

                if (averageServiceTime != 0) {
                    sumAverageServiceTime = averageServiceTime / count;
                } else {
                    sumAverageServiceTime = 0;
                }

                AverageServiceTimeList ast = new AverageServiceTimeList();
                ast.setAst(sumAverageServiceTime);
                ast.setService(sctm.getServiceTAB());

                astList.add(ast);

            }

        }

        response.setAverageServiceTimeList(astList);
        response.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
        response.setApplicationStatusResponse(xyzResponseMessage.SERVICE_CENTER_FETCHED.getMessage());
        response.getServiceCenter().setLogoDataString(new String(serviceCenter.getLogoData()));
        serviceCenter.setLogoData(new String("").getBytes());

        return response;
    }

    @Override
    public GenStatusResponse deleteServiceCentre(long serviceCentreId) throws TaboorQMSServiceException, Exception {

        ServiceCenterSubscription serviceCenterSubscription = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/get/serviceCenterId?serviceCenterId="
                + serviceCentreId,
                ServiceCenterSubscription.class, xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorDetails());

        Integer branchCount = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branches/count/serviceCenterId?serviceCenterId=" + serviceCentreId,
                Integer.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        if (serviceCenterSubscription.getNoOfInvoices() > 0) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SERVICE_CENTER_INVOICES_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.SERVICE_CENTER_INVOICES_EXISTS.getErrorDetails());
        } else if (branchCount > 0) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SERVICE_CENTER_BRANCHES_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.SERVICE_CENTER_BRANCHES_EXISTS.getErrorDetails());
        } else {
            String uri = dbConnectorMicroserviceURL + "/tab/servicecenters/delete/id?serviceCentreId="
                    + serviceCentreId;
            ResponseEntity<Boolean> deleteServiceCenterResponse = restTemplate.getForEntity(uri, Boolean.class);

            if (!deleteServiceCenterResponse.getStatusCode().equals(HttpStatus.OK)
                    || deleteServiceCenterResponse.getBody().equals(false)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.SERVICECENTER_NOT_DELETED.getErrorCode() + "::"
                        + xyzErrorMessage.SERVICECENTER_NOT_DELETED.getErrorDetails());
            }
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICECENTER_DELETED.getMessage());
    }

    @Override
    public GenStatusResponse updateServiceCentre(UpdateServiceCenterPayload payload)
            throws TaboorQMSServiceException, Exception {

        ServiceCenter serviceCenter = getById(payload.getServiceCenterId());

        serviceCenter.setEmail(payload.getEmail());
        serviceCenter.setCountry(payload.getCountry());
        serviceCenter.setPhoneNumber(payload.getPhoneNumber());
        serviceCenter.setServiceCenterArabicName(payload.getServiceCenterArabicName());
        serviceCenter.setServiceCenterName(payload.getServiceCenterName());

        serviceCenter = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/servicecenters/save",
                new HttpEntity<>(serviceCenter, HttpHeadersUtils.getApplicationJsonHeader()), ServiceCenter.class,
                xyzErrorMessage.SERVICECENTER_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTER_NOT_CREATED.getErrorDetails());

        RSocketPayload payload2 = new RSocketPayload();
        payload2.setNotificationSubject(RSocketNotificationSubject.UPDATE_SERVICE_CENTER);
        payload2.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
        payload2.setInformation(Arrays.asList(serviceCenter.getServiceCenterName()));

//		RSocketInitializer.fireAndForget(payload2);
        RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                new HttpEntity<>(payload2, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICECENTER_UPDATED.getMessage());
    }

    @Override
    public GenStatusResponse deleteService(long serviceCenterServiceId) throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/servicecenterservicetabmapper/delete/id?serviceCenterServiceId="
                + serviceCenterServiceId;
        ResponseEntity<Boolean> deleteServiceResponse = restTemplate.getForEntity(uri, Boolean.class);

        if (!deleteServiceResponse.getStatusCode().equals(HttpStatus.OK)
                || deleteServiceResponse.getBody().equals(false)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SERVICETAB_NOT_DELETED.getErrorCode() + "::"
                    + xyzErrorMessage.SERVICETAB_NOT_DELETED.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICETAB_DELETED.getMessage());
    }

    @Override
    public GetServiceCenterConfigurationResponse getConfiguration(GetByIdPayload getByIdPayload)
            throws TaboorQMSServiceException, Exception {

        Long serviceCentreId = getByIdPayload.getId();
        if (serviceCentreId == 0) {
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            Session session = myUserDetails.getSession();
            ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                    .findByUserId(session.getUser().getUserId());
            serviceCentreId = serviceCenterEmployee.getServiceCenter().getServiceCenterId();
        }

        ServiceCenterConfiguration serviceCenterConfiguration = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
                + serviceCentreId,
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorDetails());

        return new GetServiceCenterConfigurationResponse(xyzResponseCode.SUCCESS.getCode(),
                "Service Center Configuration fetched Successfully", serviceCenterConfiguration);
    }

    @Override
    public GetServiceCenterConfigurationResponse assignDefaultConfiguration(int type, long serviceCenterId)
            throws TaboorQMSServiceException, Exception {

//		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
//				.getPrincipal();
//		Session session = myUserDetails.getSession();
//		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
//				.findByUserId(session.getUser().getUserId());
        ServiceCenterConfiguration defaultConfiguration = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/default",
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorDetails());

        ServiceCenterConfiguration serviceCenterConfiguration = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/get/serviceCenter?serviceCenterId="
                + serviceCenterId,
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_EXISTS.getErrorDetails());

        if (type == 0) {
            serviceCenterConfiguration.setCurrency(defaultConfiguration.getCurrency());
            serviceCenterConfiguration.setFastpassAmount(defaultConfiguration.getFastpassAmount());
            serviceCenterConfiguration.setTaboorShareAmount(defaultConfiguration.getTaboorShareAmount());
        } else if (type == 1) {
            serviceCenterConfiguration
                    .setFastpassStepoutAllowedCount(defaultConfiguration.getFastpassStepoutAllowedCount());
            serviceCenterConfiguration.setFastpassStepoutTime(defaultConfiguration.getFastpassStepoutTime());
            serviceCenterConfiguration
                    .setNormalStepoutAllowedCount(defaultConfiguration.getNormalStepoutAllowedCount());
            serviceCenterConfiguration.setNormalStepoutTime(defaultConfiguration.getNormalStepoutTime());
        }

        serviceCenterConfiguration = RestUtil.postRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/save",
                new HttpEntity<>(serviceCenterConfiguration, HttpHeadersUtils.getApplicationJsonHeader()),
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

//		serviceCenterConfiguration.setServiceCenter(null);
        return new GetServiceCenterConfigurationResponse(xyzResponseCode.SUCCESS.getCode(),
                "Service Center Default Configuration assigned Successfully", serviceCenterConfiguration);
    }

    @Override
    public GetServiceCenterConfigurationResponse updateConfiguration(
            ServiceCenterConfiguration serviceCenterConfiguration) throws TaboorQMSServiceException, Exception {
//		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
//				.getPrincipal();
//		Session session = myUserDetails.getSession();
//		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
//				.findByUserId(session.getUser().getUserId());

//		serviceCenterConfiguration.setServiceCenter(serviceCenterEmployee.getServiceCenter());
        serviceCenterConfiguration = RestUtil.postRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenterconfigurations/save",
                new HttpEntity<>(serviceCenterConfiguration, HttpHeadersUtils.getApplicationJsonHeader()),
                ServiceCenterConfiguration.class, xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERCONFIGURATION_NOT_CREATED.getErrorDetails());

//		serviceCenterConfiguration.setServiceCenter(null);
        return new GetServiceCenterConfigurationResponse(xyzResponseCode.SUCCESS.getCode(),
                "Service Center Configuration updated Successfully", serviceCenterConfiguration);
    }

    @Override
    public List<UserBankCard> getPaymentMethods(GetByIdPayload getByIdPayload)
            throws TaboorQMSServiceException, Exception {

        Long serviceCentreId = getByIdPayload.getId();
        if (serviceCentreId == 0) {
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            Session session = myUserDetails.getSession();
            ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                    .findByUserId(session.getUser().getUserId());
            serviceCentreId = serviceCenterEmployee.getServiceCenter().getServiceCenterId();
        }

        List<?> paymentMethods = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/userbankcards/get/serviceCenter?serviceCenterId=" + serviceCentreId,
                List.class, xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorDetails());

        List<UserBankCard> paymentMethodList = GenericMapper.convertListToObject(paymentMethods,
                new TypeReference<List<UserBankCard>>() {
        });

        return paymentMethodList;
    }

    @Override
    public GenStatusResponse addPaymentMethod(AddPaymentMethodPayload payload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        PaymentMethod paymentMethod = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/paymentmethods/get/id?paymentMethodId="
                + payload.getPaymentMethodId(),
                PaymentMethod.class, xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorDetails());

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        if (currentYear > payload.getExpiryYear() || (currentYear == payload.getExpiryYear() && currentMonth > payload.getExpiryMonth())) {
            throw new TaboorQMSServiceException(xyzErrorMessage.CARD_DATE_EXPIRED.getErrorCode() + "::"
                    + xyzErrorMessage.CARD_DATE_EXPIRED.getErrorDetails());
        }
        // add credit card validation
        boolean isCardNumberValid = isValid(Long.parseLong((payload.getCardNumber())));
        if (!isCardNumberValid) {
            throw new TaboorQMSServiceException(xyzErrorMessage.INVALID_CARD_NUMBER.getErrorCode() + "::"
                    + xyzErrorMessage.INVALID_CARD_NUMBER.getErrorDetails());
        }

        UserBankCard bankCard = new UserBankCard();
        bankCard.setCardNumber(payload.getCardNumber());
        bankCard.setCardTitle(payload.getCardTitle());
        bankCard.setCardType(payload.getCardType());
        bankCard.setCardCvv(payload.getCardCvv());
        bankCard.setExpiryMonth(payload.getExpiryMonth());
        bankCard.setExpiryYear(payload.getExpiryYear());
        bankCard.setBalance(2000.0);
        bankCard.setServiceCenter(getById(payload.getServiceCenterId()));
        bankCard.setUser(session.getUser());
        bankCard.setPaymentMethod(paymentMethod);

        bankCard = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userbankcards/save",
                new HttpEntity<>(bankCard, HttpHeadersUtils.getApplicationJsonHeader()), UserBankCard.class,
                xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorCode(),
                xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.PAYMENT_METHOD_ADDED.getMessage());
    }

    @Override
    public GenStatusResponse deletePaymentMethod(long userBankCardId) throws TaboorQMSServiceException, Exception {

        String uri = dbConnectorMicroserviceURL + "/tab/userbankcards/delete?userBankCardId=" + userBankCardId;
        ResponseEntity<Boolean> deletePaymentMethodResponse = restTemplate.getForEntity(uri, Boolean.class);

        if (!deletePaymentMethodResponse.getStatusCode().equals(HttpStatus.OK)
                || deletePaymentMethodResponse.getBody().equals(false)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.PAYMENT_METHOD_NOT_DELTED.getErrorCode() + "::"
                    + xyzErrorMessage.PAYMENT_METHOD_NOT_DELTED.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.PAYMENT_METHOD_DELTED.getMessage());
    }

    @Override
    public UserBankCard updatePaymentMethod(UpdatePaymentMethodPayload payload)
            throws TaboorQMSServiceException, Exception {

        UserBankCard getUserBankCardResponse = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/userbankcards/get/id?userBankCardId=" + payload.getBankCardId(),
                UserBankCard.class, xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorDetails());

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        if (currentYear > payload.getExpiryYear() || (currentYear == payload.getExpiryYear() && currentMonth > payload.getExpiryMonth())) {
            throw new TaboorQMSServiceException(xyzErrorMessage.CARD_DATE_EXPIRED.getErrorCode() + "::"
                    + xyzErrorMessage.CARD_DATE_EXPIRED.getErrorDetails());
        }
        if (!getUserBankCardResponse.getCardTitle().equals(payload.getCardTitle())) {
            getUserBankCardResponse.setCardTitle(payload.getCardTitle());
        }

        if (!getUserBankCardResponse.getCardNumber().equals(payload.getCardNumber())) {
            getUserBankCardResponse.setCardNumber(payload.getCardNumber());
        }

        if (!getUserBankCardResponse.getCardType().equals(payload.getCardType())) {
            getUserBankCardResponse.setCardType(payload.getCardType());
        }

        if (getUserBankCardResponse.getExpiryMonth() != payload.getExpiryMonth()) {
            getUserBankCardResponse.setExpiryMonth(payload.getExpiryMonth());
        }

        if (getUserBankCardResponse.getExpiryYear() != payload.getExpiryYear()) {
            getUserBankCardResponse.setExpiryYear(payload.getExpiryYear());
        }

        if (!getUserBankCardResponse.getCardCvv().equals(payload.getCardCvv())) {
            getUserBankCardResponse.setCardCvv(payload.getCardCvv());
        }

        getUserBankCardResponse = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userbankcards/save",
                new HttpEntity<>(getUserBankCardResponse, HttpHeadersUtils.getApplicationJsonHeader()), UserBankCard.class,
                xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorCode(),
                xyzErrorMessage.USERBANKCARD_NOT_UPDATED.getErrorDetails());

        return getUserBankCardResponse;
    }

    @Override
    public List<Invoice> getInvoices(long serviceCentreId) throws TaboorQMSServiceException, Exception {

        List<?> invoices = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/invoices/get/serviceCenter?serviceCenterId=" + serviceCentreId,
                List.class, xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.PAYMENT_METHOD_NOT_EXISTS.getErrorDetails());

        List<Invoice> invoicesList = GenericMapper.convertListToObject(invoices, new TypeReference<List<Invoice>>() {
        });

        return invoicesList;
    }

    @Override
    public GetServiceCenterSubscriptionResponse getSubscription(GetByIdPayload getByIdPayload)
            throws TaboorQMSServiceException, Exception {

        Long serviceCentreId = getByIdPayload.getId();
        if (serviceCentreId == 0) {
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            Session session = myUserDetails.getSession();
            ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                    .findByUserId(session.getUser().getUserId());
            serviceCentreId = serviceCenterEmployee.getServiceCenter().getServiceCenterId();
        }

        ServiceCenterSubscription serviceCenterSubscription = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/get/serviceCenterId?serviceCenterId="
                + serviceCentreId,
                ServiceCenterSubscription.class, xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorDetails());

        Integer servicesCount = RestUtil.getRequest(dbConnectorMicroserviceURL
                + "/tab/servicecenterservicetabmapper/countServices/serviceCenter?serviceCenterId=" + serviceCentreId,
                Integer.class, xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorDetails());

        return new GetServiceCenterSubscriptionResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_SUBSCRIPITION_FETCHED.getMessage(), serviceCenterSubscription,
                servicesCount);
    }

    @Override
    public GenStatusResponse upgradeSubscription(UpdateSubscriptionPlanPayload payload)
            throws TaboorQMSServiceException, Exception {

        ServiceCenterSubscription serviceCenterSubscription = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/get/serviceCenterId?serviceCenterId="
                + payload.getServiceCenterId(),
                ServiceCenterSubscription.class, xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorDetails());

        serviceCenterSubscription.setNoOfBranches(payload.getNoOfBranches());
        serviceCenterSubscription.setNoOfUsersPerBranch(payload.getNoOfUsersPerBranch());
        serviceCenterSubscription.setNoOfSMS(payload.getNoOfSMS());
        serviceCenterSubscription.setChargesTotal(payload.getTotalCharges());

        serviceCenterSubscription = saveServiceCenterSubscription(serviceCenterSubscription);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                "Service Center Subscription Upgraded Successfully.");
    }

    @Override
    public GetBranchListResponse getBranches() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());

        String uri = dbConnectorMicroserviceURL + "/tab/branches/get/serviceCenter?serviceCenterId"
                + serviceCenterEmployee.getServiceCenter().getServiceCenterId();
        ResponseEntity<?> getBranchesResponse = restTemplate.getForEntity(uri, List.class);

        List<Branch> branchList = GenericMapper.convertListToObject(getBranchesResponse.getBody(),
                new TypeReference<List<Branch>>() {
        });
        branchList.forEach(entity -> entity.setServiceCenter(null));
        return new GetBranchListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_FETCHED.getMessage(), branchList);
    }

    @Override
    public GetServiceCentreListingResponse getAllListing() throws TaboorQMSServiceException, Exception {

        List<?> resposne = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/servicecenters/getListing",
                List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<ServiceCenterObject> centerListing = GenericMapper.convertListToObject(resposne,
                new TypeReference<List<ServiceCenterObject>>() {
        });
        return new GetServiceCentreListingResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_FETCHED.getMessage(), centerListing);
    }

    @Override
    public GetValuesResposne addBankCard(AddUserBankCardPayload payload) throws TaboorQMSServiceException, Exception {

        ServiceCenter serviceCenter = getById(payload.getServiceCenterId());
        String uri = dbConnectorMicroserviceURL + "/tab/paymentmethods/get/id?paymentMethodId="
                + payload.getPaymentMethodId();

        ResponseEntity<PaymentMethod> getPaymentMethodResponse = restTemplate.getForEntity(uri, PaymentMethod.class);

        if (!getPaymentMethodResponse.getStatusCode().equals(HttpStatus.OK)
                || getPaymentMethodResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorCode() + "::"
                    + xyzErrorMessage.PAYMENT_METHOD_INVALID.getErrorDetails());
        }
// add credit card validation
        boolean isCardNumberValid = isValid(Long.parseLong(payload.getCardNumber()));
        if (!isCardNumberValid) {
            throw new TaboorQMSServiceException(xyzErrorMessage.INVALID_CARD_NUMBER.getErrorCode() + "::"
                    + xyzErrorMessage.INVALID_CARD_NUMBER.getErrorDetails());
        }
        boolean isCardExpired = isExpired(payload.getExpiryDate());
        if (isCardExpired) {
            throw new TaboorQMSServiceException(xyzErrorMessage.CARD_DATE_EXPIRED.getErrorCode() + "::"
                    + xyzErrorMessage.CARD_DATE_EXPIRED.getErrorDetails());
        }

        UserBankCard userBankCard = new UserBankCard();
        userBankCard.setCardTitle(payload.getCardTitle());
        userBankCard.setCardNumber(payload.getCardNumber());
        userBankCard.setCardType(payload.getCardType());
        userBankCard.setExpiryMonth(Integer.parseInt(payload.getExpiryDate().split("/")[0]));
        userBankCard.setExpiryYear(Integer.parseInt(payload.getExpiryDate().split("/")[1]));
        userBankCard.setCardCvv(payload.getCardCvv());
        userBankCard.setPaymentMethod(getPaymentMethodResponse.getBody());
        userBankCard.setBalance(0.0);
        userBankCard.setUser(null);
        userBankCard.setServiceCenter(serviceCenter);

        uri = dbConnectorMicroserviceURL + "/tab/userbankcards/save";

        HttpEntity<UserBankCard> entity = new HttpEntity<>(userBankCard, HttpHeadersUtils.getApplicationJsonHeader());

        ResponseEntity<UserBankCard> createUserBankCardResponse = restTemplate.postForEntity(uri, entity,
                UserBankCard.class);

        if (!createUserBankCardResponse.getStatusCode().equals(HttpStatus.OK)
                || createUserBankCardResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERBANKCARD_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.USERBANKCARD_NOT_CREATED.getErrorDetails());
        }

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("UserBankCardId", String.valueOf(createUserBankCardResponse.getBody().getBankCardId()));

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_BANK_CARD_ADDED.getMessage(), values);
    }

    @Override
    public GenStatusResponse paySubscriptionCharges(PaySubscriptionChargesPayload paySubscriptionChargesPayload)
            throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/get/id?subscriptionId="
                + paySubscriptionChargesPayload.getServiceCenterSubscriptionId();
        ResponseEntity<ServiceCenterSubscription> getServiceCenterSubscriptionResponse = restTemplate.getForEntity(uri,
                ServiceCenterSubscription.class);

        if (!getServiceCenterSubscriptionResponse.getStatusCode().equals(HttpStatus.OK)
                || getServiceCenterSubscriptionResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorCode()
                    + "::" + xyzErrorMessage.SERVICECENTERSUBSCRIPTION_NOT_CREATED.getErrorDetails());
        }

        uri = dbConnectorMicroserviceURL + "/tab/userbankcards/get/id?userBankCardId="
                + paySubscriptionChargesPayload.getBankCardId();
        ResponseEntity<UserBankCard> getUserBankCardResponse = restTemplate.getForEntity(uri, UserBankCard.class);

        if (!getUserBankCardResponse.getStatusCode().equals(HttpStatus.OK) || getUserBankCardResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_BANK_CARD_NOT_EXISTS.getErrorDetails());
        }

        ServiceCenterSubscription serviceCenterSubscription = getServiceCenterSubscriptionResponse.getBody();

        // TODO Pay Invoice
        serviceCenterSubscription.setBankCard(getUserBankCardResponse.getBody());
        serviceCenterSubscription.setSubscriptionStatus(SubscriptionStatus.ACTIVE);
        saveServiceCenterSubscription(serviceCenterSubscription);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_SUBSCRIPITION_PAID.getMessage());
    }

    @Override
    public GenStatusResponse addService(AddServicePayload addServicePayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());

        boolean editService = false;
        if (addServicePayload.getService() != null) {
            if (addServicePayload.getService().getServiceId() != null) {
                if (addServicePayload.getService().getServiceId() > 0) {
                    editService = true;
                }
            }
        }

        ServiceTAB serviceTab = new ServiceTAB();
        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper = new ServiceCenterServiceTABMapper();

        List<ServiceRequirement> serviceRequirements = new ArrayList<ServiceRequirement>();
        for (ServiceRequirement req : addServicePayload.getRequirementList()) {
            if (req.getRequirementId() == 0) {
                req.setRequirementId(null);
                req = requiredDocumentService.saveRequiredDocument(req);
            }
            serviceRequirements.add(req);
        }

        if (editService) {
            serviceTab = addServicePayload.getService();
            serviceTab.setServiceName(addServicePayload.getService().getServiceName());
            serviceTab.setServiceNameArabic(addServicePayload.getService().getServiceNameArabic());
            serviceTab.setServiceDescription(addServicePayload.getService().getServiceDescription());

            addServicePayload.setService(serviceTABService.saveServiceTAB(serviceTab));

            serviceCenterServiceTABMapper = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/servicecenterservicetabmapper/get/serviceCenterIdAndServiceId?"
                    + "serviceCenterId=" + serviceCenterEmployee.getServiceCenter().getServiceCenterId()
                    + "&serviceId=" + addServicePayload.getService().getServiceId(),
                    ServiceCenterServiceTABMapper.class, xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorDetails());

        } else {
            // New Service
            Boolean limitAcheived = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/checkServiceLimit?serviceCenterId="
                    + serviceCenterEmployee.getServiceCenter().getServiceCenterId(),
                    Boolean.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                    xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

            if (limitAcheived) {
                throw new TaboorQMSServiceException(xyzErrorMessage.SERVICE_LIMIT_EXCEEDS.getErrorCode() + "::"
                        + xyzErrorMessage.SERVICE_LIMIT_EXCEEDS.getErrorDetails());
            }

            serviceTab.setServiceName(addServicePayload.getService().getServiceName());
            serviceTab.setServiceNameArabic(addServicePayload.getService().getServiceNameArabic());
            serviceTab.setServiceDescription(addServicePayload.getService().getServiceDescription());
            addServicePayload.setService(serviceTABService.saveServiceTAB(serviceTab));

            serviceCenterServiceTABMapper.setServiceCenter(serviceCenterEmployee.getServiceCenter());
            serviceCenterServiceTABMapper.setServiceTAB(addServicePayload.getService());

        }

        serviceCenterServiceTABMapper.setKiosikEnabled(addServicePayload.getKiosikEnabled());
        serviceCenterServiceTABMapper.setServiceRequirements(serviceRequirements);
        serviceCenterServiceTABMapper.setServiceCode(addServicePayload.getServiceCode());

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/servicecenterservicetabmapper/save",
                serviceCenterServiceTABMapper, ServiceCenterServiceTABMapper.class,
                xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.SERVICE_ADDED.getMessage());
    }

    @Override
    public GetValuesResposne getDashboardActivities(GetActivitiesPayload getActivitiesPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());
        List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

        if (branchList.size() == 0) {
            return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.DASHBOARD_ACTIVITIES_FETHCED.getMessage(), null);
        }

        List<Long> branchIdList = new ArrayList<Long>();
        for (Branch branch : branchList) {
            branchIdList.add(branch.getBranchId());
        }
        branchIdList = branchIdList.stream().distinct().collect(Collectors.toList());

        getActivitiesPayload.setIdList(branchIdList);

        String uri = dbConnectorMicroserviceURL + "/tab/tickets/count/branchList";
        HttpEntity<GetActivitiesPayload> entity = new HttpEntity<>(getActivitiesPayload,
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<Long[]> countTicketByBranchResponse = restTemplate.postForEntity(uri, entity, Long[].class);

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("NoOfTickets", new HashMap<String, Object>() {
            {
                put("TotalTickets", countTicketByBranchResponse.getBody()[0]);
                put("FastpassTickets", countTicketByBranchResponse.getBody()[1]);
            }
        });

        if (branchList.size() == 1) { // Single Branch Admin

            values.put("BranchDetails", new HashMap<String, Object>() {
                {
                    put("BranchId", branchList.get(0).getBranchId());
                    put("BranchName", branchList.get(0).getBranchName());
                    put("BranchNameArabic", branchList.get(0).getBranchNameArabic());
                }
            });

            uri = dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branch?branchId="
                    + branchList.get(0).getBranchId();
            ResponseEntity<?> getBranchServicesResponse = restTemplate.getForEntity(uri, List.class);
            List<ServiceTAB> branchServicesList = GenericMapper.convertListToObject(getBranchServicesResponse.getBody(),
                    new TypeReference<List<ServiceTAB>>() {
            });

            List<Long> serviceIdList = new ArrayList<Long>();
            for (ServiceTAB service : branchServicesList) {
                serviceIdList.add(service.getServiceId());
            }

            getActivitiesPayload.getIdList().addAll(serviceIdList);

            uri = dbConnectorMicroserviceURL + "/tab/queueticketmapper/getHighestDemandService/branch";
            HttpEntity<GetActivitiesPayload> httpEntity = new HttpEntity<>(getActivitiesPayload,
                    HttpHeadersUtils.getApplicationJsonHeader());
            ResponseEntity<?> getHighestDemandServiceResponse = restTemplate.postForEntity(uri, httpEntity, List.class);

            List<Long> idsList = GenericMapper.convertListToObject(getHighestDemandServiceResponse.getBody(),
                    new TypeReference<List<Long>>() {
            });

            uri = dbConnectorMicroserviceURL + "/tab/queues/get/branch?branchId=" + branchList.get(0).getBranchId();
            ResponseEntity<?> getBranchQueuesResponse = restTemplate.getForEntity(uri, List.class);
            List<Queue> branchQueuesList = GenericMapper.convertListToObject(getBranchQueuesResponse.getBody(),
                    new TypeReference<List<Queue>>() {
            });

            LocalTime highestAST = branchList.get(0).getAverageServiceTime();
            LocalTime lowestAST = branchList.get(0).getAverageServiceTime();
            int highestPerformanceQueueIndex = 0;
            int lowestPerformanceQueueIndex = 0;

            for (int i = 0; i < branchQueuesList.size(); i++) {
                Queue queueEntity = branchQueuesList.get(i);
                if (queueEntity.getService().getServiceId().equals(idsList.get(0))) {
                    values.put("HighestDemandService", new HashMap<String, Object>() {
                        {
                            put("Service", queueEntity.getService());
                            put("TotalTickets", idsList.get(1)); // idsList 1 is TotalTickets of Branch
                            put("AST", queueEntity.getAverageServiceTime());
                        }
                    });
                }
                if (queueEntity.getAverageServiceTime().isBefore(lowestAST)) {
                    lowestAST = queueEntity.getAverageServiceTime();
                    highestPerformanceQueueIndex = i;
                }
                if (queueEntity.getAverageServiceTime().isAfter(highestAST)) {
                    highestAST = queueEntity.getAverageServiceTime();
                    lowestPerformanceQueueIndex = i;
                }
            }
            Queue highestPerformanceQueue = branchQueuesList.get(highestPerformanceQueueIndex);
            values.put("HighestPerformanceService", new HashMap<String, Object>() {
                {
                    put("Service", highestPerformanceQueue.getService());
                    String uri = dbConnectorMicroserviceURL + "/tab/queueticketmapper/count/queue?queueId="
                            + highestPerformanceQueue.getQueueId();
                    put("TotalTickets", restTemplate.getForEntity(uri, Integer.class).getBody());
                    put("AST", highestPerformanceQueue.getAverageServiceTime());
                }
            });

            Queue lowestPerformanceQueue = branchQueuesList.get(lowestPerformanceQueueIndex);
            values.put("LowestPerformanceService", new HashMap<String, Object>() {
                {
                    put("Service", lowestPerformanceQueue.getService());
                    String uri = dbConnectorMicroserviceURL + "/tab/queueticketmapper/count/queue?queueId="
                            + lowestPerformanceQueue.getQueueId();
                    put("TotalTickets", restTemplate.getForEntity(uri, Integer.class).getBody());
                    put("AST", lowestPerformanceQueue.getAverageServiceTime());
                }
            });
            values.put("ServiceCenter", serviceCenterEmployee.getServiceCenter());

            return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.DASHBOARD_ACTIVITIES_FETHCED.getMessage(), values);
        }

        uri = dbConnectorMicroserviceURL + "/tab/tickets/getMaxVisitedBranch/branchList";
        ResponseEntity<?> getMostVisitedBranchResponse = restTemplate.postForEntity(uri, entity, List.class);
        List<Long> idsList = GenericMapper.convertListToObject(getMostVisitedBranchResponse.getBody(),
                new TypeReference<List<Long>>() {
        });
        LocalTime highestAST = branchList.get(0).getAverageServiceTime();
        LocalTime lowestAST = branchList.get(0).getAverageServiceTime();
        int highestPerformanceBranchIndex = 0;
        int lowestPerformanceBranchIndex = 0;

        for (int i = 0; i < branchList.size(); i++) {
            Branch branchEntity = branchList.get(i);
            if (branchEntity.getAverageServiceTime().isBefore(lowestAST)) {
                lowestAST = branchEntity.getAverageServiceTime();
                highestPerformanceBranchIndex = i;
            }
            if (branchEntity.getAverageServiceTime().isAfter(highestAST)) {
                highestAST = branchEntity.getAverageServiceTime();
                lowestPerformanceBranchIndex = i;
            }
            if (branchEntity.getBranchId().equals(idsList.get(0))) // idsList 0 is BranchId
            {
                values.put("MostVisitedBranch", new HashMap<String, Object>() {
                    {
                        put("BranchId", branchEntity.getBranchId());
                        put("BranchName", branchEntity.getBranchName());
                        put("BranchNameArabic", branchEntity.getBranchNameArabic());
                        put("TotalTickets", idsList.get(1)); // idsList 1 is TotalTickets of Branch
                        put("AST", branchEntity.getAverageServiceTime());
                    }
                });
            }
        }
        Branch highestPerformanceBranch = branchList.get(highestPerformanceBranchIndex);
        values.put("HighestPerformanceBranch", new HashMap<String, Object>() {
            {
                put("BranchId", highestPerformanceBranch.getBranchId());
                put("BranchName", highestPerformanceBranch.getBranchName());
                put("BranchNameArabic", highestPerformanceBranch.getBranchNameArabic());
                String uri = dbConnectorMicroserviceURL + "/tab/tickets/count/branch";
                HttpEntity<Branch> entity = new HttpEntity<>(highestPerformanceBranch,
                        HttpHeadersUtils.getApplicationJsonHeader());
                ResponseEntity<Integer> getBranchTicketCountResponse = restTemplate.postForEntity(uri, entity,
                        Integer.class);
                put("TotalTickets", getBranchTicketCountResponse.getBody());
                put("AST", highestPerformanceBranch.getAverageServiceTime());
            }
        });
        Branch lowestPerformanceBranch = branchList.get(lowestPerformanceBranchIndex);
        values.put("LowestPerformanceBranch", new HashMap<String, Object>() {
            {
                put("BranchId", lowestPerformanceBranch.getBranchId());
                put("BranchName", lowestPerformanceBranch.getBranchName());
                put("BranchNameArabic", lowestPerformanceBranch.getBranchNameArabic());
                String uri = dbConnectorMicroserviceURL + "/tab/tickets/count/branch";
                HttpEntity<Branch> entity = new HttpEntity<>(lowestPerformanceBranch,
                        HttpHeadersUtils.getApplicationJsonHeader());
                ResponseEntity<Integer> getBranchTicketCountResponse = restTemplate.postForEntity(uri, entity,
                        Integer.class);
                put("TotalTickets", getBranchTicketCountResponse.getBody());
                put("AST", lowestPerformanceBranch.getAverageServiceTime());
            }
        });
        values.put("ServiceCenter", serviceCenterEmployee.getServiceCenter());

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.DASHBOARD_ACTIVITIES_FETHCED.getMessage(), values);
    }

    @Override
    public GenStatusResponse addSupportTicket(AddSupportTicketPayload addSupportTicketPayload,
            @Valid List<MultipartFile> files) throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());

        SupportTicket supportTicket;

        if (addSupportTicketPayload.getSupportTicketId() != null) {
            supportTicket = getSupportTicketById(addSupportTicketPayload.getSupportTicketId());
        } else {
            supportTicket = new SupportTicket();
            supportTicket.setCreatedOn(OffsetDateTime.now().withNano(0));

            List<String> encodedFilePathList = new ArrayList<String>();
            List<FileDataObject> fileDataObjects = new ArrayList<FileDataObject>();

            for (MultipartFile file : files) {
                String filePath = TaboorQMSConfigurations.HOST_SUPPORTTICKET_FOLDER.getValue()
                        .concat(RandomString.make(5)).concat("-" + file.getOriginalFilename());
                encodedFilePathList.add(Base64.getEncoder().encodeToString(filePath.getBytes()));
                fileDataObjects.add(new FileDataObject(file, filePath));
            }
            Boolean saved = fileDataService.save(fileDataObjects);
            if (saved) {
                supportTicket.setFilePathList(encodedFilePathList);
            } else {
                throw new TaboorQMSServiceException(500 + "::" + "Internal Server Error, Files not saved");
            }

            supportTicket.setEmpCreatedBy(serviceCenterEmployee);
        }
        supportTicket.setUpdatedOn(OffsetDateTime.now().withNano(0));
        supportTicket.setSupportTicketStatus(SupportTicketStatus.In_Queue);
        supportTicket.setSubject(addSupportTicketPayload.getSubject());
        supportTicket.setDescription(addSupportTicketPayload.getDescription());

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/supporttickets/save",
                new HttpEntity<>(supportTicket, HttpHeadersUtils.getApplicationJsonHeader()), SupportTicket.class,
                xyzErrorMessage.SUPPORTTCKET_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SUPPORTTCKET_NOT_CREATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SUPPORT_TICKET_ADDED.getMessage());
    }

    @Override
    public GetSupportTicketListResponse getSupportTickets() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        String uri;
        if (session.getUser().getRoleName().startsWith("Taboor")) {
            uri = dbConnectorMicroserviceURL + "/tab/supporttickets/get/all";
        } else {
            ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                    .findByUserId(session.getUser().getUserId());
            uri = dbConnectorMicroserviceURL + "/tab/supporttickets/get/serviceCenter?serviceCentreId="
                    + serviceCenterEmployee.getServiceCenter().getServiceCenterId();
        }

        List<SupportTicket> supportTicketList = GenericMapper.convertListToObject(
                RestUtil.getRequest(uri, List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                        xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails()),
                new TypeReference<List<SupportTicket>>() {
        });

        return new GetSupportTicketListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SUPPORT_TICKET_FETCHED.getMessage(), supportTicketList);
    }

    @Override
    public GenStatusResponse deleteSupportTicket(long supportTicketId) throws TaboorQMSServiceException, Exception {

        String uri = dbConnectorMicroserviceURL + "/tab/supporttickets/delete/id?supportTicketId=" + supportTicketId;
        ResponseEntity<Boolean> deleteSupportTicketResponse = restTemplate.getForEntity(uri, Boolean.class);

        if (!deleteSupportTicketResponse.getStatusCode().equals(HttpStatus.OK)
                || deleteSupportTicketResponse.getBody().equals(false)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SUPPORTTCKET_NOT_DELETED.getErrorCode() + "::"
                    + xyzErrorMessage.SUPPORTTCKET_NOT_DELETED.getErrorDetails());
        }
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SUPPORT_TICKET_DELETED.getMessage());
    }

    @Override
    public GetStatisticsResponse getDashboardStatistics(GetActivitiesPayload getActivitiesPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());
        if (getActivitiesPayload.getIdList() == null) {
            getActivitiesPayload.setIdList(new ArrayList<Long>());
        }
        if (getActivitiesPayload.getIdList().isEmpty()) {
            List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

//			if (branchList.size() == 0)
//				return new GetStatisticsResponse(xyzResponseCode.SUCCESS.getCode(),
//						xyzResponseMessage.TICKETS_STATISTICS_FETHCED.getMessage(), null);
            for (Branch branch : branchList) {
                getActivitiesPayload.getIdList().add(branch.getBranchId());
            }
            getActivitiesPayload
                    .setIdList(getActivitiesPayload.getIdList().stream().distinct().collect(Collectors.toList()));
        }

        Period period = Period.between(getActivitiesPayload.getStartDate(), getActivitiesPayload.getEndDate());

        System.out.println(getActivitiesPayload.getStartDate());
        List<ValueObject> valueObjects = new ArrayList<ValueObject>();

        if (period.getYears() > 0) {
            // breakdown into years
            LocalDate startYear = getActivitiesPayload.getStartDate(),
                    endYear = getActivitiesPayload.getStartDate().plusYears(1).withDayOfYear(1).minusDays(1);

            for (int i = 0; i <= period.getYears(); i++, startYear = endYear.plusDays(1), endYear = startYear
                    .plusYears(1).minusDays(1)) {
                if (endYear.isAfter(getActivitiesPayload.getEndDate())) {
                    endYear = getActivitiesPayload.getEndDate();
                }
//                logger.info("StartDate: " + startYear);
//                logger.info("EndDate: " + endYear);
                valueObjects.add(new ValueObject(String.valueOf(startYear.getYear()),
                        String.valueOf(getBranchTicketsStats(getActivitiesPayload.getIdList(), startYear, endYear))));
//                logger.info("-----------Business Logic-----------");
            }
        } else if (period.getMonths() > 0) {
            // breakdown into months
            LocalDate startMonth = getActivitiesPayload.getStartDate(),
                    endMonth = getActivitiesPayload.getStartDate().plusMonths(1).withDayOfMonth(1).minusDays(1);

            for (int i = 0; i <= period.getMonths(); i++, startMonth = endMonth.plusDays(1), endMonth = startMonth
                    .plusMonths(1).minusDays(1)) {
                if (endMonth.isAfter(getActivitiesPayload.getEndDate())) {
                    endMonth = getActivitiesPayload.getEndDate();
                }
//                logger.info("StartDate: " + startMonth);
//                logger.info("EndDate: " + endMonth);
                valueObjects.add(new ValueObject(
                        String.valueOf(startMonth.getMonth().getDisplayName(TextStyle.SHORT, Locale.US)),
                        String.valueOf(getBranchTicketsStats(getActivitiesPayload.getIdList(), startMonth, endMonth))));
//                logger.info("-----------Business Logic-----------");
            }
        } else if (period.getDays() > 6) {
            // breakdown into weeks
            LocalDate startWeek = getActivitiesPayload.getStartDate(),
                    endWeek = getActivitiesPayload.getStartDate().plusWeeks(1).minusDays(1);
            int i = 1;
            while (true) {
                if (endWeek.isAfter(getActivitiesPayload.getEndDate())) {
                    endWeek = getActivitiesPayload.getEndDate();
                }
//                logger.info("StartDate: " + startWeek);
//                logger.info("EndDate: " + endWeek);
                valueObjects.add(new ValueObject("W-" + i++,
                        String.valueOf(getBranchTicketsStats(getActivitiesPayload.getIdList(), startWeek, endWeek))));
//                logger.info("-----------Business Logic-----------");
                if (!endWeek.isBefore(getActivitiesPayload.getEndDate())) {
                    break;
                }
                startWeek = endWeek.plusDays(1);
                endWeek = startWeek.plusWeeks(1).minusDays(1);
            }
        } else if (period.getDays() > 0
                || getActivitiesPayload.getStartDate().equals(getActivitiesPayload.getEndDate())) {
            // breakdown into days
            for (LocalDate currentDate = getActivitiesPayload.getStartDate(); getActivitiesPayload.getEndDate()
                    .plusDays(1).isAfter(currentDate); currentDate = currentDate.plusDays(1)) {
//                logger.info("CurrentDate: " + currentDate);
                valueObjects.add(new ValueObject(
                        String.valueOf(currentDate.getDayOfWeek().getDisplayName(TextStyle.NARROW, Locale.US)),
                        String.valueOf(
                                getBranchTicketsStats(getActivitiesPayload.getIdList(), currentDate, currentDate))));
            }
        }
        return new GetStatisticsResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKETS_STATISTICS_FETHCED.getMessage(), valueObjects);
    }

    private int getBranchTicketsStats(List<Long> branchIdList, LocalDate startDate, LocalDate endDate) {
        if (branchIdList.isEmpty()) {
            return 0;
        }
        String uri = dbConnectorMicroserviceURL + "/tab/tickets/count/branchList";
        HttpEntity<GetActivitiesPayload> entity = new HttpEntity<>(
                new GetActivitiesPayload(startDate, endDate, branchIdList),
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<Long[]> countTicketByBranchResponse = restTemplate.postForEntity(uri, entity, Long[].class);
        return countTicketByBranchResponse.getBody()[0].intValue();
    }

    @Override
    public GetValuesResposne getDashboardRatings(GetActivitiesPayload getActivitiesPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());
        if (getActivitiesPayload.getIdList() == null) {
            getActivitiesPayload.setIdList(new ArrayList<Long>());
        }
        if (getActivitiesPayload.getIdList().isEmpty()) {
            List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

            if (branchList.size() == 0) {
                return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                        xyzResponseMessage.TICKETS_FEEDBACK_STATISTICS_FETHCED.getMessage(), null);
            }
            for (Branch branch : branchList) {
                getActivitiesPayload.getIdList().add(branch.getBranchId());
            }
            getActivitiesPayload
                    .setIdList(getActivitiesPayload.getIdList().stream().distinct().collect(Collectors.toList()));
        }

        Map<String, Object> values = new HashMap<String, Object>();

        String uri = dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/count/branchList";
        HttpEntity<GetActivitiesPayload> entity = new HttpEntity<>(
                new GetActivitiesPayload(getActivitiesPayload.getStartDate(), getActivitiesPayload.getEndDate(),
                        getActivitiesPayload.getIdList()),
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<?> countTicketFeedbackByBranchResponse = restTemplate.postForEntity(uri, entity, List.class);

//		List<ValueObject> response = new ArrayList<GetStatisticsResponse.ValueObject>();
//		response.add(new ValueObject("1.0", String.valueOf(10)));
//		response.add(new ValueObject("2.0", String.valueOf(20)));
//		response.add(new ValueObject("3.0", String.valueOf(30)));
        List<ValueObject> response = GenericMapper.convertListToObject(countTicketFeedbackByBranchResponse.getBody(),
                new TypeReference<List<ValueObject>>() {
        });

        if (response.isEmpty()) {
            values.put("1.0", "0");
            values.put("2.0", "0");
            values.put("3.0", "0");
        }
        response.forEach((responseEntity) -> values.put(responseEntity.getKey(), responseEntity.getValue()));

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKETS_FEEDBACK_STATISTICS_FETHCED.getMessage(), values);
    }

    @Override
    public GetServiceCentreListResponse getAll() throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/servicecenters/get/all";
        ResponseEntity<?> getAllServiceCenterResponse = restTemplate.getForEntity(uri, List.class);
        List<ServiceCenter> serviceCenterList = GenericMapper.convertListToObject(getAllServiceCenterResponse.getBody(),
                new TypeReference<List<ServiceCenter>>() {
        });

        List<ServiceCentreResponse> serviceCenterListResponse = new ArrayList<GetServiceCentreListResponse.ServiceCentreResponse>();
        for (ServiceCenter entity : serviceCenterList) {
            GetServiceCentreListResponse.ServiceCentreResponse serviceCentreResponse = new GetServiceCentreListResponse().new ServiceCentreResponse();
            serviceCentreResponse.setServiceCenterId(entity.getServiceCenterId());
            serviceCentreResponse.setCountry(entity.getCountry());
            serviceCentreResponse.setServiceCenterName(entity.getServiceCenterName());
            serviceCentreResponse.setServiceCenterNameArabic(entity.getServiceCenterArabicName());
            serviceCenterListResponse.add(serviceCentreResponse);
        }

        return new GetServiceCentreListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_FETCHED.getMessage(), serviceCenterListResponse);
    }

    @Override
    public GetServiceCenterSubscriptionListResponse getActiveSubscriptionList()
            throws TaboorQMSServiceException, Exception {
        List<?> response = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/getAll/active", List.class,
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<ServiceCenterSubscription> subscriptionList = GenericMapper.convertListToObject(response,
                new TypeReference<List<ServiceCenterSubscription>>() {
        });

        for (ServiceCenterSubscription item : subscriptionList) {
            item.setBankCard(null);
            item.setPaymentPlan(null);
        }
        return new GetServiceCenterSubscriptionListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_SUBSCRIPITION_FETCHED.getMessage(), subscriptionList);
    }

    @Override
    public GenStatusResponse updateSupportTicketStatus(UpdateSupportTicketStatusPayload payload)
            throws TaboorQMSServiceException, Exception {
        SupportTicket supportTicket = getSupportTicketById(payload.getSupportTicketId());
        supportTicket.setSupportTicketStatus(payload.getSupportTicketStatus());
        supportTicket.setUpdatedOn(OffsetDateTime.now().withNano(0));

        supportTicket.getStatusHistoryList().add(new StatusHistoryObject(supportTicket.getSupportTicketStatus(),
                payload.getReason(), supportTicket.getUpdatedOn()));

        supportTicket = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/supporttickets/save",
                new HttpEntity<>(supportTicket, HttpHeadersUtils.getApplicationJsonHeader()), SupportTicket.class,
                xyzErrorMessage.SUPPORTTCKET_NOT_UPDATED.getErrorCode(),
                xyzErrorMessage.SUPPORTTCKET_NOT_UPDATED.getErrorDetails());

        if (supportTicket.getEmpCreatedBy().getUser().getUserId() != null) {
            RSocketPayload payload2 = new RSocketPayload();
            payload2.setNotificationSubject(RSocketNotificationSubject.SUPPORT_TICKET_UPDATE);
            payload2.setRecipientId(supportTicket.getEmpCreatedBy().getUser().getUserId());
            payload2.setTypes(Arrays.asList(NotificationType.IN_APP));
            payload2.setInformation(Arrays.asList(String.valueOf(supportTicket.getSupportTicketId()),
                    "Now Ticket status is updated to '" + supportTicket.getSupportTicketStatus().name()
                    + "' with mentioned reason '" + payload.getReason() + "'."));

            RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                    new HttpEntity<>(payload2, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);
//		RSocketInitializer.fireAndForget(rSocketPayload);
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SUPPORT_TICKET_UPDATED.getMessage());
    }

    @Override
    public GenStatusResponse assignEmpToSupportTicket(GetByIdListPayload payload)
            throws TaboorQMSServiceException, Exception {
        SupportTicket supportTicket = getSupportTicketById(payload.getIds().get(0));

        TaboorEmployee taboorEmployee = new TaboorEmployee();
        taboorEmployee.setTaboorEmpId(payload.getIds().get(1));
        supportTicket.setEmpAssignedTo(taboorEmployee);
        supportTicket.setUpdatedOn(OffsetDateTime.now().withNano(0));

        supportTicket = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/supporttickets/save",
                new HttpEntity<>(supportTicket, HttpHeadersUtils.getApplicationJsonHeader()), SupportTicket.class,
                xyzErrorMessage.SUPPORTTCKET_NOT_UPDATED.getErrorCode(),
                xyzErrorMessage.SUPPORTTCKET_NOT_UPDATED.getErrorDetails());

        if (supportTicket.getEmpCreatedBy().getUser().getUserId() != null) {

            RSocketPayload payload2 = new RSocketPayload();
            payload2.setNotificationSubject(RSocketNotificationSubject.SUPPORT_TICKET_UPDATE);
            payload2.setRecipientId(supportTicket.getEmpCreatedBy().getUser().getUserId());
            payload2.setTypes(Arrays.asList(NotificationType.IN_APP));
            payload2.setInformation(
                    Arrays.asList(String.valueOf(supportTicket.getSupportTicketId()), "Taboor Employee '"
                            + supportTicket.getEmpAssignedTo().getUser().getName() + "' is assigned to this ticket."));

            RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                    new HttpEntity<>(payload2, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);
//		RSocketInitializer.fireAndForget(rSocketPayload);
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SUPPORT_TICKET_UPDATED.getMessage());
    }

    private SupportTicket getSupportTicketById(Long supportTicketId)
            throws RestClientException, TaboorQMSServiceException {
        return RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/supporttickets/get/id?supportTicketId=" + supportTicketId,
                SupportTicket.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
    }

    @Override
    public GetStatisticsResponse getVotes(GetActivitiesPayload payload) throws TaboorQMSServiceException, Exception {

        List<?> response = RestUtil.postRequest(
                dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/countRatings/serviceCenterId",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), List.class,
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<ValueObject> valueObjects = new ArrayList<ValueObject>();
        valueObjects.addAll(GenericMapper.convertListToObject(response, new TypeReference<List<ValueObject>>() {
        }));

        Period period = Period.between(payload.getStartDate(), payload.getEndDate());
        if (period.getYears() > 0) {
            // breakdown into years
            LocalDate startYear = payload.getStartDate(),
                    endYear = payload.getStartDate().plusYears(1).withDayOfYear(1).minusDays(1);

            for (int i = 0; i <= period.getYears(); i++, startYear = endYear.plusDays(1), endYear = startYear
                    .plusYears(1).minusDays(1)) {
                if (endYear.isAfter(payload.getEndDate())) {
                    endYear = payload.getEndDate();
                }
                valueObjects.add(new ValueObject(String.valueOf(startYear.getYear()),
                        String.valueOf(getServiceCenterVotes(payload.getIdList().get(0), startYear, endYear))));
            }
        } else if (period.getMonths() > 0) {
            // breakdown into months
            LocalDate startMonth = payload.getStartDate(),
                    endMonth = payload.getStartDate().plusMonths(1).withDayOfMonth(1).minusDays(1);

            for (int i = 0; i <= period.getMonths(); i++, startMonth = endMonth.plusDays(1), endMonth = startMonth
                    .plusMonths(1).minusDays(1)) {
                if (endMonth.isAfter(payload.getEndDate())) {
                    endMonth = payload.getEndDate();
                }
                valueObjects.add(new ValueObject(
                        String.valueOf(startMonth.getMonth().getDisplayName(TextStyle.SHORT, Locale.US)),
                        String.valueOf(getServiceCenterVotes(payload.getIdList().get(0), startMonth, endMonth))));
            }
        } else if (period.getDays() > 6) {
            // breakdown into weeks
            LocalDate startWeek = payload.getStartDate(), endWeek = payload.getStartDate().plusWeeks(1).minusDays(1);
            int i = 1;
            while (true) {
                if (endWeek.isAfter(payload.getEndDate())) {
                    endWeek = payload.getEndDate();
                }
                valueObjects.add(new ValueObject("W-" + i++,
                        String.valueOf(getServiceCenterVotes(payload.getIdList().get(0), startWeek, endWeek))));
                if (!endWeek.isBefore(payload.getEndDate())) {
                    break;
                }
                startWeek = endWeek.plusDays(1);
                endWeek = startWeek.plusWeeks(1).minusDays(1);
            }
        } else if (period.getDays() > 0) {
            // breakdown into days
            for (LocalDate currentDate = payload.getStartDate(); payload.getEndDate().plusDays(1)
                    .isAfter(currentDate); currentDate = currentDate.plusDays(1)) {
                valueObjects.add(new ValueObject(
                        String.valueOf(currentDate.getDayOfWeek().getDisplayName(TextStyle.NARROW, Locale.US)),
                        String.valueOf(getServiceCenterVotes(payload.getIdList().get(0), currentDate, currentDate))));
            }
        }
        return new GetStatisticsResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.DASHBOARD_ACTIVITIES_FETHCED.getMessage(), valueObjects);

    }

    private Double getServiceCenterVotes(Long serviceCenterId, LocalDate startDate, LocalDate endDate)
            throws RestClientException, TaboorQMSServiceException {
        return RestUtil.post(dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/countAvgServicePoints/serviceCenterId",
                new HttpEntity<>(new GetActivitiesPayload(startDate, endDate, Arrays.asList(serviceCenterId)),
                        HttpHeadersUtils.getApplicationJsonHeader()),
                Double.class);
    }

    @Override
    public GetValuesResposne countServiceCenter() throws TaboorQMSServiceException, Exception {
        Map<String, Object> values = new HashMap<String, Object>();
        values.put("CenterCount",
                RestUtil.getRequest(dbConnectorMicroserviceURL + "tab/servicecenters/count", Integer.class,
                        xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                        xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails()));

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_FETCHED.getMessage(), values);
    }

    // Return true if the card number is valid
    public static boolean isValid(long number) {
        return (getSize(number) >= 13
                && getSize(number) <= 16)
                && (prefixMatched(number, 4)
                || prefixMatched(number, 5)
                || prefixMatched(number, 37)
                || prefixMatched(number, 6))
                && ((sumOfDoubleEvenPlace(number)
                + sumOfOddPlace(number)) % 10 == 0);
    }

    // Get the result from Step 2
    public static int sumOfDoubleEvenPlace(long number) {
        int sum = 0;
        String num = number + "";
        for (int i = getSize(number) - 2; i >= 0; i -= 2) {
            sum += getDigit(Integer.parseInt(num.charAt(i) + "") * 2);
        }

        return sum;
    }

    // Return this number if it is a single digit, otherwise,
    // return the sum of the two digits
    public static int getDigit(int number) {
        if (number < 9) {
            return number;
        }
        return number / 10 + number % 10;
    }

    // Return sum of odd-place digits in number
    public static int sumOfOddPlace(long number) {
        int sum = 0;
        String num = number + "";
        for (int i = getSize(number) - 1; i >= 0; i -= 2) {
            sum += Integer.parseInt(num.charAt(i) + "");
        }
        return sum;
    }

    // Return true if the digit d is a prefix for number
    public static boolean prefixMatched(long number, int d) {
        return getPrefix(number, getSize(d)) == d;
    }

    // Return the number of digits in d
    public static int getSize(long d) {
        String num = d + "";
        return num.length();
    }

    // Return the first k number of digits from
    // number. If the number of digits in number
    // is less than k, return number.
    public static long getPrefix(long number, int k) {
        if (getSize(number) > k) {
            String num = number + "";
            return Long.parseLong(num.substring(0, k));
        }
        return number;
    }

    public static boolean isExpired(String input) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
        simpleDateFormat.setLenient(false);
        Date expiry = simpleDateFormat.parse(input);
        boolean expired = expiry.before(new Date());
        return expired;
    }
}
