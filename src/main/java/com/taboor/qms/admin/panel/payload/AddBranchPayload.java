package com.taboor.qms.admin.panel.payload;

import java.util.List;

import javax.validation.constraints.Email;

import com.taboor.qms.core.model.ServiceTAB;

public class AddBranchPayload {

	private Long branchId;

	private String branchName;
	private String branchNameArabic;
	@Email
	private String email;
	private String phoneNumber;
	private Double Longitude;
	private Double Latitude;
	private String City;
	private String address;

	private List<ServiceTAB> serviceList;
	private List<WorkingHours> workingHoursList;

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchNameArabic() {
		return branchNameArabic;
	}

	public void setBranchNameArabic(String branchNameArabic) {
		this.branchNameArabic = branchNameArabic;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Double getLongitude() {
		return Longitude;
	}

	public void setLongitude(Double longitude) {
		Longitude = longitude;
	}

	public Double getLatitude() {
		return Latitude;
	}

	public void setLatitude(Double latitude) {
		Latitude = latitude;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<ServiceTAB> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<ServiceTAB> serviceList) {
		this.serviceList = serviceList;
	}

	public List<WorkingHours> getWorkingHoursList() {
		return workingHoursList;
	}

	public void setWorkingHoursList(List<WorkingHours> workingHoursList) {
		this.workingHoursList = workingHoursList;
	}

}
