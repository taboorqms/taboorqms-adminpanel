package com.taboor.qms.admin.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.response.GetUserConfigurationsResponse;
import com.taboor.qms.admin.panel.response.GetUserNotificationTypeResponse;
import com.taboor.qms.admin.panel.service.SettingService;
import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.core.response.GenStatusResponse;

@RestController
@RequestMapping("/setting")
@CrossOrigin(origins = "*")
public class SettingController {

	private static final Logger logger = LoggerFactory.getLogger(SettingController.class);

	@Autowired
	SettingService settingService;

	@GetMapping("/configuration/get/user")
	public @ResponseBody GetUserConfigurationsResponse getUserConfigurations() throws Exception, Throwable {
		logger.info("Calling Get User Configuration - API");
		return settingService.getUserConfigurations();
	}

	@PostMapping("/configuration/update")
	public @ResponseBody GenStatusResponse updateUserConfiguration(@RequestBody UserConfiguration userConfiguration)
			throws Exception, Throwable {
		logger.info("Calling Update User Configuration - API");
		return settingService.updateUserConfigurations(userConfiguration);
	}

	@GetMapping("/notificationType/get/user")
	public @ResponseBody GetUserNotificationTypeResponse getUserNotificationType() throws Exception, Throwable {
		logger.info("Calling Get User Notification Type - API");
		return settingService.getUserNotificationType();
	}

	@PostMapping("/notificationType/update")
	public @ResponseBody GenStatusResponse updateNotificationType(
			@RequestBody UserNotificationType userNotificationType) throws Exception, Throwable {
		logger.info("Calling Update User Notification Type - API");
		return settingService.updateNotificationType(userNotificationType);
	}
}
