package com.taboor.qms.admin.panel.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.service.BranchCounterService;
import com.taboor.qms.admin.panel.service.BranchService;
import com.taboor.qms.admin.panel.service.EmpoyeeBranchService;
import com.taboor.qms.admin.panel.service.QueueService;
import com.taboor.qms.admin.panel.service.ServiceCenterEmpService;
import com.taboor.qms.admin.panel.service.ServiceCenterService;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.response.GetQueueListingResponse;
import com.taboor.qms.core.response.GetQueueListingResponse.QueueListingObject;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;

@Service
public class QueueServiceImpl implements QueueService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(QueueServiceImpl.class);

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Value("${url.microservice.user.management}")
	private String userManagementMicroserviceURL;

	private RestTemplate restTemplate = new RestTemplate();

	@Autowired
	ServiceCenterService serviceCenterService;

	@Autowired
	ServiceCenterEmpService serviceCenterEmpService;

	@Autowired
	EmpoyeeBranchService employeeBranchService;

	@Autowired
	BranchService branchService;

	@Autowired
	BranchCounterService branchCounterService;

	@Override
	public GetQueueListingResponse getQueueListing() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
				.findByUserId(session.getUser().getUserId());
		List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

		if (branchList.isEmpty())
			return new GetQueueListingResponse(xyzResponseCode.SUCCESS.getCode(),
					xyzResponseMessage.QUEUE_FETCHED.getMessage(), null);

		List<Long> branchIdList = new ArrayList<Long>();
		for (Branch branch : branchList)
			branchIdList.add(branch.getBranchId());
		branchIdList = branchIdList.stream().distinct().collect(Collectors.toList());

		String uri = dbConnectorMicroserviceURL + "/tab/queues/getQueueListing/branchList";
		HttpEntity<?> branchListEntity = new HttpEntity<>(branchIdList, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> getQueueListingResponse = restTemplate.postForEntity(uri, branchListEntity, List.class);

		List<QueueListingObject> queueListing = GenericMapper.convertListToObject(getQueueListingResponse.getBody(),
				new TypeReference<List<QueueListingObject>>() {
				});
		queueListing.forEach(entity -> entity.getQueue().getBranch().setServiceCenter(null));
		return new GetQueueListingResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.QUEUE_FETCHED.getMessage(), queueListing);
	}

	@Override
	public GetQueueListingResponse getQueueListingByQueueId(long queueId) throws TaboorQMSServiceException, Exception {
		Queue queue = getById(queueId);
		String uri = dbConnectorMicroserviceURL + "/tab/queues/getQueueListing/queue";
		HttpEntity<Queue> queueEntity = new HttpEntity<>(queue, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> getQueueListingResponse = restTemplate.postForEntity(uri, queueEntity, List.class);

		List<QueueListingObject> queueListing = GenericMapper.convertListToObject(getQueueListingResponse.getBody(),
				new TypeReference<List<QueueListingObject>>() {
				});
		queueListing.forEach(entity -> entity.getQueue().getBranch().setServiceCenter(null));
		return new GetQueueListingResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.QUEUE_FETCHED.getMessage(), queueListing);
	}

	private Queue getById(long queueId) throws TaboorQMSServiceException {
		String uri = dbConnectorMicroserviceURL + "/tab/queues/get/id?queueId=" + queueId;
		ResponseEntity<Queue> getQueueResponse = restTemplate.getForEntity(uri, Queue.class);
		if (!getQueueResponse.getStatusCode().equals(HttpStatus.OK) || getQueueResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorDetails());
		return getQueueResponse.getBody();

	}

	@Override
	public List<Queue> saveQueueList(List<Queue> queueList) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/queues/saveAll";
		HttpEntity<?> queueListEntity = new HttpEntity<>(queueList, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> createQueueListResponse = restTemplate.postForEntity(uri, queueListEntity, List.class);

		queueList = GenericMapper.convertListToObject(createQueueListResponse.getBody(),
				new TypeReference<List<Queue>>() {
				});
		return queueList;
	}

	@Override
	public GetValuesResposne countQueue() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
				.findByUserId(session.getUser().getUserId());
		List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

		Map<String, Object> values = new HashMap<String, Object>();
		Integer countResponse = 0;

		if (branchList.isEmpty()) {
			values.put("QueueCount", countResponse);
			return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
					xyzResponseMessage.QUEUE_FETCHED.getMessage(), values);
		}

		List<Long> branchIdList = new ArrayList<Long>();
		for (Branch branch : branchList)
			branchIdList.add(branch.getBranchId());
		branchIdList = branchIdList.stream().distinct().collect(Collectors.toList());

		countResponse = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/queues/count/branchList",
				new HttpEntity<>(branchIdList, HttpHeadersUtils.getApplicationJsonHeader()), Integer.class,
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
		values.put("QueueCount", countResponse);
		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.QUEUE_FETCHED.getMessage(),
				values);
	}

}
