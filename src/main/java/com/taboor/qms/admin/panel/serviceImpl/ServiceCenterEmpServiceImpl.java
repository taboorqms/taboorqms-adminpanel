package com.taboor.qms.admin.panel.serviceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.taboor.qms.admin.panel.payload.RegisterServiceCenterEmpPayload;
import com.taboor.qms.admin.panel.response.GetEmployeeProfileResponse;
import com.taboor.qms.admin.panel.service.EmpoyeeBranchService;
import com.taboor.qms.admin.panel.service.ServiceCenterEmpService;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterEmpBranchMapper;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetEmployeeListResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import java.util.Objects;

@Service
public class ServiceCenterEmpServiceImpl implements ServiceCenterEmpService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterEmpServiceImpl.class);

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    @Value("${url.microservice.user.management}")
    private String userManagementMicroserviceURL;

    @Autowired
    EmpoyeeBranchService employeeBranchService;

    @Autowired
    ServiceCenterEmpService serviceCenterEmpService;

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public ServiceCenterEmployee findByUserId(Long userId) throws TaboorQMSServiceException {
        String uri = dbConnectorMicroserviceURL + "/tab/servicecenteremployees/get/userId?userId=" + userId;
        ResponseEntity<ServiceCenterEmployee> getByUserIdResponse = restTemplate.getForEntity(uri,
                ServiceCenterEmployee.class);
        if (!getByUserIdResponse.getStatusCode().equals(HttpStatus.OK) || getByUserIdResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SERVICE_CENTER_EMPLOYEE_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.SERVICE_CENTER_EMPLOYEE_NOT_EXISTS.getErrorDetails());
        }
        return getByUserIdResponse.getBody();
    }

    @Override
    public ServiceCenterEmployee saveServiceCenterEmp(ServiceCenterEmployee serviceCenterEmployee)
            throws TaboorQMSServiceException {
//		HttpEntity<ServiceCenterEmployee> serviceCenterEmpEntity = new HttpEntity<>(serviceCenterEmployee,
//				HttpHeadersUtils.getApplicationJsonHeader());
//		ResponseEntity<ServiceCenterEmployee> createServiceCenterEmpResponse = restTemplate.postForEntity(uri,
//				serviceCenterEmpEntity, ServiceCenterEmployee.class);
//
//		if (!createServiceCenterEmpResponse.getStatusCode().equals(HttpStatus.OK)
//				|| createServiceCenterEmpResponse.getBody() == null)
//			throw new TaboorQMSServiceException(xyzErrorMessage.SERVICECENTEREMP_NOT_CREATED.getErrorCode() + "::"
//					+ xyzErrorMessage.SERVICECENTEREMP_NOT_CREATED.getErrorDetails());
        serviceCenterEmployee = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/servicecenteremployees/save",
                new HttpEntity<>(serviceCenterEmployee, HttpHeadersUtils.getApplicationJsonHeader()),
                ServiceCenterEmployee.class, xyzErrorMessage.SERVICECENTEREMP_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.SERVICECENTEREMP_NOT_CREATED.getErrorDetails());

        return serviceCenterEmployee;
    }

    @Override
    public List<ServiceCenterEmployee> findAdminByServiceCenter(ServiceCenter serviceCenter)
            throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException {
        String uri = dbConnectorMicroserviceURL + "/tab/servicecenteremployees/getAdmin/serviceCenter";
        HttpEntity<ServiceCenter> serviceCenterEntity = new HttpEntity<>(serviceCenter,
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<?> getServiceCenterEmpsResponse = restTemplate.postForEntity(uri, serviceCenterEntity,
                List.class);
        List<ServiceCenterEmployee> employeeList = GenericMapper.convertListToObject(
                getServiceCenterEmpsResponse.getBody(), new TypeReference<List<ServiceCenterEmployee>>() {
        });
        return employeeList;
    }

    @Override
    public GetEmployeeProfileResponse getProfile() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = findByUserId(session.getUser().getUserId());
        List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

        int agentCount = 0;
        if (serviceCenterEmployee.getUser().getRoleName().equals(RoleName.Service_Center_Admin.name())) {
            agentCount = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/agents/count/serviceCenterId?serviceCenterId="
                    + serviceCenterEmployee.getServiceCenter().getServiceCenterId(),
                    Integer.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                    xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        } else {
            List<Long> branchIdList = new ArrayList<Long>();
            for (Branch branch : branchList) {
                branchIdList.add(branch.getBranchId());
            }

            String uri = dbConnectorMicroserviceURL + "/tab/agents/count/branchList";
            HttpEntity<?> httpEntity = new HttpEntity<>(branchIdList, HttpHeadersUtils.getApplicationJsonHeader());
            ResponseEntity<Integer> countAgentResponse = restTemplate.postForEntity(uri, httpEntity, Integer.class);
            if (!countAgentResponse.getStatusCode().equals(HttpStatus.OK)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                        + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
            }
            agentCount = countAgentResponse.getBody();
        }

        User user = serviceCenterEmployee.getUser();

        String uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/get/user?userId=" + user.getUserId();
        ResponseEntity<?> getUserPrivilegeResponse = restTemplate.getForEntity(uri, List.class);
        if (!getUserPrivilegeResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        List<UserPrivilege> privilegeList = GenericMapper.convertListToObject(getUserPrivilegeResponse.getBody(),
                new TypeReference<List<UserPrivilege>>() {
        });

        user.setPassword(null);
        user.setRoleName(user.getRoleName().replaceAll("_", " "));

        return new GetEmployeeProfileResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_FETCHED.getMessage(), user,
                serviceCenterEmployee.getServiceCenter().getCountry(), branchList.size(), agentCount, privilegeList);
    }

    @Override
    public GetEmployeeListResponse getList() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());

        String uri = dbConnectorMicroserviceURL + "/tab/servicecenteremployees/getList";
        HttpEntity<ServiceCenterEmployee> serviceCenterEmpEntity = new HttpEntity<>(serviceCenterEmployee,
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<GetEmployeeListResponse> response = restTemplate.postForEntity(uri, serviceCenterEmpEntity,
                GetEmployeeListResponse.class);
        response.getBody().getEmployees().forEach(emp -> {
            emp.getEmployee().getUser().setRoleName(emp.getEmployee().getUser().getRoleName().replaceAll("_", " "));
            emp.getBranchList().forEach(branch -> branch.setServiceCenter(null));
            emp.getEmployee().setServiceCenter(null);
        });
        return response.getBody();
    }

    @Override
    public GetValuesResposne registerEmp(RegisterServiceCenterEmpPayload registerServiceCenterEmpPayload,
            MultipartFile profileImage) throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee employee = serviceCenterEmpService.findByUserId(session.getUser().getUserId());

        registerServiceCenterEmpPayload.getBranchIds().add(employee.getServiceCenter().getServiceCenterId());
        List<?> branchIdsTemp = RestUtil.postRequest(
                dbConnectorMicroserviceURL + "/tab/employeebranchmapper/checkBranchUserLimit",
                new HttpEntity<>(registerServiceCenterEmpPayload.getBranchIds(),
                        HttpHeadersUtils.getApplicationJsonHeader()),
                List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<Long> branchIds = GenericMapper.convertListToObject(branchIdsTemp, new TypeReference<List<Long>>() {
        });

        if (branchIds.isEmpty()) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_USER_LIMIT_EXCEEDS.getErrorCode() + "::"
                    + xyzErrorMessage.BRANCH_USER_LIMIT_EXCEEDS.getErrorDetails());
        }

        UserRole userRole = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/userroles/get/id?roleId="
                + registerServiceCenterEmpPayload.getRoleId(),
                UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());

        RegisterUserPayload registerUserPayload = new RegisterUserPayload();
        registerUserPayload.setName(registerServiceCenterEmpPayload.getName());
        registerUserPayload.setEmail(registerServiceCenterEmpPayload.getEmail());
        registerUserPayload.setPassword(registerServiceCenterEmpPayload.getPassword());
        registerUserPayload.setPhoneNumber(registerServiceCenterEmpPayload.getPhoneNumber());
        registerUserPayload.setUserRole(userRole);
        registerUserPayload.setAssignRolePrivileges(true);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("RegisterUserPayload", registerUserPayload);
        if (profileImage == null) {
            body.add("UserProfileImage", null);
        } else {
            body.add("UserProfileImage", profileImage.getResource());
        }

        RegisterUserResponse registerUserResponse = RestUtil.postRequest(
                userManagementMicroserviceURL + "/user/register",
                new HttpEntity<>(body, HttpHeadersUtils.getMultipartFormDataHeader()), RegisterUserResponse.class,
                xyzErrorMessage.USER_NOT_REGISTERED.getErrorCode(),
                xyzErrorMessage.USER_NOT_REGISTERED.getErrorDetails());

        employee = createEmp(registerUserResponse.getUser(), employee.getServiceCenter(), branchIds);

        RSocketPayload payload = new RSocketPayload();
        payload.setNotificationSubject(RSocketNotificationSubject.NEW_SERVICECENTER_USER);
        payload.setRecipientId(employee.getServiceCenter().getServiceCenterId());
        payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
        payload.setInformation(Arrays.asList(employee.getUser().getName()));

//		RSocketInitializer.fireAndForget(payload);
        RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_EMP_ADDED.getMessage(), null);
    }

    @Override
    public GenStatusResponse updateEmp(RegisterServiceCenterEmpPayload registerServiceCenterEmpPayload)
            throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        RegisterUserPayload registerUserPayload = new RegisterUserPayload();
        if (registerServiceCenterEmpPayload.getUserId() == -1) {
            registerUserPayload.setUser(session.getUser());
        } else {
            registerUserPayload.setUser(RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/users/get/id?userId="
                    + registerServiceCenterEmpPayload.getUserId(),
                    User.class, xyzErrorMessage.USER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails()));
        }
        registerUserPayload.setName(registerServiceCenterEmpPayload.getName());
        
        if(registerServiceCenterEmpPayload.getEmail() != null && !registerServiceCenterEmpPayload.getEmail().equals("")){
            if(!registerUserPayload.getUser().getEmail().equals(registerServiceCenterEmpPayload.getEmail())){
                registerUserPayload.setEmail(registerServiceCenterEmpPayload.getEmail());
            }
        } else{
            registerUserPayload.setEmail(null);
        }
        
//		registerUserPayload.setPassword(registerServiceCenterEmpPayload.getPassword());
        registerUserPayload.setPhoneNumber(registerServiceCenterEmpPayload.getPhoneNumber());
//        logged in user can not change their role
        if(!Objects.equals(session.getUser().getUserId(), registerUserPayload.getUser().getUserId()) &&
                !registerUserPayload.getUser().getRoleName().equalsIgnoreCase(RoleName.Service_Center_Admin.name())){
            if (registerServiceCenterEmpPayload.getRoleId() != -1) {
                UserRole userRole = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/userroles/get/id?roleId="
                        + registerServiceCenterEmpPayload.getRoleId(),
                        UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
                        xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());
                registerUserPayload.setUserRole(userRole);
            } else {
                registerUserPayload.setUserRole(null);
            }
        }else {
            return new GetValuesResposne(xyzResponseCode.ERROR.getCode(),
                xyzResponseMessage.SERVICE_CENTER_EMP_ERROR.getMessage(), null);
        }
        
        registerUserPayload.setAssignRolePrivileges(true);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("UpdateUserPayload", registerUserPayload);
        body.add("UserProfileImage", null);

        RegisterUserResponse registerUserResponse = RestUtil.postRequest(userManagementMicroserviceURL + "/user/update",
                new HttpEntity<>(body, HttpHeadersUtils.getMultipartFormDataHeader()), RegisterUserResponse.class,
                xyzErrorMessage.USER_NOT_UPDATED.getErrorCode(), xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());

        ServiceCenterEmployee employee = findByUserId(registerUserResponse.getUser().getUserId());

        if (!registerServiceCenterEmpPayload.getBranchIds().isEmpty()) {
            if (registerServiceCenterEmpPayload.getBranchIds().get(0) != -1) {
                List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMappers = new ArrayList<ServiceCenterEmpBranchMapper>();
                for (Long branchId : registerServiceCenterEmpPayload.getBranchIds()) {
                    ServiceCenterEmpBranchMapper serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
                    Branch branch = new Branch();
                    branch.setBranchId(branchId);
                    serviceCenterEmpBranchMapper.setBranch(branch);
                    serviceCenterEmpBranchMapper.setServiceCenterEmployee(employee);
                    serviceCenterEmpBranchMappers.add(serviceCenterEmpBranchMapper);
                }
                employeeBranchService.saveEmployeeBranchMapperList(serviceCenterEmpBranchMappers, true);
            }
        }

        RSocketPayload payload = new RSocketPayload();
        payload.setNotificationSubject(RSocketNotificationSubject.UPDATE_SERVICECENTER_USER);
        payload.setRecipientId(employee.getServiceCenter().getServiceCenterId());
        payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
        payload.setInformation(Arrays.asList(employee.getUser().getName()));

//		RSocketInitializer.fireAndForget(payload);
        RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.SERVICE_CENTER_EMP_UPDATED.getMessage(), null);
    }

    @Override
    public ServiceCenterEmployee createEmp(User user, ServiceCenter serviceCenter, List<Long> branchIds)
            throws TaboorQMSServiceException, Exception {
        ServiceCenterEmployee serviceCenterEmployee = new ServiceCenterEmployee();
        serviceCenterEmployee.setEmployeeNumber(nextEmpNumber(serviceCenter));
        serviceCenterEmployee.setServiceCenter(serviceCenter);
        serviceCenterEmployee.setUser(user);

        serviceCenterEmployee = saveServiceCenterEmp(serviceCenterEmployee);

        if (!branchIds.isEmpty()) {
            List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMappers = new ArrayList<ServiceCenterEmpBranchMapper>();
            for (Long branchId : branchIds) {
                ServiceCenterEmpBranchMapper serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
                Branch branch = new Branch();
                branch.setBranchId(branchId);
                serviceCenterEmpBranchMapper.setBranch(branch);
                serviceCenterEmpBranchMapper.setServiceCenterEmployee(serviceCenterEmployee);
                serviceCenterEmpBranchMappers.add(serviceCenterEmpBranchMapper);
            }

            employeeBranchService.saveEmployeeBranchMapperList(serviceCenterEmpBranchMappers, false);
        }

        // Add Configurations & Notification Types
        UserConfiguration userConfiguration = new UserConfiguration();
        userConfiguration.setUser(serviceCenterEmployee.getUser());
        userConfiguration.setEmailAllowed(false);
        userConfiguration.setInAppNotificationAllowed(false);
        userConfiguration.setSmsAllowed(false);

        userConfiguration = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userconfigurations/save",
                new HttpEntity<>(userConfiguration, HttpHeadersUtils.getApplicationJsonHeader()),
                UserConfiguration.class, xyzErrorMessage.USERCONFIGURATION_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.USERCONFIGURATION_NOT_CREATED.getErrorDetails());

        UserNotificationType userNotificationType = new UserNotificationType();
        userNotificationType.setUser(serviceCenterEmployee.getUser());
        userNotificationType.setCustomerFeedback1Star(false);
        userNotificationType.setCustomerFeedback2Star(false);
        userNotificationType.setCustomerFeedback3Star(false);
        userNotificationType.setCustomerFeedback4Star(false);
        userNotificationType.setCustomerFeedback5Star(false);
        userNotificationType.setUpdateSupportTicket(false);
        userNotificationType.setNewOrUpdateAgent(false);
        userNotificationType.setNewOrUpdateBranch(false);
        userNotificationType.setNewOrUpdateUser(false);

        userNotificationType = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/usernotificationtypes/save",
                new HttpEntity<>(userNotificationType, HttpHeadersUtils.getApplicationJsonHeader()),
                UserNotificationType.class, xyzErrorMessage.USERNOTIFICATIONTYPE_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.USERNOTIFICATIONTYPE_NOT_CREATED.getErrorDetails());
        return serviceCenterEmployee;
    }

    @Override
    public GetValuesResposne getNextEmpNumber() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee employee = serviceCenterEmpService.findByUserId(session.getUser().getUserId());

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("NextEmpNumber", nextEmpNumber(employee.getServiceCenter()));
        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), "Next Employee Number fetched Successfully.",
                values);

    }

    @Override
    public String nextEmpNumber(ServiceCenter serviceCenter) throws RestClientException, TaboorQMSServiceException {
        String nextEmpNumber = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/servicecenteremployees/getNextEmpNumber?serviceCenterId="
                + serviceCenter.getServiceCenterId(),
                String.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        if (nextEmpNumber.equals("FirstEntry")) {
            nextEmpNumber = serviceCenter.getServiceCenterName().substring(0, 2).toUpperCase() + "0001";
        }
        return nextEmpNumber;
    }
    
    @Override
    public GenStatusResponse deleteEmployee(String userId) throws TaboorQMSServiceException{
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        User sessionUser = myUserDetails.getSession().getUser();
//        Logged in user can not delete themselves
        if(!Objects.equals(sessionUser.getUserId(), Long.valueOf(userId))){
            User user = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/users/get/id?userId="+ userId,
                    User.class, xyzErrorMessage.USER_NOT_EXISTS.getErrorCode(), xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
//            Taboor Admin can not be deleted
            if(!user.getRoleName().equalsIgnoreCase(RoleName.Taboor_Admin.name())){
                //        Deleting from user table
                ResponseEntity<Integer> entity1 = restTemplate.getForEntity(dbConnectorMicroserviceURL + "/tab/users/delete?userId="
                                + userId, int.class);
                if (!entity1.getStatusCode().equals(HttpStatus.OK) || entity1.getBody() == null){
                    throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() 
                                + "::" + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
                }else{
                    if(!entity1.getBody().equals(1)){
                        throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_DELETED.getErrorCode() 
                                + "::" + xyzErrorMessage.USER_NOT_DELETED.getErrorDetails());
                    }
                }
//        //        deleting from service center emplyee table
//                ResponseEntity<Integer> entity2 = restTemplate.getForEntity(dbConnectorMicroserviceURL + "/tab/servicecenteremployees/delete?userId="
//                                + userId, int.class);
//                if (!entity2.getStatusCode().equals(HttpStatus.OK) || entity2.getBody() == null){
//                    throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() 
//                                + "::" + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
//                }
//
//        //        deleting from taboor emplyee table
//                ResponseEntity<Integer> entity3 = restTemplate.getForEntity(dbConnectorMicroserviceURL + "/tab/tabooremployees/delete?userId="
//                                + userId, int.class);
//                if (!entity3.getStatusCode().equals(HttpStatus.OK) || entity3.getBody() == null){
//                    throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() 
//                                + "::" + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
//                }
                return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_DELETED.getMessage());
            } else{
                throw new TaboorQMSServiceException(xyzErrorMessage.TABBOR_ADMIN_CANNOT_BE_DELETED.getErrorCode() 
                                + "::" + xyzErrorMessage.TABBOR_ADMIN_CANNOT_BE_DELETED.getErrorDetails());
            }
        }else{
            throw new TaboorQMSServiceException(xyzErrorMessage.SESSION_USER_CANNOT_BE_DELETED.getErrorCode() 
                                + "::" + xyzErrorMessage.SESSION_USER_CANNOT_BE_DELETED.getErrorDetails());
        }
    }
}
