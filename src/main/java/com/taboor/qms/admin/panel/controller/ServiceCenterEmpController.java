package com.taboor.qms.admin.panel.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.admin.panel.payload.RegisterServiceCenterEmpPayload;
import com.taboor.qms.admin.panel.response.GetEmployeeProfileResponse;
import com.taboor.qms.admin.panel.service.ServiceCenterEmpService;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetEmployeeListResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.GenericMapper;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/serviceCenterEmp")
@CrossOrigin(origins = "*")
public class ServiceCenterEmpController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceCenterEmpController.class);

	@Autowired
	ServiceCenterEmpService serviceCenterEmpService;

	@GetMapping("/getProfile")
	public @ResponseBody GetEmployeeProfileResponse getEmployeeProfile() throws Exception, Throwable {
		logger.info("Calling Get Service Center Employee Profile - API");
		return serviceCenterEmpService.getProfile();
	}

	@GetMapping("/getList")
	public @ResponseBody GetEmployeeListResponse getEmployeeList() throws Exception, Throwable {
		logger.info("Calling Get Employees - API");
		return serviceCenterEmpService.getList();
	}

	@ApiOperation(value = "Add Service Center Bank Card.", response = RegisterServiceCenterEmpPayload.class)
	@PostMapping(value = "/register", consumes = { "multipart/form-data" }, produces = "application/json")
	public @ResponseBody GetValuesResposne registerServiceCenterEmp(
			@RequestParam(value = "RegisterServiceCenterEmpPayload", required = true) @Valid final String payload,
			@RequestParam(value = "ProfileImage", required = false) @Valid final MultipartFile profileImage)
			throws Exception, Throwable {
		logger.info("Calling Register Service Center Emp - API");
		RegisterServiceCenterEmpPayload registerServiceCenterEmpPayload = GenericMapper.jsonToObjectMapper(payload,
				RegisterServiceCenterEmpPayload.class);
		return serviceCenterEmpService.registerEmp(registerServiceCenterEmpPayload, profileImage);
	}

	@PostMapping("/update")
	public @ResponseBody GenStatusResponse updateServiceCenterEmp(
			@RequestBody RegisterServiceCenterEmpPayload registerServiceCenterEmpPayload) throws Exception, Throwable {
		logger.info("Calling Update Service Center Emp - API");
		return serviceCenterEmpService.updateEmp(registerServiceCenterEmpPayload);
	}

	@GetMapping("/getNextEmpNumber")
	public @ResponseBody GetValuesResposne getNextEmpNumber() throws Exception, Throwable {
		logger.info("Calling Get Next Employee Number - API");
		return serviceCenterEmpService.getNextEmpNumber();
	}
        
        @GetMapping("/delete")
	public @ResponseBody GenStatusResponse getNextEmpNumber(@RequestParam String userId) throws Exception, Throwable {
		logger.info("Calling Get Next Employee Number - API");
		return serviceCenterEmpService.deleteEmployee(userId);
	}

}
