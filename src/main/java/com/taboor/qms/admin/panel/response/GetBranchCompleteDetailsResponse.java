package com.taboor.qms.admin.panel.response;

import java.util.Collection;
import java.util.List;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchWorkingHours;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAgentListResponse.AgentObject;
import com.taboor.qms.core.response.GetBranchCounterListResponse.BranchCounterObject;

public class GetBranchCompleteDetailsResponse extends GenStatusResponse {

	private Branch branch;
	private int totalTicketCount;
	private List<BranchServicesCompleteObj> branchServicesCompleteObjs;
	private List<BranchWorkingHours> branchWorkingHourList;
	private List<AgentObject> agentList;
	private List<BranchCounterObject> counterList;

	public static class BranchServicesCompleteObj {

		private ServiceTAB serviceTAB;
		private Collection<ServiceRequirement> serviceTABRequiredDocumentMapperList;

		public ServiceTAB getServiceTAB() {
			return serviceTAB;
		}

		public void setServiceTAB(ServiceTAB serviceTAB) {
			this.serviceTAB = serviceTAB;
		}

		public Collection<ServiceRequirement> getServiceTABRequiredDocumentMapperList() {
			return serviceTABRequiredDocumentMapperList;
		}

		public void setServiceTABRequiredDocumentMapperList(
				Collection<ServiceRequirement> serviceTABRequiredDocumentMapperList) {
			this.serviceTABRequiredDocumentMapperList = serviceTABRequiredDocumentMapperList;
		}

	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public List<BranchServicesCompleteObj> getBranchServicesCompleteObjs() {
		return branchServicesCompleteObjs;
	}

	public void setBranchServicesCompleteObjs(List<BranchServicesCompleteObj> branchServicesCompleteObjs) {
		this.branchServicesCompleteObjs = branchServicesCompleteObjs;
	}

	public List<BranchWorkingHours> getBranchWorkingHourList() {
		return branchWorkingHourList;
	}

	public List<AgentObject> getAgentList() {
		return agentList;
	}

	public void setAgentList(List<AgentObject> agentList) {
		this.agentList = agentList;
	}

	public List<BranchCounterObject> getCounterList() {
		return counterList;
	}

	public void setCounterList(List<BranchCounterObject> counterList) {
		this.counterList = counterList;
	}

	public void setBranchWorkingHourList(List<BranchWorkingHours> branchWorkingHourList) {
		this.branchWorkingHourList = branchWorkingHourList;
	}

	public int getTotalTicketCount() {
		return totalTicketCount;
	}

	public void setTotalTicketCount(int totalTicketCount) {
		this.totalTicketCount = totalTicketCount;
	}

	public GetBranchCompleteDetailsResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetBranchCompleteDetailsResponse(int applicationStatusCode, String applicationStatusResponse, Branch branch,
			int totalTicketCount, List<BranchServicesCompleteObj> branchServicesCompleteObjs,
			List<BranchWorkingHours> branchWorkingHourList, List<AgentObject> agentList,
			List<BranchCounterObject> counterList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branch = branch;
		this.totalTicketCount = totalTicketCount;
		this.branchServicesCompleteObjs = branchServicesCompleteObjs;
		this.branchWorkingHourList = branchWorkingHourList;
		this.agentList = agentList;
		this.counterList = counterList;
	}

}
