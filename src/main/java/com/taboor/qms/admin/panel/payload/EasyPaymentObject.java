package com.taboor.qms.admin.panel.payload;

import java.time.OffsetDateTime;

import com.taboor.qms.core.utils.Currency;

public class EasyPaymentObject {

	private Double amountPercentage;
	private Double amountValue;
	private OffsetDateTime dueDate;
	private int graceDays;
	private Currency currency;

	public Double getAmountPercentage() {
		return amountPercentage;
	}

	public void setAmountPercentage(Double amountPercentage) {
		this.amountPercentage = amountPercentage;
	}

	public Double getAmountValue() {
		return amountValue;
	}

	public void setAmountValue(Double amountValue) {
		this.amountValue = amountValue;
	}

	public OffsetDateTime getDueDate() {
		return dueDate;
	}

	public void setDueDate(OffsetDateTime dueDate) {
		this.dueDate = dueDate;
	}

	public int getGraceDays() {
		return graceDays;
	}

	public void setGraceDays(int graceDays) {
		this.graceDays = graceDays;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
