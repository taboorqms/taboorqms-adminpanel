package com.taboor.qms.admin.panel.controller;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.payload.AddInvoicePayload;
import com.taboor.qms.admin.panel.response.GetInvoiceListResponse;
import com.taboor.qms.admin.panel.service.InvoiceService;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@RestController
@RequestMapping("/invoice")
@CrossOrigin(origins = "*")
public class InvoiceController {

	private static final Logger logger = LoggerFactory.getLogger(InvoiceController.class);

	@Autowired
	InvoiceService invoiceService;

	@GetMapping("/getListing")
	public @ResponseBody GetInvoiceListResponse getAll() throws Exception, Throwable {
		logger.info("Calling Get Invoice Listing - API");
		return invoiceService.getAll();
	}

	@PostMapping("/getDetails")
	public @ResponseBody GetInvoiceListResponse getById(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Invoice Details - API");
		// 0- InvoiceId
		return invoiceService.getById(getByIdPayload.getId());
	}

	@PostMapping("/get/serviceCenter")
	public @ResponseBody GetInvoiceListResponse getByServiceCenterId(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Invoice By ServiceCenterId - API");
		// 0- serviceCenterId
		return invoiceService.getByServiceCenterId(getByIdPayload.getId());
	}

	@PostMapping("/addOrUpdate")
	public @ResponseBody GetValuesResposne addOrUpdate(@RequestBody AddInvoicePayload addInvoicePayload)
			throws Exception, Throwable {
		logger.info("Calling Add or Update Invoice - API");
		return invoiceService.addOrUpdate(addInvoicePayload);
	}

	@PostMapping("/delete")
	public @ResponseBody GenStatusResponse deleteInvoice(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Invoice - API");
		return invoiceService.deleteInvoice(getByIdPayload.getId());
	}

	@GetMapping("/getNextInvoiceNumber")
	public @ResponseBody GetValuesResposne getNextInvoiceNumber() throws Exception, Throwable {
		logger.info("Calling Get Next Invoice Number - API");
		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.INVOICE_FETCHED.getMessage(),
				Collections.singletonMap("NextInvoiceNumber", invoiceService.getNextInvoiceNumber()));
	}

}
