package com.taboor.qms.admin.panel.payload;

import java.util.List;

import com.taboor.qms.core.model.ServiceTAB;

public class AddBranchCounterPayload {

	private Long branchId;

	private Long counterId;
	private Boolean update;

	private List<ServiceTAB> services;

	public Long getCounterId() {
		return counterId;
	}

	public void setCounterId(Long counterId) {
		this.counterId = counterId;
	}

	public Boolean getUpdate() {
		return update;
	}

	public void setUpdate(Boolean update) {
		this.update = update;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public List<ServiceTAB> getServices() {
		return services;
	}

	public void setServices(List<ServiceTAB> services) {
		this.services = services;
	}

	public AddBranchCounterPayload() {
		super();
	}

	public AddBranchCounterPayload(Long branchId, List<ServiceTAB> services, Boolean update) {
		super();
		this.branchId = branchId;
		this.services = services;
		this.update = update;
	}

}
