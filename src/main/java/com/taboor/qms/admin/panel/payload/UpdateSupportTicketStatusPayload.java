package com.taboor.qms.admin.panel.payload;

import com.taboor.qms.core.utils.SupportTicketStatus;

public class UpdateSupportTicketStatusPayload {

	private Long supportTicketId;
	private SupportTicketStatus supportTicketStatus;
	private String reason;

	public Long getSupportTicketId() {
		return supportTicketId;
	}

	public void setSupportTicketId(Long supportTicketId) {
		this.supportTicketId = supportTicketId;
	}

	public SupportTicketStatus getSupportTicketStatus() {
		return supportTicketStatus;
	}

	public void setSupportTicketStatus(SupportTicketStatus supportTicketStatus) {
		this.supportTicketStatus = supportTicketStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
