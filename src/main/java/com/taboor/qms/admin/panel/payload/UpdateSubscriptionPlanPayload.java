package com.taboor.qms.admin.panel.payload;

public class UpdateSubscriptionPlanPayload {

	private Long serviceCenterId;
	private Boolean kioskEnabled;
	private Boolean EasyPaymentEnabled;
	private Boolean advanceReportingEnabled;
	private int noOfBranches;
	private int noOfUsersPerBranch;
	private int noOfSMS;
	private Double chargesRemaining;
	private Double totalChargesBeforeTax;
	private Double totalCharges;
	
	public Long getServiceCenterId() {
		return serviceCenterId;
	}
	public void setServiceCenterId(Long serviceCenterId) {
		this.serviceCenterId = serviceCenterId;
	}
	public Boolean getKioskEnabled() {
		return kioskEnabled;
	}
	public void setKioskEnabled(Boolean kioskEnabled) {
		this.kioskEnabled = kioskEnabled;
	}
	public Boolean getEasyPaymentEnabled() {
		return EasyPaymentEnabled;
	}
	public void setEasyPaymentEnabled(Boolean easyPaymentEnabled) {
		EasyPaymentEnabled = easyPaymentEnabled;
	}
	public Boolean getAdvanceReportingEnabled() {
		return advanceReportingEnabled;
	}
	public void setAdvanceReportingEnabled(Boolean advanceReportingEnabled) {
		this.advanceReportingEnabled = advanceReportingEnabled;
	}
	public int getNoOfBranches() {
		return noOfBranches;
	}
	public void setNoOfBranches(int noOfBranches) {
		this.noOfBranches = noOfBranches;
	}
	public int getNoOfUsersPerBranch() {
		return noOfUsersPerBranch;
	}
	public void setNoOfUsersPerBranch(int noOfUsersPerBranch) {
		this.noOfUsersPerBranch = noOfUsersPerBranch;
	}
	public int getNoOfSMS() {
		return noOfSMS;
	}
	public void setNoOfSMS(int noOfSMS) {
		this.noOfSMS = noOfSMS;
	}
	public Double getChargesRemaining() {
		return chargesRemaining;
	}
	public void setChargesRemaining(Double chargesRemaining) {
		this.chargesRemaining = chargesRemaining;
	}
	public Double getTotalChargesBeforeTax() {
		return totalChargesBeforeTax;
	}
	public void setTotalChargesBeforeTax(Double totalChargesBeforeTax) {
		this.totalChargesBeforeTax = totalChargesBeforeTax;
	}
	public Double getTotalCharges() {
		return totalCharges;
	}
	public void setTotalCharges(Double totalCharges) {
		this.totalCharges = totalCharges;
	}
	
}
