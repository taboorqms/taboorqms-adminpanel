package com.taboor.qms.admin.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.response.GetTaboorBankAccountListResponse;
import com.taboor.qms.admin.panel.service.TaboorBankAccountService;

@RestController
@RequestMapping("/bankAccount")
@CrossOrigin(origins = "*")
public class TaboorBankAccountController {

	private static final Logger logger = LoggerFactory.getLogger(TaboorBankAccountController.class);

	@Autowired
	TaboorBankAccountService taboorBankAccountService;

	@GetMapping("/get/all")
	public @ResponseBody GetTaboorBankAccountListResponse getAll() throws Exception, Throwable {
		logger.info("Calling Get All Taboor Bank Account - API");
		return taboorBankAccountService.getAll();
	}

}
