package com.taboor.qms.admin.panel.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.admin.panel.response.GetServiceSectorListResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;

@Service
public interface ServiceSectorService {

	GetServiceSectorListResponse getAllServiceSectors() throws TaboorQMSServiceException, Exception;

}
