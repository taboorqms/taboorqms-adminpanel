package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.ServiceCenterServiceTABMapper;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetServiceCenterServiceListResponse extends GenStatusResponse {

	private List<ServiceCenterServiceTABMapper> services;

	public List<ServiceCenterServiceTABMapper> getServices() {
		return services;
	}

	public void setServices(List<ServiceCenterServiceTABMapper> services) {
		this.services = services;
	}

	public GetServiceCenterServiceListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<ServiceCenterServiceTABMapper> services) {
		super(applicationStatusCode, applicationStatusResponse);
		this.services = services;
	}

}
