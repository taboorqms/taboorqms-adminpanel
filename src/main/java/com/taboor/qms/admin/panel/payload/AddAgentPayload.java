package com.taboor.qms.admin.panel.payload;

import java.util.List;

import com.taboor.qms.core.model.UserPrivilege;

public class AddAgentPayload {

	private String agentId;
	private String agentPin;
	private String agentName;
	private String email;
	private Long branchId;
	private Long assignedCounterId;
	private List<UserPrivilege> privileges;

	private int updateStatus;
	private Long agentPk;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentPin() {
		return agentPin;
	}

	public void setAgentPin(String agentPin) {
		this.agentPin = agentPin;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public List<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

	public Long getAssignedCounterId() {
		return assignedCounterId;
	}

	public void setAssignedCounterId(Long assignedCounterId) {
		this.assignedCounterId = assignedCounterId;
	}

	public int getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(int updateStatus) {
		this.updateStatus = updateStatus;
	}

	public Long getAgentPk() {
		return agentPk;
	}

	public void setAgentPk(Long agentPk) {
		this.agentPk = agentPk;
	}

}
