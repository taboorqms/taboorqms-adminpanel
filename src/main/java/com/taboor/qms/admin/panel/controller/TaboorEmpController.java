package com.taboor.qms.admin.panel.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.admin.panel.payload.RegisterTaboorEmpPayload;
import com.taboor.qms.admin.panel.response.GetEmployeeProfileResponse;
import com.taboor.qms.admin.panel.service.TaboorEmpService;
import com.taboor.qms.core.response.GetTaboorEmployeeListResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.GenericMapper;

@RestController
@RequestMapping("/taboorEmployee")
@CrossOrigin(origins = "*")
public class TaboorEmpController {

	private static final Logger logger = LoggerFactory.getLogger(TaboorEmpController.class);

	@Autowired
	TaboorEmpService taboorEmpService;

	@GetMapping("/getProfile")
	public @ResponseBody GetEmployeeProfileResponse getEmployeeProfile() throws Exception, Throwable {
		logger.info("Calling Get Taboor Employee Profile - API");
		return taboorEmpService.getProfile();
	}

	@GetMapping("/getListing")
	public @ResponseBody GetTaboorEmployeeListResponse getEmployeeListing() throws Exception, Throwable {
		logger.info("Calling Get Taboor Employees - API");
		return taboorEmpService.getListing();
	}

	@GetMapping("/getList")
	public @ResponseBody GetTaboorEmployeeListResponse getEmployeeList() throws Exception, Throwable {
		logger.info("Calling Get Taboor Employees - API");
		return taboorEmpService.getList();
	}

	@PostMapping(value = "/register", consumes = { "multipart/form-data" }, produces = "application/json")
	public @ResponseBody GetValuesResposne registerTaboorEmp(
			@RequestParam(value = "RegisterTaboorEmpPayload", required = true) @Valid final String payload,
			@RequestParam(value = "ProfileImage", required = false) @Valid final MultipartFile profileImage)
			throws Exception, Throwable {
		logger.info("Calling Register Taboor Emp - API");
		RegisterTaboorEmpPayload registerTaboorEmpPayload = GenericMapper.jsonToObjectMapper(payload,
				RegisterTaboorEmpPayload.class);
		return taboorEmpService.registerEmp(registerTaboorEmpPayload, profileImage);
	}

	@PostMapping(value = "/update")
	public @ResponseBody GetValuesResposne updateTaboorEmp(
			@RequestBody RegisterTaboorEmpPayload registerTaboorEmpPayload) throws Exception, Throwable {
		logger.info("Calling Update Taboor Emp - API");
		return taboorEmpService.updateEmp(registerTaboorEmpPayload);
	}

	@GetMapping("/getNextEmpNumber")
	public @ResponseBody GetValuesResposne getNextEmpNumber() throws Exception, Throwable {
		logger.info("Calling Get Next Employee Number - API");
		return taboorEmpService.getNextEmpNumber();
	}

}
