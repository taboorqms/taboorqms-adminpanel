package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetUserPrivilegesListResponse extends GenStatusResponse {

	List<UserPrivilege> privilegeList;

	public List<UserPrivilege> getPrivilegeList() {
		return privilegeList;
	}

	public void setPrivilegeList(List<UserPrivilege> privilegeList) {
		this.privilegeList = privilegeList;
	}

	public GetUserPrivilegesListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<UserPrivilege> privilegeList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.privilegeList = privilegeList;
	}

}
