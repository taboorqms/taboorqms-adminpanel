package com.taboor.qms.admin.panel.payload;

public class AddSupportTicketPayload {

	private String subject;
	private String description;
	private Long supportTicketId;

	public Long getSupportTicketId() {
		return supportTicketId;
	}

	public void setSupportTicketId(Long supportTicketId) {
		this.supportTicketId = supportTicketId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
