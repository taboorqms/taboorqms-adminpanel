package com.taboor.qms.admin.panel.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.ServiceRequirement;

@Service
public interface RequiredDocumentService {

	public ServiceRequirement getRequiredDocument(@Valid Long requiredDocumentId)
			throws TaboorQMSServiceException, Exception;

	public ServiceRequirement saveRequiredDocument(@Valid ServiceRequirement requiredDocument)
			throws TaboorQMSServiceException, Exception;

}
