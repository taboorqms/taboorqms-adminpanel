package com.taboor.qms.admin.panel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceCenterEmpBranchMapper;

@Service
public interface EmpoyeeBranchService {

	ServiceCenterEmpBranchMapper saveEmployeeBranchMapper(ServiceCenterEmpBranchMapper userBranchMapper)
			throws TaboorQMSServiceException, Exception;

	List<ServiceCenterEmpBranchMapper> saveEmployeeBranchMapperList(
			List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMapperList, Boolean update)
			throws TaboorQMSServiceException, Exception;

	List<Branch> getBranchByEmp(Long employeeId) throws TaboorQMSServiceException, Exception;

	int countBranchByEmp(Long employeeId) throws TaboorQMSServiceException, Exception;

}
