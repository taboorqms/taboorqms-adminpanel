//package com.taboor.qms.admin.panel;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.taboor.qms.core.utils.GenericMapper;
//import com.taboor.qms.core.utils.RSocketPayload;
//import com.taboor.qms.core.utils.TaboorQMSConfigurations;
//
//import io.rsocket.RSocket;
//import io.rsocket.RSocketFactory;
//import io.rsocket.transport.netty.client.TcpClientTransport;
//import io.rsocket.util.DefaultPayload;
//
//public class RSocketInitializer {
//	public static RSocket socket;
//
//	public static void fireAndForget(RSocketPayload rSocketPayload) throws JsonProcessingException {
//		if (socket == null)
//			try {
//				socket = RSocketFactory.connect()
//						.transport(TcpClientTransport.create(TaboorQMSConfigurations.RSocket_HOST.getValue(),
//								Integer.valueOf(TaboorQMSConfigurations.RSocket_PORT.getValue())))
//						.start().block();
//
//			} catch (Exception e) {
//				System.err.println(e);
//			}
//		if (socket != null)
//			socket.fireAndForget(DefaultPayload.create(GenericMapper.objectToJSONMapper(rSocketPayload))).subscribe();
//
//	}
//
//}
