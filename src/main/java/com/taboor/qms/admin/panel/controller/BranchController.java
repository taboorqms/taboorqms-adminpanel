package com.taboor.qms.admin.panel.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.payload.AddBranchPayload;
import com.taboor.qms.admin.panel.payload.AddBranchServicePayload;
import com.taboor.qms.admin.panel.payload.CloneBranchPayload;
import com.taboor.qms.admin.panel.response.GetBranchCompleteDetailsResponse;
import com.taboor.qms.admin.panel.service.BranchService;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetBranchListingResponse;
import com.taboor.qms.core.response.GetServiceListResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@RestController
@RequestMapping("/branch")
@CrossOrigin(origins = "*")
public class BranchController {

	private static final Logger logger = LoggerFactory.getLogger(BranchController.class);

	@Autowired
	BranchService branchService;

	@PostMapping("/getServices")
	public @ResponseBody GetServiceListResponse getServices(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Branch Provided Services - API");
		// 0-BranchId
		return branchService.getProvidedServices(getByIdPayload.getId());
	}

	@PostMapping("/add")
	public @ResponseBody GetValuesResposne addBranch(@RequestBody final AddBranchPayload addBranchPayload)
			throws Exception, Throwable {
		logger.info("Calling Add Branch - API");
		return branchService.addBranch(addBranchPayload);
	}

	@PostMapping("/update")
	public @ResponseBody GetValuesResposne updateBranch(@RequestBody final AddBranchPayload addBranchPayload)
			throws Exception, Throwable {
		logger.info("Calling Update Branch - API");
		return branchService.updateBranch(addBranchPayload);
	}

	@GetMapping("/getListing")
	public @ResponseBody GetBranchListingResponse getBranchListing() throws Exception, Throwable {
		logger.info("Calling Get Branch Listing - API");
		return branchService.getBranchListing();
	}

	@GetMapping("/getList")
	// For Drop down in Add User
	public @ResponseBody GetBranchListResponse getBranchList() throws Exception, Throwable {
		logger.info("Calling Get Branch Listing - API");
		return branchService.getBranchList();
	}

	@GetMapping("/count")
	public @ResponseBody GetValuesResposne countBranch() throws Exception, Throwable {
		logger.info("Calling Get Branch Count - API");
		return branchService.countBranch();
	}

	@PostMapping("/getDetails/branch")
	public @ResponseBody GetBranchListingResponse getQueueListingByQueue(@RequestBody GetByIdPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Branch Details - API");
		return branchService.getBranchDetailsByBranchId(payload.getId());
	}

	@PostMapping("/getCompleteDetails/id")
	public @ResponseBody GetBranchCompleteDetailsResponse getBranchCompleteDetails(
			@RequestBody @Valid final GetByIdPayload payload) throws Exception, Throwable {
		logger.info("Calling Get Branch Complete Details - API");
		// 0-BranchId
		return branchService.getBranchCompleteDetails(payload.getId());
	}

	@PostMapping("/delete")
	public @ResponseBody GenStatusResponse deleteBranch(@RequestBody final GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Branch - API");
		// 0-BranchId
		return branchService.deleteBranch(getByIdPayload.getId());
	}

	@PostMapping("/status/change")
	public @ResponseBody GenStatusResponse changeBranchStatus(@RequestBody final GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Change Branch Status - API");
		// 0-BranchId
		return branchService.changeBranchStatus(getByIdPayload.getId());
	}

	@PostMapping("/clone")
	public @ResponseBody GetValuesResposne cloneBranch(@RequestBody final CloneBranchPayload cloneBranchPayload)
			throws Exception, Throwable {
		logger.info("Calling Clone Branch - API");
		return branchService.cloneBranch(cloneBranchPayload);
	}

	@PostMapping("/service/add")
	public @ResponseBody GenStatusResponse addService(@RequestBody final AddBranchServicePayload payload)
			throws Exception, Throwable {
		logger.info("Calling Add Branch Service - API");
		return branchService.addService(payload);
	}

	@PostMapping("/service/delete")
	public @ResponseBody GenStatusResponse deleteService(@RequestBody final GetByIdListPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Delete Branch Service - API");
		return branchService.deleteService(payload);
	}

	@PostMapping("/workingHours/update")
	public @ResponseBody GenStatusResponse updateWorkingHours(@RequestBody final AddBranchPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Update Branch Working Hours - API");
		return branchService.updateWorkingHours(payload);
	}

}
