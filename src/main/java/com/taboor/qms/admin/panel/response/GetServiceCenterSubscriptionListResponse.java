package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetServiceCenterSubscriptionListResponse extends GenStatusResponse {

	private List<ServiceCenterSubscription> subscriptions;

	public List<ServiceCenterSubscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<ServiceCenterSubscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public GetServiceCenterSubscriptionListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetServiceCenterSubscriptionListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<ServiceCenterSubscription> subscriptions) {
		super(applicationStatusCode, applicationStatusResponse);
		this.subscriptions = subscriptions;
	}

}
