package com.taboor.qms.admin.panel.payload;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.taboor.qms.core.model.ServiceSector;
import com.taboor.qms.core.utils.Currency;

public class RegisterServiceCenterPayload {

	@NotBlank
	@Size(max = 50)
	private String name;

	@NotBlank
	@Size(max = 50)
	private String arabicName;

	@NotBlank
	@Size(max = 50)
	private String phoneNumber;

	@NotBlank
	@Email
	@Size(max = 50)
	private String email;

	@NotBlank
	@Size(max = 50)
	private String password;

	@NotBlank
	@Size(max = 50)
	private String country;

	private int noOfBranch;
	private Double totalAmount;
	private Currency currency;
	private int planSubscriptionType;

	private ServiceSector serviceSector;
	private Long paymentPlanId;

	private List<EasyPaymentObject> easyPayments;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getNoOfBranch() {
		return noOfBranch;
	}

	public void setNoOfBranch(int noOfBranch) {
		this.noOfBranch = noOfBranch;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public int getPlanSubscriptionType() {
		return planSubscriptionType;
	}

	public void setPlanSubscriptionType(int planSubscriptionType) {
		this.planSubscriptionType = planSubscriptionType;
	}

	public ServiceSector getServiceSector() {
		return serviceSector;
	}

	public void setServiceSector(ServiceSector serviceSector) {
		this.serviceSector = serviceSector;
	}

	public String getArabicName() {
		return arabicName;
	}

	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}

	public Long getPaymentPlanId() {
		return paymentPlanId;
	}

	public void setPaymentPlanId(Long paymentPlanId) {
		this.paymentPlanId = paymentPlanId;
	}

	public List<EasyPaymentObject> getEasyPayments() {
		return easyPayments;
	}

	public void setEasyPayments(List<EasyPaymentObject> easyPayments) {
		this.easyPayments = easyPayments;
	}

}
