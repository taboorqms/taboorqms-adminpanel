package com.taboor.qms.admin.panel.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.admin.panel.response.GetUserConfigurationsResponse;
import com.taboor.qms.admin.panel.response.GetUserNotificationTypeResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.core.response.GenStatusResponse;

@Service
public interface SettingService {

	GetUserConfigurationsResponse getUserConfigurations() throws TaboorQMSServiceException, Exception;

	GenStatusResponse updateUserConfigurations(UserConfiguration userConfiguration)
			throws TaboorQMSServiceException, Exception;

	GetUserNotificationTypeResponse getUserNotificationType() throws TaboorQMSServiceException, Exception;

	GenStatusResponse updateNotificationType(UserNotificationType userNotificationType)
			throws TaboorQMSServiceException, Exception;

}
