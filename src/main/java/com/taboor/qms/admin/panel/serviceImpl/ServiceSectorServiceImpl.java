package com.taboor.qms.admin.panel.serviceImpl;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.response.GetServiceSectorListResponse;
import com.taboor.qms.admin.panel.service.ServiceSectorService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.ServiceSector;
import com.taboor.qms.core.utils.GenericMapper;

@Service
public class ServiceSectorServiceImpl implements ServiceSectorService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public GetServiceSectorListResponse getAllServiceSectors() throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/servicesectors/get/all";
		ResponseEntity<?> getServiceSectorsResponse = restTemplate.getForEntity(uri, List.class);

		List<ServiceSector> serviceSectorList = GenericMapper.convertListToObject(getServiceSectorsResponse.getBody(),
				new TypeReference<List<ServiceSector>>() {
				});
		return new GetServiceSectorListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.SERVICE_SECTOR_FETCHED.getMessage(), serviceSectorList);
	}

}
