package com.taboor.qms.admin.panel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.response.GetQueueListingResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface QueueService {

	GetQueueListingResponse getQueueListing() throws TaboorQMSServiceException, Exception;

	GetQueueListingResponse getQueueListingByQueueId(long queueId) throws TaboorQMSServiceException, Exception;

	List<Queue> saveQueueList(List<Queue> queueList) throws TaboorQMSServiceException, Exception;

	GetValuesResposne countQueue() throws TaboorQMSServiceException, Exception;

}
