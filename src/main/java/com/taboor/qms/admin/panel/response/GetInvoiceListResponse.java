package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetInvoiceListResponse extends GenStatusResponse {

	private List<Invoice> invoices;

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	public GetInvoiceListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetInvoiceListResponse(int applicationStatusCode, String applicationStatusResponse, List<Invoice> invoices) {
		super(applicationStatusCode, applicationStatusResponse);
		this.invoices = invoices;
	}

}
