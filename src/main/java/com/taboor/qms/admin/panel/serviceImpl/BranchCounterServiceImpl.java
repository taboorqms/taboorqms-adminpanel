package com.taboor.qms.admin.panel.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.payload.AddBranchCounterPayload;
import com.taboor.qms.admin.panel.service.BranchCounterService;
import com.taboor.qms.admin.panel.service.BranchService;
import com.taboor.qms.admin.panel.service.ServiceTABService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterServiceTABMapper;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchCounterListResponse;
import com.taboor.qms.core.response.GetBranchCounterListResponse.BranchCounterObject;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.RestUtil;

@Service
public class BranchCounterServiceImpl implements BranchCounterService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BranchCounterServiceImpl.class);

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    BranchService branchService;

    @Autowired
    ServiceTABService serviceTABService;

    @Override
    public GetBranchCounterListResponse getByBranch(long branchId) throws TaboorQMSServiceException, Exception {
        Branch branch = branchService.getBranch(branchId);

        String uri = dbConnectorMicroserviceURL + "/tab/branchcounters/get/branch?branchId=" + branch.getBranchId();
        ResponseEntity<?> getBranchCountersResponse = restTemplate.getForEntity(uri, List.class);

        if (!getBranchCountersResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        List<BranchCounterObject> branchCounterObjectList = GenericMapper.convertListToObject(
                getBranchCountersResponse.getBody(), new TypeReference<List<BranchCounterObject>>() {
        });

        return new GetBranchCounterListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_COUNTER_FETCHED.getMessage(), branchCounterObjectList);
    }

    @Override
    public GetValuesResposne addOrUpdateCounter(AddBranchCounterPayload addBranchCounterPayload)
            throws TaboorQMSServiceException, Exception {
        BranchCounter branchCounter = new BranchCounter();
        if (addBranchCounterPayload.getUpdate()) // Update
        {
            branchCounter = getById(addBranchCounterPayload.getCounterId());

            String getCounterServiceTABMapperURI = dbConnectorMicroserviceURL
                    + "/tab/branchcounterservicetabmapper/get/id?branchCounterId="
                    + branchCounter.getCounterId();

            ResponseEntity<?> branchCounterServiceTABs = restTemplate.getForEntity(getCounterServiceTABMapperURI, Boolean.class);

            if (branchCounterServiceTABs.getStatusCode().equals(HttpStatus.OK)) {
                if(branchCounterServiceTABs.getBody() != null){
                    String uri = dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/delete/branch?branchCounterId=" + branchCounter.getCounterId();
                    ResponseEntity<Boolean> deleteBranchCounterServiceTABMapperResponse = restTemplate.getForEntity(uri, Boolean.class);
                    if (!deleteBranchCounterServiceTABMapperResponse.getStatusCode().equals(HttpStatus.OK)
                            || deleteBranchCounterServiceTABMapperResponse.getBody().equals(false)) {
                        throw new TaboorQMSServiceException( xyzErrorMessage.BRANCHCOUNTERSERVICETABMAPPER_NOT_DELETED.getErrorCode() + "::"
                                + xyzErrorMessage.BRANCHCOUNTERSERVICETABMAPPER_NOT_DELETED.getErrorDetails());
                    }
                }
            } else{
                throw new TaboorQMSServiceException( xyzErrorMessage.BRANCHCOUNTERSERVICETABMAPPER_NOT_UPDATED.getErrorCode() + "::"
                                + xyzErrorMessage.BRANCHCOUNTERSERVICETABMAPPER_NOT_UPDATED.getErrorDetails());
            }

        } else { // New
            Branch branch = branchService.getBranch(addBranchCounterPayload.getBranchId());
            branchCounter.setBranch(branch);
            branchCounter.setCounterNumber(
                    getNextCounterNumber(branch.getBranchId()).getValues().get("NextCounterNumber").toString());
            branchCounter = saveBranchCounter(branchCounter);
        }

        List<BranchCounterServiceTABMapper> branchCounterServiceTABMappers = new ArrayList<BranchCounterServiceTABMapper>();
        for (ServiceTAB serviceTAB : addBranchCounterPayload.getServices()) {
            BranchCounterServiceTABMapper branchCounterServiceTABMapper = new BranchCounterServiceTABMapper();
            branchCounterServiceTABMapper.setServiceTAB(serviceTAB);
            branchCounterServiceTABMapper.setBranchCounter(branchCounter);
            branchCounterServiceTABMappers.add(branchCounterServiceTABMapper);
        }

        String uri = dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/saveAll";
        HttpEntity<?> branchCounterServiceTABMapperEntity = new HttpEntity<>(branchCounterServiceTABMappers,
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<?> createBranchCounterServiceTABMapperResponse = restTemplate.postForEntity(uri,
                branchCounterServiceTABMapperEntity, List.class);

        if (!createBranchCounterServiceTABMapperResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCHCOUNTERSERVICETABMAPPER_NOT_CREATED.getErrorCode()
                    + "::" + xyzErrorMessage.BRANCHCOUNTERSERVICETABMAPPER_NOT_CREATED.getErrorDetails());
        }

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("CounterId", String.valueOf(branchCounter.getCounterId()));
        values.put("SavedCounterNumber", String.valueOf(branchCounter.getCounterNumber()));
        values.put("AssignedServices", addBranchCounterPayload.getServices());
        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_COUNTER_ADDED.getMessage(), values);
    }

    private BranchCounter getByCounterNumber(String counterNumber) throws TaboorQMSServiceException {
        String uri = dbConnectorMicroserviceURL + "/tab/branchcounters/get/counterNumber?counterNumber="
                + counterNumber;
        ResponseEntity<BranchCounter> getBranchCounterResponse = restTemplate.getForEntity(uri, BranchCounter.class);

        if (!getBranchCounterResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        } else if (getBranchCounterResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());
        }
        return getBranchCounterResponse.getBody();
    }

    private BranchCounter saveBranchCounter(BranchCounter branchCounter) throws TaboorQMSServiceException {
        String uri = dbConnectorMicroserviceURL + "/tab/branchcounters/save";
        HttpEntity<BranchCounter> branchCounterEntity = new HttpEntity<>(branchCounter,
                HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<BranchCounter> createBranchCounterResponse = restTemplate.postForEntity(uri, branchCounterEntity,
                BranchCounter.class);

        if (!createBranchCounterResponse.getStatusCode().equals(HttpStatus.OK)
                || createBranchCounterResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCHCOUNTER_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.BRANCHCOUNTER_NOT_CREATED.getErrorDetails());
        }
        return createBranchCounterResponse.getBody();
    }

    @Override
    public GetValuesResposne getNextCounterNumber(long branchId) throws TaboorQMSServiceException, Exception {
        Branch branch = branchService.getBranch(branchId);
        String uri = dbConnectorMicroserviceURL + "/tab/branchcounters/getNextCounterNumber/branch?branchId="
                + branch.getBranchId();
        ResponseEntity<String> getNextCounterNumberResponse = restTemplate.getForEntity(uri, String.class);

        if (!getNextCounterNumberResponse.getStatusCode().equals(HttpStatus.OK)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                    + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("NextCounterNumber", String.valueOf(getNextCounterNumberResponse.getBody()));

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_COUNTER_NEXT_NUMBER_FETCHED.getMessage(), values);
    }

    @Override
    public BranchCounter getById(Long branchCounterId) throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId=" + branchCounterId;
        ResponseEntity<BranchCounter> getBarnchCounterResponse = restTemplate.getForEntity(uri, BranchCounter.class);

        if (!getBarnchCounterResponse.getStatusCode().equals(HttpStatus.OK)
                || getBarnchCounterResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());
        }
        return getBarnchCounterResponse.getBody();
    }

    @Override
    public GenStatusResponse deleteCounterList(List<Long> branchCounterIdList)
            throws TaboorQMSServiceException, Exception {
        if (branchCounterIdList == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());
        } else if (branchCounterIdList.isEmpty()) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());
        }

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/branchcounters/delete/idList",
                new HttpEntity<>(branchCounterIdList, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class,
                xyzErrorMessage.BRANCHCOUNTER_NOT_DELETED.getErrorCode(),
                xyzErrorMessage.BRANCHCOUNTER_NOT_DELETED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_COUNTER_DELETED.getMessage());
    }

    @Override
    public GenStatusResponse changeCounterStatus(long branchCounterId) throws TaboorQMSServiceException, Exception {
        BranchCounter branchCounter = getById(branchCounterId);
        if (branchCounter.getStatus() == 1) {
            branchCounter.setStatus(0); // Working
        } else if (branchCounter.getStatus() == 0) {
            branchCounter.setStatus(1); // Freeze
        }
        branchCounter = saveBranchCounter(branchCounter);
        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_COUNTER_STATUS_CHANGED.getMessage());
    }

}
