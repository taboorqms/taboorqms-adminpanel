package com.taboor.qms.admin.panel.serviceImpl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.taboor.qms.admin.panel.payload.AddAgentPayload;
import com.taboor.qms.admin.panel.response.GetUserPrivilegesListResponse;
import com.taboor.qms.admin.panel.service.AgentService;
import com.taboor.qms.admin.panel.service.BranchCounterService;
import com.taboor.qms.admin.panel.service.BranchService;
import com.taboor.qms.admin.panel.service.EmpoyeeBranchService;
import com.taboor.qms.admin.panel.service.ServiceCenterEmpService;
import com.taboor.qms.admin.panel.service.ServiceCenterService;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterAgentMapper;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserPrivilegeMapper;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAgentListResponse;
import com.taboor.qms.core.response.GetAgentListResponse.AgentObject;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.core.response.GetStatisticsResponse.ValueObject;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import net.minidev.json.JSONArray;
import org.apache.http.util.EntityUtils;

@Service
public class AgentServiceImpl implements AgentService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AgentServiceImpl.class);

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    @Value("${url.microservice.user.management}")
    private String userManagementMicroserviceURL;

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    ServiceCenterService serviceCenterService;

    @Autowired
    ServiceCenterEmpService serviceCenterEmpService;

    @Autowired
    EmpoyeeBranchService employeeBranchService;

    @Autowired
    BranchService branchService;

    @Autowired
    BranchCounterService branchCounterService;

    @Override
    public GetValuesResposne getNextAgentCredentials() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());

        Map<String, Object> values = new HashMap<String, Object>();
        Random random = new Random();
        values.put("AgentPIN", String.valueOf(random.ints(111111, 999999).findFirst().getAsInt()));
        values.put("NextAgentId", serviceCenterEmpService.nextEmpNumber(serviceCenterEmployee.getServiceCenter()));

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.NEXT_AGENT_CREDENTIALS_FETCHED.getMessage(), values);
    }

    @Override
    public GetValuesResposne addAgent(AddAgentPayload addAgentPayload) throws TaboorQMSServiceException, Exception {
        Branch branch = branchService.getBranch(addAgentPayload.getBranchId());

        BranchCounter assignBranchCounter = new BranchCounter();
        if (addAgentPayload.getAssignedCounterId() != null) {
            if (addAgentPayload.getAssignedCounterId() > 0) {
                assignBranchCounter = branchCounterService.getById(addAgentPayload.getAssignedCounterId());
            }
        }

        String uri = dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + RoleName.Agent;
        ResponseEntity<UserRole> getUserRoleResponse = restTemplate.getForEntity(uri, UserRole.class);

        if (!getUserRoleResponse.getStatusCode().equals(HttpStatus.OK) || getUserRoleResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());
        }

        RegisterUserPayload registerUserPayload = new RegisterUserPayload();
        registerUserPayload.setName(addAgentPayload.getAgentName());
        registerUserPayload.setEmail(addAgentPayload.getEmail());
        registerUserPayload.setPassword(addAgentPayload.getAgentPin());
        registerUserPayload.setPhoneNumber(null);
        registerUserPayload.setUserRole(getUserRoleResponse.getBody());
        registerUserPayload.setAssignRolePrivileges(false);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("RegisterUserPayload", registerUserPayload);
        body.add("UserProfileImage", null);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body,
                HttpHeadersUtils.getMultipartFormDataHeader());

        uri = userManagementMicroserviceURL + "/user/register";

        ResponseEntity<RegisterUserResponse> registerUserResponse = restTemplate.postForEntity(uri, requestEntity,
                RegisterUserResponse.class);

        if (!registerUserResponse.getStatusCode().equals(HttpStatus.OK)
                || registerUserResponse.getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_REGISTERED.getErrorCode() + "::"
                    + registerUserResponse.getBody().getApplicationStatusResponse());
        }

        ServiceCenterEmployee serviceCenterEmployee = new ServiceCenterEmployee();
        serviceCenterEmployee.setEmployeeNumber(serviceCenterEmpService.nextEmpNumber(branch.getServiceCenter()));
        serviceCenterEmployee.setServiceCenter(branch.getServiceCenter());
        serviceCenterEmployee.setUser(registerUserResponse.getBody().getUser());

        HttpEntity<ServiceCenterEmployee> registerEmployeeEntity = new HttpEntity<>(serviceCenterEmployee,
                HttpHeadersUtils.getApplicationJsonHeader());
        uri = dbConnectorMicroserviceURL + "/tab/servicecenteremployees/save";
        ResponseEntity<ServiceCenterEmployee> registerEmployeeResponse = restTemplate.postForEntity(uri,
                registerEmployeeEntity, ServiceCenterEmployee.class);

        if (!registerEmployeeResponse.getStatusCode().equals(HttpStatus.OK)
                || registerEmployeeResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.SERVICECENTEREMP_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.SERVICECENTEREMP_NOT_CREATED.getErrorDetails());
        }

        serviceCenterEmployee = registerEmployeeResponse.getBody();

        Agent agent = new Agent();
        agent.setAverageServiceTime(LocalTime.of(0, 0));
        agent.setAverageServiceTimeCount(0);
        agent.setBranch(branch);
        agent.setServiceCenterEmployee(serviceCenterEmployee);
        agent.setTotalTicketServed(0);
        agent.setTotalWorkHours(LocalTime.of(0, 0));
        agent.setLoginStatus(false);

        agent = saveAgent(agent);

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("SavedAgentId", String.valueOf(agent.getServiceCenterEmployee().getEmployeeNumber()));
        values.put("AgentPk", agent.getAgentId());
        values.put("AgentPIN", String.valueOf(addAgentPayload.getAgentPin()));
        values.put("Email", agent.getServiceCenterEmployee().getUser().getEmail());
        values.put("AgentName", agent.getServiceCenterEmployee().getUser().getName());
        values.put("BranchName", agent.getBranch().getBranchName());

        // Assign Counter
        if (addAgentPayload.getAssignedCounterId() != null) {
            if (addAgentPayload.getAssignedCounterId() > 0) {
                BranchCounterAgentMapper branchCounterAgentMapper = new BranchCounterAgentMapper();
                branchCounterAgentMapper.setAgent(agent);
                branchCounterAgentMapper.setBranchCounter(assignBranchCounter);
                uri = dbConnectorMicroserviceURL + "/tab/branchcounteragent/save";
                HttpEntity<BranchCounterAgentMapper> registerBranchCounterAgentEntity = new HttpEntity<>(
                        branchCounterAgentMapper, HttpHeadersUtils.getApplicationJsonHeader());
                ResponseEntity<BranchCounterAgentMapper> assignCounterResponse = restTemplate.postForEntity(uri,
                        registerBranchCounterAgentEntity, BranchCounterAgentMapper.class);

                if (!assignCounterResponse.getStatusCode().equals(HttpStatus.OK)
                        || assignCounterResponse.getBody() == null) {
                    throw new TaboorQMSServiceException(
                            xyzErrorMessage.BRANCHCOUNTERAGENTMAPPER_NOT_CREATED.getErrorCode() + "::"
                            + xyzErrorMessage.BRANCHCOUNTERAGENTMAPPER_NOT_CREATED.getErrorDetails());
                }

                values.put("AssignedCounterId",
                        String.valueOf(branchCounterAgentMapper.getBranchCounter().getCounterId()));
                values.put("AssignedCounterNumber",
                        String.valueOf(branchCounterAgentMapper.getBranchCounter().getCounterNumber()));
            }
        }

        // Privileges
        List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();

        addAgentPayload.getPrivileges().forEach(userPrivilege -> {
            UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
            userPrivilegeMapper.setUser(registerUserResponse.getBody().getUser());
            userPrivilegeMapper.setUserPrivilege(userPrivilege);
            userPrivilegeMappers.add(userPrivilegeMapper);
        });

        uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/saveAll";
        HttpEntity<?> httpEntity = new HttpEntity<>(userPrivilegeMappers, HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<Boolean> createUserPrivilegeMappersResponse = restTemplate.postForEntity(uri, httpEntity,
                Boolean.class);

        if (!createUserPrivilegeMappersResponse.getStatusCode().equals(HttpStatus.OK)
                || createUserPrivilegeMappersResponse.getBody().equals(false)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorDetails());
        }

        values.put("AssignedPrivileges", addAgentPayload.getPrivileges());

        RSocketPayload payload = new RSocketPayload();
        payload.setNotificationSubject(RSocketNotificationSubject.NEW_AGENT);
        payload.setRecipientId(agent.getServiceCenterEmployee().getServiceCenter().getServiceCenterId());
        payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
        payload.setInformation(Arrays.asList(agent.getServiceCenterEmployee().getUser().getName()));

        RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);
//		RSocketInitializer.fireAndForget(rSocketPayload);

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.AGENT_ADDED.getMessage(),
                values);
    }

    private Agent saveAgent(Agent agent) throws TaboorQMSServiceException {
        HttpEntity<Agent> registerAgentEntity = new HttpEntity<>(agent, HttpHeadersUtils.getApplicationJsonHeader());
        String uri = dbConnectorMicroserviceURL + "/tab/agents/save";
        ResponseEntity<Agent> registerAgentResponse = restTemplate.postForEntity(uri, registerAgentEntity, Agent.class);

        if (!registerAgentResponse.getStatusCode().equals(HttpStatus.OK) || registerAgentResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.AGENT_NOT_CREATED.getErrorCode() + "::"
                    + xyzErrorMessage.AGENT_NOT_CREATED.getErrorDetails());
        }
        return registerAgentResponse.getBody();
    }

    @Override
    public GetUserPrivilegesListResponse getAgentPrivileges() throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + RoleName.Agent;
        ResponseEntity<UserRole> getAgentRoleResponse = restTemplate.getForEntity(uri, UserRole.class);

        return new GetUserPrivilegesListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.USER_PRIVILEGE_FETCHED.getMessage(),
                new ArrayList<>(getAgentRoleResponse.getBody().getUserPrivileges()));
    }

    @Override
    public GetAgentListResponse getByServiceCenter() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());
        String uri = dbConnectorMicroserviceURL + "/tab/agents/get/serviceCenter?serviceCenterId="
                + serviceCenterEmployee.getServiceCenter().getServiceCenterId();
        ResponseEntity<?> getServiceCenterAgentsResponse = restTemplate.getForEntity(uri, List.class);

        List<AgentObject> agentList = GenericMapper.convertListToObject(getServiceCenterAgentsResponse.getBody(),
                new TypeReference<List<GetAgentListResponse.AgentObject>>() {
        });
        agentList.forEach(agent -> {
            if (agent.getAgent().getBranch() != null) {
                agent.getAgent().getBranch().setServiceCenter(null);
            }
            agent.getAgent().getServiceCenterEmployee().setServiceCenter(null);
            if (agent.getAssignedBranchCounter() != null) {
                agent.getAssignedBranchCounter().setBranch(null);
            }
        });

        return new GetAgentListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_FETCHED.getMessage(), agentList);
    }

    @Override
    public GetValuesResposne updateAgent(AddAgentPayload addAgentPayload) throws TaboorQMSServiceException, Exception {
        Agent agent = getById(addAgentPayload.getAgentPk());
        String uri;
        if (addAgentPayload.getBranchId() != null) {
            if (addAgentPayload.getBranchId() == -1) {
                // Release Agent: Remove Branch from Agent
                uri = dbConnectorMicroserviceURL + "/tab/branchcounteragent/delete/agent?agentId=" + agent.getAgentId();
                ResponseEntity<Boolean> deleteBranchCounterAgentMapperResponse = restTemplate.getForEntity(uri,
                        Boolean.class);

                if (!deleteBranchCounterAgentMapperResponse.getStatusCode().equals(HttpStatus.OK)
                        || deleteBranchCounterAgentMapperResponse.getBody().equals(false)) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                            + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
                }

                agent.setBranch(null);
                agent = saveAgent(agent);
                return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                        xyzResponseMessage.AGENT_UPDATED.getMessage(), null);

            } else if (addAgentPayload.getBranchId() > 0) {
                Branch branch = branchService.getBranch(addAgentPayload.getBranchId());
                agent.setBranch(branch);
                // update Agent
                agent = saveAgent(agent);
            }
        }

        Map<String, Object> values = new HashMap<String, Object>();
        values.put("SavedAgentId", String.valueOf(agent.getServiceCenterEmployee().getEmployeeNumber()));
        values.put("AgentPk", agent.getAgentId());
        values.put("AgentPIN", String.valueOf(agent.getServiceCenterEmployee().getUser().getPassword()));
        values.put("Email", agent.getServiceCenterEmployee().getUser().getEmail());
        if (agent.getBranch() != null) {
            values.put("BranchName", agent.getBranch().getBranchName());
        }

        if (addAgentPayload.getUpdateStatus() == 1)// Update Agent
        {

            if (addAgentPayload.getEmail() != null & addAgentPayload.getAgentName() != null) {

//				if (addAgentPayload.getEmail() != null)
//					agent.getServiceCenterEmployee().getUser().setEmail(addAgentPayload.getEmail());
                if (addAgentPayload.getAgentName() != null) {
                    agent.getServiceCenterEmployee().getUser().setName(addAgentPayload.getAgentName());
                }

                uri = dbConnectorMicroserviceURL + "/tab/users/save";
                HttpEntity<User> updateUserEntity = new HttpEntity<>(agent.getServiceCenterEmployee().getUser(),
                        HttpHeadersUtils.getApplicationJsonHeader());
                ResponseEntity<User> updateUserResponse = restTemplate.postForEntity(uri, updateUserEntity, User.class);
                if (!updateUserResponse.getStatusCode().equals(HttpStatus.OK) || updateUserResponse.getBody() == null) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_UPDATED.getErrorCode() + "::"
                            + xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());
                }

                agent.getServiceCenterEmployee().setUser(updateUserResponse.getBody());
            }

            // Assign Counter
            if (addAgentPayload.getAssignedCounterId() != null) {
                uri = dbConnectorMicroserviceURL + "/tab/branchcounteragent/get/agent?agentId=" + agent.getAgentId();
                ResponseEntity<BranchCounterAgentMapper> getBranchCounterAgentMapperResponse = restTemplate
                        .getForEntity(uri, BranchCounterAgentMapper.class);

                if (!getBranchCounterAgentMapperResponse.getStatusCode().equals(HttpStatus.OK)) {
                    throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                            + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
                }

                BranchCounterAgentMapper branchCounterAgentMapper = getBranchCounterAgentMapperResponse.getBody();
                if (branchCounterAgentMapper == null) {
                    branchCounterAgentMapper = new BranchCounterAgentMapper();
                    branchCounterAgentMapper.setAgent(agent);
                }
                branchCounterAgentMapper
                        .setBranchCounter(branchCounterService.getById(addAgentPayload.getAssignedCounterId()));
                uri = dbConnectorMicroserviceURL + "/tab/branchcounteragent/save";
                HttpEntity<BranchCounterAgentMapper> registerBranchCounterAgentEntity = new HttpEntity<>(
                        branchCounterAgentMapper, HttpHeadersUtils.getApplicationJsonHeader());
                ResponseEntity<BranchCounterAgentMapper> assignCounterResponse = restTemplate.postForEntity(uri,
                        registerBranchCounterAgentEntity, BranchCounterAgentMapper.class);

                if (!assignCounterResponse.getStatusCode().equals(HttpStatus.OK)
                        || assignCounterResponse.getBody() == null) {
                    throw new TaboorQMSServiceException(
                            xyzErrorMessage.BRANCHCOUNTERAGENTMAPPER_NOT_CREATED.getErrorCode() + "::"
                            + xyzErrorMessage.BRANCHCOUNTERAGENTMAPPER_NOT_CREATED.getErrorDetails());
                }

                values.put("AssignedCounterId",
                        String.valueOf(branchCounterAgentMapper.getBranchCounter().getCounterId()));
                values.put("AssignedCounterNumber",
                        String.valueOf(branchCounterAgentMapper.getBranchCounter().getCounterNumber()));
            }

            // Privileges
            List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();

            for (UserPrivilege item : addAgentPayload.getPrivileges()) {
                UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
                userPrivilegeMapper.setUser(agent.getServiceCenterEmployee().getUser());
                userPrivilegeMapper.setUserPrivilege(item);
                userPrivilegeMappers.add(userPrivilegeMapper);
            }

            uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/updateAll";
            HttpEntity<?> httpEntity = new HttpEntity<>(userPrivilegeMappers,
                    HttpHeadersUtils.getApplicationJsonHeader());
            ResponseEntity<Boolean> updateUserPrivilegeMappersResponse = restTemplate.postForEntity(uri, httpEntity,
                    Boolean.class);

            if (!updateUserPrivilegeMappersResponse.getStatusCode().equals(HttpStatus.OK)
                    || updateUserPrivilegeMappersResponse.getBody().equals(false)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorCode()
                        + "::" + xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorDetails());
            }

            values.put("AssignedPrivileges", addAgentPayload.getPrivileges());

        } else if (addAgentPayload.getUpdateStatus() == 2) // Select from Existing
        {
            uri = dbConnectorMicroserviceURL + "/tab/branchcounteragent/get/agent?agentId=" + agent.getAgentId();
            ResponseEntity<BranchCounterAgentMapper> getBranchCounterAgentMapperResponse = restTemplate
                    .getForEntity(uri, BranchCounterAgentMapper.class);

            if (!getBranchCounterAgentMapperResponse.getStatusCode().equals(HttpStatus.OK)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                        + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
            }
            if (getBranchCounterAgentMapperResponse.getBody() != null) {
                values.put("AssignedCounterId", String
                        .valueOf(getBranchCounterAgentMapperResponse.getBody().getBranchCounter().getCounterId()));
                values.put("AssignedCounterNumber", String
                        .valueOf(getBranchCounterAgentMapperResponse.getBody().getBranchCounter().getCounterNumber()));
            }

            uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/get/user?userId="
                    + agent.getServiceCenterEmployee().getUser().getUserId();
            ResponseEntity<?> getUserPrivilegeResponse = restTemplate.getForEntity(uri, List.class);
            if (!getUserPrivilegeResponse.getStatusCode().equals(HttpStatus.OK)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
                        + xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
            }

            List<UserPrivilege> privilegeList = GenericMapper.convertListToObject(getUserPrivilegeResponse.getBody(),
                    new TypeReference<List<UserPrivilege>>() {
            });

            values.put("AssignedPrivileges", privilegeList);
        }
        values.put("AgentName", agent.getServiceCenterEmployee().getUser().getName());

        RSocketPayload payload = new RSocketPayload();
        payload.setNotificationSubject(RSocketNotificationSubject.UPDATE_AGENT);
        payload.setRecipientId(agent.getServiceCenterEmployee().getServiceCenter().getServiceCenterId());
        payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
        payload.setInformation(Arrays.asList(agent.getServiceCenterEmployee().getUser().getName()));

        RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);
//		RSocketInitializer.fireAndForget(rSocketPayload);

        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.AGENT_UPDATED.getMessage(),
                values);
    }

    @Override
    public Agent getById(Long agentId) throws TaboorQMSServiceException, Exception {
        logger.warn("AgentId = " + agentId);
        String uri = dbConnectorMicroserviceURL + "/tab/agents/get/id?agentId=" + agentId;
        ResponseEntity<Agent> getAgentResponse = restTemplate.getForEntity(uri, Agent.class);

        logger.warn(GenericMapper.objectToJSONMapper(getAgentResponse.getBody()));
        if (!getAgentResponse.getStatusCode().equals(HttpStatus.OK) || getAgentResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.AGENT_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.AGENT_NOT_EXISTS.getErrorDetails());
        }

        return getAgentResponse.getBody();
    }

    @Override
    public GenStatusResponse deleteAgentList(List<Long> agentIdList) throws TaboorQMSServiceException, Exception {

        if (agentIdList == null) {
            return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.AGENT_DELETED.getMessage());
        } else if (agentIdList.isEmpty()) {
            return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.AGENT_DELETED.getMessage());
        }

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/agents/delete/idList",
                new HttpEntity<>(agentIdList, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class,
                xyzErrorMessage.AGENT_NOT_DELETED.getErrorCode(), xyzErrorMessage.AGENT_NOT_DELETED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.AGENT_DELETED.getMessage());
    }

    @Override
    public GetAgentListResponse getByBranch(Branch branch) throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/agents/get/branch";
        HttpEntity<Branch> httpEntity = new HttpEntity<>(branch, HttpHeadersUtils.getApplicationJsonHeader());
        ResponseEntity<?> getBranchAgentsResponse = restTemplate.postForEntity(uri, httpEntity, List.class);
        List<AgentObject> agentList = GenericMapper.convertListToObject(getBranchAgentsResponse.getBody(),
                new TypeReference<List<GetAgentListResponse.AgentObject>>() {
        });
        agentList.forEach(agent -> {
            agent.getAgent().setBranch(null);
//			agent.getAgent().setServiceCenterEmployee(null);
            if (agent.getAssignedBranchCounter() != null) {
                agent.getAssignedBranchCounter().setBranch(null);
            }
        });

        return new GetAgentListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_FETCHED.getMessage(), agentList);
    }

    @Override
    public GetAgentListResponse getListing() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());
        List<AgentObject> agentList = new ArrayList<>();
        if (serviceCenterEmployee.getUser().getRoleName().equals(RoleName.Service_Center_Admin.name())) {
            ResponseEntity<?> agentListResponse = restTemplate.getForEntity(dbConnectorMicroserviceURL + "/tab/agents/get/serviceCenter?serviceCenterId="
                    + serviceCenterEmployee.getServiceCenter().getServiceCenterId(),
                    List.class);

            agentList = GenericMapper.convertListToObject(agentListResponse.getBody(),
                    new TypeReference<List<AgentObject>>() {
            });

        } else {

            List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

            if (branchList.isEmpty()) {
                return new GetAgentListResponse(xyzResponseCode.SUCCESS.getCode(),
                        xyzResponseMessage.AGENT_FETCHED.getMessage(), null);
            }

            List<Long> branchIdList = new ArrayList<Long>();
            for (Branch branch : branchList) {
                branchIdList.add(branch.getBranchId());
            }
            branchIdList = branchIdList.stream().distinct().collect(Collectors.toList());

            List<?> response = RestUtil.postRequest(
                    dbConnectorMicroserviceURL + "/tab/agents/getAgentListing/branchList",
                    new HttpEntity<>(branchIdList, HttpHeadersUtils.getApplicationJsonHeader()), List.class,
                    xyzErrorMessage.AGENT_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.AGENT_NOT_EXISTS.getErrorDetails());
            agentList = GenericMapper.convertListToObject(response,
                    new TypeReference<List<AgentObject>>() {
            });
        }

        agentList.forEach(agent -> {
            if (agent.getAgent().getBranch() != null) {
                agent.getAgent().getBranch().setServiceCenter(null);
            }
            agent.getAgent().getServiceCenterEmployee().setServiceCenter(null);
            if (agent.getAssignedBranchCounter() != null) {
                agent.getAssignedBranchCounter().setBranch(null);
            }
        });

        return new GetAgentListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_FETCHED.getMessage(), agentList);
    }

    @Override
    public GetAgentListResponse getDetails(long agentId) throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/agents/getDeatils/id?agentId=" + agentId;
        ResponseEntity<AgentObject> getAgentDetailsResponse = restTemplate.getForEntity(uri, AgentObject.class);
        if (!getAgentDetailsResponse.getStatusCode().equals(HttpStatus.OK) || getAgentDetailsResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.AGENT_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.AGENT_NOT_EXISTS.getErrorDetails());
        }

        if (getAgentDetailsResponse.getBody().getAgent().getBranch() != null) {
            getAgentDetailsResponse.getBody().getAgent().getBranch().setServiceCenter(null);
        }
        getAgentDetailsResponse.getBody().getAgent().getServiceCenterEmployee().setServiceCenter(null);
        if (getAgentDetailsResponse.getBody().getAssignedBranchCounter() != null) {
            getAgentDetailsResponse.getBody().getAssignedBranchCounter().setBranch(null);
        }

        return new GetAgentListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_FETCHED.getMessage(), Arrays.asList(getAgentDetailsResponse.getBody()));
    }

    @Override
    public GetStatisticsResponse getStatistics(GetActivitiesPayload payload)
            throws TaboorQMSServiceException, Exception {

        Period period = Period.between(payload.getStartDate(), payload.getEndDate());

        List<?> response = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/countRatings/agent",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), List.class,
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<ValueObject> valueObjects = new ArrayList<ValueObject>();
        valueObjects.addAll(GenericMapper.convertListToObject(response, new TypeReference<List<ValueObject>>() {
        }));

        if (period.getYears() > 0) {
            // breakdown into years
            LocalDate startYear = payload.getStartDate(),
                    endYear = payload.getStartDate().plusYears(1).withDayOfYear(1).minusDays(1);

            for (int i = 0; i <= period.getYears(); i++, startYear = endYear.plusDays(1), endYear = startYear
                    .plusYears(1).minusDays(1)) {
                if (endYear.isAfter(payload.getEndDate())) {
                    endYear = payload.getEndDate();
                }
                valueObjects.add(new ValueObject(String.valueOf(startYear.getYear()),
                        String.valueOf(getAgentStats(payload.getIdList().get(0), startYear, endYear))));
            }
        } else if (period.getMonths() > 0) {
            // breakdown into months
            LocalDate startMonth = payload.getStartDate(),
                    endMonth = payload.getStartDate().plusMonths(1).withDayOfMonth(1).minusDays(1);

            for (int i = 0; i <= period.getMonths(); i++, startMonth = endMonth.plusDays(1), endMonth = startMonth
                    .plusMonths(1).minusDays(1)) {
                if (endMonth.isAfter(payload.getEndDate())) {
                    endMonth = payload.getEndDate();
                }
                valueObjects.add(new ValueObject(
                        String.valueOf(startMonth.getMonth().getDisplayName(TextStyle.SHORT, Locale.US)),
                        String.valueOf(getAgentStats(payload.getIdList().get(0), startMonth, endMonth))));
            }
        } else if (period.getDays() > 6) {
            // breakdown into weeks
            LocalDate startWeek = payload.getStartDate(), endWeek = payload.getStartDate().plusWeeks(1).minusDays(1);
            int i = 1;
            while (true) {
                if (endWeek.isAfter(payload.getEndDate())) {
                    endWeek = payload.getEndDate();
                }
                valueObjects.add(new ValueObject("W-" + i++,
                        String.valueOf(getAgentStats(payload.getIdList().get(0), startWeek, endWeek))));
                if (!endWeek.isBefore(payload.getEndDate())) {
                    break;
                }
                startWeek = endWeek.plusDays(1);
                endWeek = startWeek.plusWeeks(1).minusDays(1);
            }
        } else if (period.getDays() > 0) {
            // breakdown into days
            for (LocalDate currentDate = payload.getStartDate(); payload.getEndDate().plusDays(1)
                    .isAfter(currentDate); currentDate = currentDate.plusDays(1)) {
                valueObjects.add(new ValueObject(
                        String.valueOf(currentDate.getDayOfWeek().getDisplayName(TextStyle.NARROW, Locale.US)),
                        String.valueOf(getAgentStats(payload.getIdList().get(0), currentDate, currentDate))));
            }
        }
        return new GetStatisticsResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_STATISTICS_FETHCED.getMessage(), valueObjects);
    }

    private Double getAgentStats(Long agentId, LocalDate startDate, LocalDate endDate)
            throws RestClientException, TaboorQMSServiceException {
        return RestUtil.post(dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/countAvgRatings/agent",
                new HttpEntity<>(new GetActivitiesPayload(startDate, endDate, Arrays.asList(agentId)),
                        HttpHeadersUtils.getApplicationJsonHeader()),
                Double.class);
    }

    @Override
    public GetValuesResposne countAgent() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
                .findByUserId(session.getUser().getUserId());

        Map<String, Object> values = new HashMap<String, Object>();
        Integer countResponse = 0;

        if (serviceCenterEmployee.getUser().getRoleName().equals(RoleName.Service_Center_Admin.name())) {
            countResponse = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/agents/count/serviceCenterId?serviceCenterId="
                    + serviceCenterEmployee.getServiceCenter().getServiceCenterId(),
                    Integer.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                    xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        } else {

            List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

            if (branchList.isEmpty()) {
                values.put("AgentCount", countResponse);
                return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
                        xyzResponseMessage.AGENT_FETCHED.getMessage(), values);
            }

            List<Long> branchIdList = new ArrayList<Long>();
            for (Branch branch : branchList) {
                branchIdList.add(branch.getBranchId());
            }
            branchIdList = branchIdList.stream().distinct().collect(Collectors.toList());

            countResponse = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/agents/getAgentCount/branchList",
                    new HttpEntity<>(branchIdList, HttpHeadersUtils.getApplicationJsonHeader()), Integer.class,
                    xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                    xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        }

        values.put("AgentCount", countResponse);
        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.BRANCH_FETCHED.getMessage(),
                values);
    }
}
