package com.taboor.qms.admin.panel.payload;

import java.time.LocalTime;

public class WorkingHours {
	private int day;
	private LocalTime openingTime;
	private LocalTime closingTime;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public LocalTime getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(LocalTime openingTime) {
		this.openingTime = openingTime;
	}

	public LocalTime getClosingTime() {
		return closingTime;
	}

	public void setClosingTime(LocalTime closingTime) {
		this.closingTime = closingTime;
	}

}
