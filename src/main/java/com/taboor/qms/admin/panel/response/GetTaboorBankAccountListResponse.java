package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.TaboorBankAccount;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetTaboorBankAccountListResponse extends GenStatusResponse {

	private List<TaboorBankAccount> bankAccounts;

	public List<TaboorBankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(List<TaboorBankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public GetTaboorBankAccountListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetTaboorBankAccountListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<TaboorBankAccount> bankAccounts) {
		super(applicationStatusCode, applicationStatusResponse);
		this.bankAccounts = bankAccounts;
	}

}
