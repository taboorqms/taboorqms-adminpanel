package com.taboor.qms.admin.panel.serviceImpl;

import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.taboor.qms.admin.panel.service.ServiceTABService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.utils.HttpHeadersUtils;

@Service
public class ServiceTABServiceImpl implements ServiceTABService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public ServiceTAB getServiceTAB(@Valid Long serviceId) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/services/get/id?serviceId=" + serviceId;
		ResponseEntity<ServiceTAB> getServiceTABByIdResponse = restTemplate.getForEntity(uri, ServiceTAB.class);

		if (!getServiceTABByIdResponse.getStatusCode().equals(HttpStatus.OK)
				|| getServiceTABByIdResponse.getBody() == null)
//		Optional<ServiceTAB> service = serviceTABRepository.findById(serviceId);
//		if (!service.isPresent())
			throw new TaboorQMSServiceException(xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorDetails());
		return getServiceTABByIdResponse.getBody();
	}

	@Override
	public ServiceTAB saveServiceTAB(@Valid ServiceTAB serviceTAB) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/services/save";
		HttpEntity<ServiceTAB> entity = new HttpEntity<>(serviceTAB, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<ServiceTAB> createServiceResponse = restTemplate.postForEntity(uri, entity, ServiceTAB.class);

		if (!createServiceResponse.getStatusCode().equals(HttpStatus.OK) || createServiceResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorDetails());
		return createServiceResponse.getBody();
	}

}
