package com.taboor.qms.admin.panel.response;

import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetServiceCenterSubscriptionResponse extends GenStatusResponse {

	private ServiceCenterSubscription subscriptions;
	private int servicesCount;

	public ServiceCenterSubscription getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(ServiceCenterSubscription subscriptions) {
		this.subscriptions = subscriptions;
	}

	public int getServicesCount() {
		return servicesCount;
	}

	public void setServicesCount(int servicesCount) {
		this.servicesCount = servicesCount;
	}

	public GetServiceCenterSubscriptionResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetServiceCenterSubscriptionResponse(int applicationStatusCode, String applicationStatusResponse,
			ServiceCenterSubscription subscriptions, int count) {
		super(applicationStatusCode, applicationStatusResponse);
		this.subscriptions = subscriptions;
		this.servicesCount = count;
	}

}
