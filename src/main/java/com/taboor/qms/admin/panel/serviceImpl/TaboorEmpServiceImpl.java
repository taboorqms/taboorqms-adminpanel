package com.taboor.qms.admin.panel.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.payload.RegisterTaboorEmpPayload;
import com.taboor.qms.admin.panel.response.GetEmployeeProfileResponse;
import com.taboor.qms.admin.panel.service.TaboorEmpService;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.TaboorEmployee;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.payload.RegisterUserPayload;
import com.taboor.qms.core.response.GetTaboorEmployeeListResponse;
import com.taboor.qms.core.response.GetTaboorEmployeeListResponse.TaboorEmployeeObject;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.response.RegisterUserResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.core.utils.RestUtil;

@Service
public class TaboorEmpServiceImpl implements TaboorEmpService {

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Value("${url.microservice.user.management}")
	private String userManagementMicroserviceURL;

	@Override
	public TaboorEmployee saveTaboorEmp(TaboorEmployee taboorEmployee) throws TaboorQMSServiceException {
		taboorEmployee = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/tabooremployees/save",
				new HttpEntity<>(taboorEmployee, HttpHeadersUtils.getApplicationJsonHeader()), TaboorEmployee.class,
				xyzErrorMessage.TABOOREMP_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.TABOOREMP_NOT_CREATED.getErrorDetails());

		return taboorEmployee;
	}

	@Override
	public TaboorEmployee findByUserId(Long userId) throws TaboorQMSServiceException {
		return RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/tabooremployees/get/userId?userId=" + userId,
				TaboorEmployee.class, xyzErrorMessage.TABOOREMP_NOT_EXIST.getErrorCode(),
				xyzErrorMessage.TABOOREMP_NOT_EXIST.getErrorDetails());
	}

	@Override
	public GetEmployeeProfileResponse getProfile() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		TaboorEmployee taboorEmployee = findByUserId(session.getUser().getUserId());

		Integer centerCount = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/servicecenters/count",
				Integer.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		Integer empCount = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/tabooremployees/count", Integer.class,
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		User user = taboorEmployee.getUser();

		List<UserPrivilege> privilegeList = GenericMapper
				.convertListToObject(
						RestUtil.getRequest(
								dbConnectorMicroserviceURL + "/tab/userprivilegemapper/get/user?userId="
										+ user.getUserId(),
								List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
								xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails()),
						new TypeReference<List<UserPrivilege>>() {
						});

		user.setRoleName(user.getRoleName().replaceAll("_", " "));
		user.setPassword(null);

		return new GetEmployeeProfileResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_FETCHED.getMessage(), user, centerCount, empCount, privilegeList);
	}

	@Override
	public GetValuesResposne registerEmp(RegisterTaboorEmpPayload registerTaboorEmpPayload, MultipartFile profileImage)
			throws TaboorQMSServiceException, Exception {
//		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
//				.getPrincipal();
//		Session session = myUserDetails.getSession();

		UserRole userRole = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/userroles/get/id?roleId=" + registerTaboorEmpPayload.getRoleId(),
				UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
				xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());

		RegisterUserPayload registerUserPayload = new RegisterUserPayload();
		registerUserPayload.setName(registerTaboorEmpPayload.getName());
		registerUserPayload.setEmail(registerTaboorEmpPayload.getEmail());
		registerUserPayload.setPassword(registerTaboorEmpPayload.getPassword());
		registerUserPayload.setPhoneNumber(registerTaboorEmpPayload.getPhoneNumber());
		registerUserPayload.setUserRole(userRole);
		registerUserPayload.setAssignRolePrivileges(true);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("RegisterUserPayload", registerUserPayload);
		if (profileImage == null)
			body.add("UserProfileImage", null);
		else
			body.add("UserProfileImage", profileImage.getResource());

		RegisterUserResponse registerUserResponse = RestUtil.postRequest(
				userManagementMicroserviceURL + "/user/register",
				new HttpEntity<>(body, HttpHeadersUtils.getMultipartFormDataHeader()), RegisterUserResponse.class,
				xyzErrorMessage.USER_NOT_REGISTERED.getErrorCode(),
				xyzErrorMessage.USER_NOT_REGISTERED.getErrorDetails());

		TaboorEmployee taboorEmployee = new TaboorEmployee();
		taboorEmployee.setEmployeeNumber(nextEmpNumber());
		taboorEmployee.setUser(registerUserResponse.getUser());
		taboorEmployee = saveTaboorEmp(taboorEmployee);

		UserConfiguration userConfiguration = new UserConfiguration();
		userConfiguration.setUser(registerUserResponse.getUser());
		userConfiguration.setEmailAllowed(false);
		userConfiguration.setInAppNotificationAllowed(false);
		userConfiguration.setSmsAllowed(false);

		userConfiguration = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/userconfigurations/save",
				new HttpEntity<>(userConfiguration, HttpHeadersUtils.getApplicationJsonHeader()),
				UserConfiguration.class, xyzErrorMessage.USERCONFIGURATION_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.USERCONFIGURATION_NOT_CREATED.getErrorDetails());

		UserNotificationType userNotificationType = new UserNotificationType();
		userNotificationType.setUser(registerUserResponse.getUser());
		userNotificationType.setNewOrUpdateUser(false);
		userNotificationType.setNewOrUpdateServiceCenter(false);

		userNotificationType = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/usernotificationtypes/save",
				new HttpEntity<>(userNotificationType, HttpHeadersUtils.getApplicationJsonHeader()),
				UserNotificationType.class, xyzErrorMessage.USERNOTIFICATIONTYPE_NOT_CREATED.getErrorCode(),
				xyzErrorMessage.USERNOTIFICATIONTYPE_NOT_CREATED.getErrorDetails());

		RSocketPayload payload = new RSocketPayload();
		payload.setNotificationSubject(RSocketNotificationSubject.NEW_TABOOR_USER);
		payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
		payload.setInformation(Arrays.asList(taboorEmployee.getUser().getName()));

//		RSocketInitializer.fireAndForget(payload);
		RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
				new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TABOOR_EMP_ADDED.getMessage(), null);
	}

	@Override
	public GetValuesResposne updateEmp(RegisterTaboorEmpPayload registerTaboorEmpPayload)
			throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();

		RegisterUserPayload registerUserPayload = new RegisterUserPayload();
		if (registerTaboorEmpPayload.getUserId() == -1)
			registerUserPayload.setUser(session.getUser());
		else
			registerUserPayload.setUser(RestUtil.getRequest(
					dbConnectorMicroserviceURL + "/tab/users/get/id?userId=" + registerTaboorEmpPayload.getUserId(),
					User.class, xyzErrorMessage.USER_NOT_EXISTS.getErrorCode(),
					xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails()));
		registerUserPayload.setName(registerTaboorEmpPayload.getName());
                if(registerTaboorEmpPayload.getEmail() != null && !registerTaboorEmpPayload.getEmail().equals("")){
                    if(!registerUserPayload.getUser().getEmail().equals(registerTaboorEmpPayload.getEmail())){
                        registerUserPayload.setEmail(registerTaboorEmpPayload.getEmail());
                    }
                } else{
                    registerUserPayload.setEmail(null);
                }
//		registerUserPayload.setPassword(registerTaboorEmpPayload.getPassword());
		registerUserPayload.setPhoneNumber(registerTaboorEmpPayload.getPhoneNumber());
		if (registerTaboorEmpPayload.getRoleId() != -1) {
			UserRole userRole = RestUtil.getRequest(
					dbConnectorMicroserviceURL + "/tab/userroles/get/id?roleId=" + registerTaboorEmpPayload.getRoleId(),
					UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
					xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());
			registerUserPayload.setUserRole(userRole);
		} else
			registerUserPayload.setUserRole(null);
		registerUserPayload.setAssignRolePrivileges(true);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("UpdateUserPayload", registerUserPayload);
		body.add("UserProfileImage", null);

		RegisterUserResponse registerUserResponse = RestUtil.postRequest(userManagementMicroserviceURL + "/user/update",
				new HttpEntity<>(body, HttpHeadersUtils.getMultipartFormDataHeader()), RegisterUserResponse.class,
				xyzErrorMessage.USER_NOT_UPDATED.getErrorCode(), xyzErrorMessage.USER_NOT_UPDATED.getErrorDetails());

		RSocketPayload payload = new RSocketPayload();
		payload.setNotificationSubject(RSocketNotificationSubject.UPDATE_TABOOR_USER);
		payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
		payload.setInformation(Arrays.asList(registerUserResponse.getUser().getName()));

//		RSocketInitializer.fireAndForget(payload);
		RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
				new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TABOOR_EMP_UPDATED.getMessage(), null);
	}

	@Override
	public GetValuesResposne getNextEmpNumber() throws TaboorQMSServiceException, Exception {
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("NextEmpNumber", nextEmpNumber());
		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), "Next Employee Number fetched Successfully.",
				values);
	}

	@Override
	public String nextEmpNumber() throws RestClientException, TaboorQMSServiceException {
		String nextEmpNumber = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/tabooremployees/getNextEmpNumber",
				String.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		if (nextEmpNumber.equals("FirstEntry"))
			nextEmpNumber = "TAB0001";
		return nextEmpNumber;
	}

	@Override
	public GetTaboorEmployeeListResponse getListing() throws TaboorQMSServiceException, Exception {

		GetTaboorEmployeeListResponse response = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/tabooremployees/getList", GetTaboorEmployeeListResponse.class,
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		for (TaboorEmployeeObject emp : response.getEmployees())
			emp.getEmployee().getUser().setRoleName(emp.getEmployee().getUser().getRoleName().replaceAll("_", " "));

		return response;
	}

	@Override
	public GetTaboorEmployeeListResponse getList() throws TaboorQMSServiceException, Exception {
		List<TaboorEmployee> employees = GenericMapper.convertListToObject(
				RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/tabooremployees/get/all", List.class,
						xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
						xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails()),
				new TypeReference<List<TaboorEmployee>>() {
				});
		List<TaboorEmployeeObject> response = new ArrayList<TaboorEmployeeObject>();
		for (TaboorEmployee emp : employees)
			response.add(new TaboorEmployeeObject(emp, new ArrayList<UserPrivilege>(), new Long(0)));

		return new GetTaboorEmployeeListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_FETCHED.getMessage(), response, 0);
	}

}
