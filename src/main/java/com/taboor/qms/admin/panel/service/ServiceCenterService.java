package com.taboor.qms.admin.panel.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.admin.panel.payload.AddPaymentMethodPayload;
import com.taboor.qms.admin.panel.payload.AddServicePayload;
import com.taboor.qms.admin.panel.payload.AddSupportTicketPayload;
import com.taboor.qms.admin.panel.payload.PaySubscriptionChargesPayload;
import com.taboor.qms.admin.panel.payload.RegisterServiceCenterPayload;
import com.taboor.qms.admin.panel.payload.UpdatePaymentMethodPayload;
import com.taboor.qms.admin.panel.payload.UpdateServiceCenterPayload;
import com.taboor.qms.admin.panel.payload.UpdateSubscriptionPlanPayload;
import com.taboor.qms.admin.panel.payload.UpdateSupportTicketStatusPayload;
import com.taboor.qms.admin.panel.response.GetServiceCenterConfigurationResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterProfileResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterServiceListResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterSubscriptionListResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterSubscriptionResponse;
import com.taboor.qms.admin.panel.response.GetSupportTicketListResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.payload.AddUserBankCardPayload;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetPaymentPlanListResponse;
import com.taboor.qms.core.response.GetServiceCentreListResponse;
import com.taboor.qms.core.response.GetServiceCentreListingResponse;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface ServiceCenterService {

	ServiceCenter saveServiceCenter(ServiceCenter serviceCenter) throws TaboorQMSServiceException;

	GetValuesResposne registerServiceCenter(@Valid RegisterServiceCenterPayload registerServiceCenterPayload,
			MultipartFile profileImage) throws TaboorQMSServiceException, Exception;

	GetPaymentPlanListResponse getSelectedPaymentPlan(Long serviceCenterId) throws TaboorQMSServiceException, Exception;

	GetServiceCenterServiceListResponse getProvidedServices() throws TaboorQMSServiceException, Exception;

	////////// Talha working

	GetServiceCenterProfileResponse getProfile(GetByIdPayload payload) throws TaboorQMSServiceException, Exception;

	GetStatisticsResponse getVotes(GetActivitiesPayload payload) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteServiceCentre(long serviceCentreId) throws TaboorQMSServiceException, Exception;

	GenStatusResponse updateServiceCentre(UpdateServiceCenterPayload payload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse addService(AddServicePayload addServicePayload) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteService(long serviceCenterServiceId) throws TaboorQMSServiceException, Exception;

	GetServiceCenterConfigurationResponse getConfiguration(GetByIdPayload getByIdPayload)
			throws TaboorQMSServiceException, Exception;

	GetServiceCenterConfigurationResponse assignDefaultConfiguration(int type, long serviceCenterId)
			throws TaboorQMSServiceException, Exception;

	GetServiceCenterConfigurationResponse updateConfiguration(ServiceCenterConfiguration serviceCenterConfiguration)
			throws TaboorQMSServiceException, Exception;

	List<UserBankCard> getPaymentMethods(GetByIdPayload getByIdPayload) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deletePaymentMethod(long paymentMethodId) throws TaboorQMSServiceException, Exception;

	UserBankCard updatePaymentMethod(UpdatePaymentMethodPayload payload) throws TaboorQMSServiceException, Exception;

	GenStatusResponse addPaymentMethod(AddPaymentMethodPayload payload) throws TaboorQMSServiceException, Exception;

	List<Invoice> getInvoices(long serviceCentreId) throws TaboorQMSServiceException, Exception;

	GetServiceCenterSubscriptionResponse getSubscription(GetByIdPayload getByIdPayload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse upgradeSubscription(UpdateSubscriptionPlanPayload payload)
			throws TaboorQMSServiceException, Exception;

	GetBranchListResponse getBranches() throws TaboorQMSServiceException, Exception;

	GetServiceCentreListingResponse getAllListing() throws TaboorQMSServiceException, Exception;

	GetValuesResposne addBankCard(AddUserBankCardPayload payload) throws TaboorQMSServiceException, Exception;

	GenStatusResponse paySubscriptionCharges(PaySubscriptionChargesPayload paySubscriptionChargesPayload)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne getDashboardActivities(GetActivitiesPayload getActivitiesPayload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse addSupportTicket(AddSupportTicketPayload addSupportTicketPayload,
			@Valid List<MultipartFile> files) throws TaboorQMSServiceException, Exception;

	GetSupportTicketListResponse getSupportTickets() throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteSupportTicket(long supportTicketId) throws TaboorQMSServiceException, Exception;

	GetStatisticsResponse getDashboardStatistics(GetActivitiesPayload getActivitiesPayload)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne getDashboardRatings(GetActivitiesPayload getActivitiesPayload)
			throws TaboorQMSServiceException, Exception;

	GetServiceCentreListResponse getAll() throws TaboorQMSServiceException, Exception;

	GetServiceCenterSubscriptionListResponse getActiveSubscriptionList() throws TaboorQMSServiceException, Exception;

	GenStatusResponse updateSupportTicketStatus(UpdateSupportTicketStatusPayload payload)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse assignEmpToSupportTicket(GetByIdListPayload payload) throws TaboorQMSServiceException, Exception;

	GetValuesResposne countServiceCenter() throws TaboorQMSServiceException, Exception;

}
