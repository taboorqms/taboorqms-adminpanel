package com.taboor.qms.admin.panel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.taboor.qms.core.auth.CustomAccessDeniedHandler;
import com.taboor.qms.core.auth.JwtAuthenticationEntryPoint;
import com.taboor.qms.core.auth.JwtAuthenticationFilter;
import com.taboor.qms.core.utils.PrivilegeName;

@EnableWebSecurity
class AuthWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService myUserDetailsService;

	@Autowired
	private JwtAuthenticationFilter jwtAuthenticationFilter;

	@Autowired
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	CustomAccessDeniedHandler customAccessDeniedHandler;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
				.exceptionHandling().accessDeniedHandler(customAccessDeniedHandler).and()
				// don't create session
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS).permitAll()
				.antMatchers("/h2-console/**", "/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/**")
				.permitAll()//
				.antMatchers("/serviceCenter/getListing").hasAnyRole(PrivilegeName.Service_Center_View.name())
				.antMatchers("/serviceCenter/count").hasAnyRole(PrivilegeName.Service_Center_View.name())
				.antMatchers("/serviceCenter/getServices").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/branchCounter/addOrUpdate").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/branchCounter/get/branch").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/branchCounter/getNextCounterNumber").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/branchCounter/delete").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/branch/add").hasAnyRole(PrivilegeName.Branch_Edit.name())//
				.antMatchers("/branch/update").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/branch/getServices").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/agent/add").hasAnyRole(PrivilegeName.Agent_Edit.name())//
				.antMatchers("/agent/update").hasAnyRole(PrivilegeName.Agent_Edit.name())//
				.antMatchers("/agent/delete").hasAnyRole(PrivilegeName.Agent_Edit.name())
				.antMatchers("/agent/get/serviceCenter").hasAnyRole(PrivilegeName.Branch_Edit.name())
				.antMatchers("/agent/getNextCredentials").hasAnyRole(PrivilegeName.Agent_Edit.name())
				.antMatchers("/agent/getPrivileges").hasAnyRole(PrivilegeName.Agent_Edit.name())
				.antMatchers("/queue/getListing").hasAnyRole(PrivilegeName.Queue_View.name())
				.antMatchers("/queue/getDetails").hasAnyRole(PrivilegeName.Queue_View.name())
				.antMatchers("/queue/count").hasAnyRole(PrivilegeName.Queue_View.name())
				.antMatchers("/branch/getListing").hasAnyRole(PrivilegeName.Branch_View.name())
				.antMatchers("/branch/getList")
				.hasAnyRole(PrivilegeName.User_Managment.name(), PrivilegeName.Agent_Edit.name())
				.antMatchers("/branch/getCompleteDetails/id").hasAnyRole(PrivilegeName.Branch_View.name())
				.antMatchers("/branch/delete").hasAnyRole(PrivilegeName.Branch_Edit.name()).antMatchers("/branch/count")
				.hasAnyRole(PrivilegeName.Branch_View.name()).antMatchers("/branch/status/change")
				.hasAnyRole(PrivilegeName.Branch_Edit.name()).antMatchers("/branch/clone")
				.hasAnyRole(PrivilegeName.Branch_Edit.name()).antMatchers("/serviceCenter/service/add")
				.hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/dashboard/activities")
				.hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/serviceCenter/dashboard/statistics")
				.hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())//
				.antMatchers("/serviceCenter/dashboard/ratings").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/serviceCenterEmp/getProfile").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())//
				.antMatchers("/serviceCenterEmp/getNextEmpNumber").hasAnyRole(PrivilegeName.User_Managment.name())
                                .antMatchers("/serviceCenterEmp/delete").hasAnyRole(PrivilegeName.User_Managment.name())
				.antMatchers("/serviceCenter/supportTicket/add").hasAnyRole(PrivilegeName.Support_Ticket_Edit.name())
				.antMatchers("/serviceCenter/supportTicket/get").hasAnyRole(PrivilegeName.Support_Ticket_View.name())
				.antMatchers("/serviceCenter/supportTicket/delete").hasAnyRole(PrivilegeName.Support_Ticket_Edit.name())

				.antMatchers("/serviceCenter/getProfile").hasAnyRole(PrivilegeName.Service_Center_Profile_View.name())
				.antMatchers("/serviceCenter/config/get").hasAnyRole(PrivilegeName.Service_Center_Profile_View.name())
				.antMatchers("/serviceCenter/config/update").hasAnyRole(PrivilegeName.Service_Center_Profile_View.name())
				.antMatchers("/serviceCenter/getInvoices").hasAnyRole(PrivilegeName.Service_Center_Profile_View.name())
				.antMatchers("/serviceCenter/getPaymentMethods").hasAnyRole(PrivilegeName.Service_Center_Profile_View.name())
				.antMatchers("/serviceCenter/getSubscription").hasAnyRole(PrivilegeName.Service_Center_Profile_View.name())
				.antMatchers("/serviceCenter/getVotes").hasAnyRole(PrivilegeName.Service_Center_Profile_View.name())

				.antMatchers("/serviceCenter/service/add").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/service/delete").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/config/default/assign").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/updatePayment").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/paymentMethod/delete").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/edit").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/delete").hasAnyRole(PrivilegeName.Service_Center_Edit.name())
				.antMatchers("/serviceCenter/upgradeSubscription").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				.antMatchers("/serviceCenter/paymentMethod/add").hasAnyRole(PrivilegeName.Service_Center_Profile_Edit.name())
				
				.antMatchers("/serviceCenter/supportTicket/status/update")
				.hasAnyRole(PrivilegeName.Support_Ticket_Edit.name())
				.antMatchers("/serviceCenter/supportTicket/employee/assign")
				.hasAnyRole(PrivilegeName.Support_Ticket_Edit.name()).antMatchers("/agent/getListing")
				.hasAnyRole(PrivilegeName.Agent_View.name()).antMatchers("/agent/getDetails")
				.hasAnyRole(PrivilegeName.Agent_View.name()).antMatchers("/agent/count")
				.hasAnyRole(PrivilegeName.Agent_View.name()).antMatchers("/agent/getStatistics")
				.hasAnyRole(PrivilegeName.Agent_View.name()).antMatchers("/serviceCenterEmp/getList")
				.hasAnyRole(PrivilegeName.User_View.name()).antMatchers("/branch/service/add")
				.hasAnyRole(PrivilegeName.Branch_Edit.name()).antMatchers("/branch/service/delete")
				.hasAnyRole(PrivilegeName.Branch_Edit.name()).antMatchers("/branchCounter/status/change")
				.hasAnyRole(PrivilegeName.Branch_Edit.name()).antMatchers("/branch/workingHours/update")
				.hasAnyRole(PrivilegeName.Branch_Edit.name()).antMatchers("/bankAccount/get/all")
				.hasAnyRole(PrivilegeName.Invoice_Edit.name()).antMatchers("/serviceCenter/get/all").hasAnyRole()//
				.antMatchers("/serviceCenter/getActiveSubscriptionList").hasAnyRole(PrivilegeName.Invoice_Edit.name())
				.antMatchers("/invoice/getListing").hasAnyRole(PrivilegeName.Invoice_View.name())
				.antMatchers("/invoice/getDetails").hasAnyRole(PrivilegeName.Invoice_View.name())
				.antMatchers("/invoice/getNextInvoiceNumber").hasAnyRole(PrivilegeName.Invoice_Edit.name())
				.antMatchers("/invoice/delete").hasAnyRole(PrivilegeName.Invoice_Edit.name())
				.antMatchers("/invoice/addOrUpdate").hasAnyRole(PrivilegeName.Invoice_Edit.name())
				.antMatchers("/invoice/get/serviceCenter").hasAnyRole()//
				.antMatchers("/setting/configuration/get/user").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/setting/configuration/update").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/setting/notificationType/get/user")
				.hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())//
				.antMatchers("/setting/notificationType/update").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/serviceCenterEmp/register").hasAnyRole(PrivilegeName.User_Managment.name())
				.antMatchers("/serviceCenterEmp/update").hasAnyRole(PrivilegeName.User_Managment.name())
				.antMatchers("/paymentPlan/getDetails").hasAnyRole(PrivilegeName.Subscription_Plan_View.name())
				.antMatchers("/paymentPlan/getListing").hasAnyRole(PrivilegeName.Subscription_Plan_View.name())
				.antMatchers("/paymentPlan/addOrUpdate").hasAnyRole(PrivilegeName.Subscription_Plan_Edit.name())
				.antMatchers("/paymentPlan/delete").hasAnyRole(PrivilegeName.Subscription_Plan_Edit.name())
				.antMatchers("/taboorEmployee/getProfile").hasAnyRole(PrivilegeName.Admin_Panel_View_Only.name())
				.antMatchers("/taboorEmployee/getListing").hasAnyRole(PrivilegeName.User_View.name())
				.antMatchers("/taboorEmployee/getListing").hasAnyRole(PrivilegeName.User_View.name())
				.antMatchers("/taboorEmployee/getList").hasAnyRole(PrivilegeName.Support_Ticket_Edit.name())
				.antMatchers("/taboorEmployee/register").hasAnyRole(PrivilegeName.User_Managment.name())
				.antMatchers("/taboorEmployee/update").hasAnyRole(PrivilegeName.User_Managment.name())
				.antMatchers("/taboorEmployee/getNextEmpNumber").hasAnyRole(PrivilegeName.User_Managment.name())

				// ByPass Routes
				.antMatchers("/serviceCenter/register", "/serviceCenter/rsocket", "/serviceSector/get/all",
						"/paymentPlan/get/all", "/serviceCenter/subscription/pay", "/serviceCenter/bankCard/add")
				.permitAll().anyRequest().denyAll();

		httpSecurity.headers().frameOptions().disable();
		httpSecurity.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

	}

}