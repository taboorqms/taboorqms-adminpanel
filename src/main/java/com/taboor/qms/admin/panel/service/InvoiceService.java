package com.taboor.qms.admin.panel.service;

import java.time.OffsetDateTime;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.taboor.qms.admin.panel.payload.AddInvoicePayload;
import com.taboor.qms.admin.panel.response.GetInvoiceListResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.model.TaboorBankAccount;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.Currency;
import com.taboor.qms.core.utils.InvoiceType;

@Service
public interface InvoiceService {

	GetInvoiceListResponse getAll() throws TaboorQMSServiceException, Exception;

	GetInvoiceListResponse getById(long invoiceId) throws TaboorQMSServiceException, Exception;

	GetInvoiceListResponse getByServiceCenterId(long serviceCenterId) throws TaboorQMSServiceException, Exception;

	GetValuesResposne addOrUpdate(AddInvoicePayload addInvoicePayload) throws TaboorQMSServiceException, Exception;

	String getNextInvoiceNumber() throws RestClientException, TaboorQMSServiceException;

	void createAutoInvoice(Double amount, Currency currency, OffsetDateTime issuedOn, OffsetDateTime dueDate,
			OffsetDateTime expiredOn, ServiceCenterSubscription serviceCenterSubscription,
			TaboorBankAccount taboorBankAccount, InvoiceType invoiceType) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteInvoice(long invoiceId) throws TaboorQMSServiceException, Exception;

}
