package com.taboor.qms.admin.panel.payload;

public class UpdateServiceCenterPayload {

	private Long serviceCenterId;
	private String serviceCenterName;
	private String serviceCenterArabicName;
	private String country;
	private String email;
	private String phoneNumber;
	
	public Long getServiceCenterId() {
		return serviceCenterId;
	}
	public void setServiceCenterId(Long serviceCenterId) {
		this.serviceCenterId = serviceCenterId;
	}
	public String getServiceCenterName() {
		return serviceCenterName;
	}
	public void setServiceCenterName(String serviceCenterName) {
		this.serviceCenterName = serviceCenterName;
	}
	public String getServiceCenterArabicName() {
		return serviceCenterArabicName;
	}
	public void setServiceCenterArabicName(String serviceCenterArabicName) {
		this.serviceCenterArabicName = serviceCenterArabicName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
		
}
