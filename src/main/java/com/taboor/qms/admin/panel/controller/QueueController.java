package com.taboor.qms.admin.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.service.QueueService;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GetQueueListingResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@RestController
@RequestMapping("/queue")
@CrossOrigin(origins = "*")
public class QueueController {

	private static final Logger logger = LoggerFactory.getLogger(QueueController.class);

	@Autowired
	QueueService queueService;

	@GetMapping("/getListing")
	public @ResponseBody GetQueueListingResponse getQueueListing() throws Exception, Throwable {
		logger.info("Calling Get Queue Listing - API");
		return queueService.getQueueListing();
	}

	@GetMapping("/count")
	public @ResponseBody GetValuesResposne countQueue() throws Exception, Throwable {
		logger.info("Calling Get Queue Count - API");
		return queueService.countQueue();
	}

	@PostMapping("/getDetails")
	public @ResponseBody GetQueueListingResponse getQueueListingByQueue(@RequestBody GetByIdPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Queue Details - API");
		return queueService.getQueueListingByQueueId(payload.getId());
	}

}
