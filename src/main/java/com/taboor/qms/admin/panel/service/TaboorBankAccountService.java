package com.taboor.qms.admin.panel.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.admin.panel.response.GetTaboorBankAccountListResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.TaboorBankAccount;

@Service
public interface TaboorBankAccountService {

	GetTaboorBankAccountListResponse getAll() throws TaboorQMSServiceException, Exception;

	TaboorBankAccount getDefaultAccount() throws TaboorQMSServiceException, Exception;

}
