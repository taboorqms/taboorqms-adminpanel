package com.taboor.qms.admin.panel.payload;

import java.util.List;

import com.taboor.qms.core.model.ServiceTAB;

public class AddBranchServicePayload {

	private List<ServiceTAB> serviceList;
	private Long branchId;

	public List<ServiceTAB> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<ServiceTAB> serviceList) {
		this.serviceList = serviceList;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

}
