package com.taboor.qms.admin.panel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.taboor.qms.admin.panel.payload.AddBranchCounterPayload;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchCounterListResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface BranchCounterService {

	GetBranchCounterListResponse getByBranch(long branchId) throws TaboorQMSServiceException, Exception;

	GetValuesResposne addOrUpdateCounter(AddBranchCounterPayload addBranchCounterPayload)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne getNextCounterNumber(long branchId) throws TaboorQMSServiceException, Exception;

	BranchCounter getById(Long branchCounterId) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteCounterList(List<Long> branchCounterIdList) throws TaboorQMSServiceException, Exception;

	GenStatusResponse changeCounterStatus(long branchCounterId) throws TaboorQMSServiceException, Exception;

}
