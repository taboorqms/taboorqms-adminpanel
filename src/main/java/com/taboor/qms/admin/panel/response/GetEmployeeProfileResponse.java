package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetEmployeeProfileResponse extends GenStatusResponse {

	private User user;
	private String address;
	private int branchCount;
	private int agentCount;
	private int centerCount;
	private int userCount;
	private List<UserPrivilege> privileges;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getBranchCount() {
		return branchCount;
	}

	public void setBranchCount(int branchCount) {
		this.branchCount = branchCount;
	}

	public int getAgentCount() {
		return agentCount;
	}

	public void setAgentCount(int agentCount) {
		this.agentCount = agentCount;
	}

	public int getCenterCount() {
		return centerCount;
	}

	public void setCenterCount(int centerCount) {
		this.centerCount = centerCount;
	}

	public int getUserCount() {
		return userCount;
	}

	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}

	public List<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

	public GetEmployeeProfileResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetEmployeeProfileResponse(int applicationStatusCode, String applicationStatusResponse, User user,
			String address, int branchCount, int agentCount, List<UserPrivilege> privileges) {
		super(applicationStatusCode, applicationStatusResponse);
		this.user = user;
		this.address = address;
		this.branchCount = branchCount;
		this.agentCount = agentCount;
		this.privileges = privileges;
	}

	public GetEmployeeProfileResponse(int applicationStatusCode, String applicationStatusResponse, User user,
			int centerCount, int userCount, List<UserPrivilege> privileges) {
		super(applicationStatusCode, applicationStatusResponse);
		this.user = user;
		this.centerCount = centerCount;
		this.userCount = userCount;
		this.privileges = privileges;
	}

}
