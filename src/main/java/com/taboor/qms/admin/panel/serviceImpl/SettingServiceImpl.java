package com.taboor.qms.admin.panel.serviceImpl;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.taboor.qms.admin.panel.response.GetUserConfigurationsResponse;
import com.taboor.qms.admin.panel.response.GetUserNotificationTypeResponse;
import com.taboor.qms.admin.panel.service.SettingService;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.utils.HttpHeadersUtils;

@Service
public class SettingServiceImpl implements SettingService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public GetUserConfigurationsResponse getUserConfigurations() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		String uri = dbConnectorMicroserviceURL + "/tab/userconfigurations/get/userId?userId="
				+ session.getUser().getUserId();
		ResponseEntity<UserConfiguration> getUserConfigurationResponse = restTemplate.getForEntity(uri,
				UserConfiguration.class);
		getUserConfigurationResponse.getBody().setUser(null);
		return new GetUserConfigurationsResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_CONFIGURATION_FETCHED.getMessage(), getUserConfigurationResponse.getBody());
	}

	@Override
	public GenStatusResponse updateUserConfigurations(UserConfiguration userConfiguration)
			throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		String uri = dbConnectorMicroserviceURL + "/tab/userconfigurations/get/userId?userId="
				+ session.getUser().getUserId();
		ResponseEntity<UserConfiguration> getUserConfigurationResponse = restTemplate.getForEntity(uri,
				UserConfiguration.class);

		userConfiguration.setConfigurationId(getUserConfigurationResponse.getBody().getConfigurationId());
		userConfiguration.setUser(getUserConfigurationResponse.getBody().getUser());

		uri = dbConnectorMicroserviceURL + "/tab/userconfigurations/save";
		HttpEntity<UserConfiguration> userConfigurationEntity = new HttpEntity<>(userConfiguration,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<UserConfiguration> saveUserConfigurationResponse = restTemplate.postForEntity(uri,
				userConfigurationEntity, UserConfiguration.class);
		if (!saveUserConfigurationResponse.getStatusCode().equals(HttpStatus.OK)
				|| saveUserConfigurationResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERCONFIGURATION_NOT_UPDATED.getErrorCode() + "::"
					+ xyzErrorMessage.USERCONFIGURATION_NOT_UPDATED.getErrorDetails());

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_CONFIGURATION_UPDATED.getMessage());
	}

	@Override
	public GetUserNotificationTypeResponse getUserNotificationType() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		String uri = dbConnectorMicroserviceURL + "/tab/usernotificationtypes/get/userId?userId="
				+ session.getUser().getUserId();
		ResponseEntity<UserNotificationType> getUserNotificationTypeResponse = restTemplate.getForEntity(uri,
				UserNotificationType.class);
		getUserNotificationTypeResponse.getBody().setUser(null);
		return new GetUserNotificationTypeResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_CONFIGURATION_FETCHED.getMessage(), getUserNotificationTypeResponse.getBody());
	}

	@Override
	public GenStatusResponse updateNotificationType(UserNotificationType userNotificationType)
			throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		String uri = dbConnectorMicroserviceURL + "/tab/usernotificationtypes/get/userId?userId="
				+ session.getUser().getUserId();
		ResponseEntity<UserNotificationType> getUserNotificationTypeResponse = restTemplate.getForEntity(uri,
				UserNotificationType.class);

		userNotificationType.setNotificationTypeId(getUserNotificationTypeResponse.getBody().getNotificationTypeId());
		userNotificationType.setUser(getUserNotificationTypeResponse.getBody().getUser());

		uri = dbConnectorMicroserviceURL + "/tab/usernotificationtypes/save";
		HttpEntity<UserNotificationType> userNotificationTypeEntity = new HttpEntity<>(userNotificationType,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<UserNotificationType> saveUserNotificationTypeResponse = restTemplate.postForEntity(uri,
				userNotificationTypeEntity, UserNotificationType.class);
		if (!saveUserNotificationTypeResponse.getStatusCode().equals(HttpStatus.OK)
				|| saveUserNotificationTypeResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.USERNOTIFICATIONTYPE_NOT_UPDATED.getErrorCode() + "::"
					+ xyzErrorMessage.USERNOTIFICATIONTYPE_NOT_UPDATED.getErrorDetails());

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_NOTIFICATION_TYPE_UPDATED.getMessage());
	}

}
