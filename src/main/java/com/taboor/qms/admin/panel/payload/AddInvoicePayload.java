package com.taboor.qms.admin.panel.payload;

import java.time.LocalDate;
import java.util.List;

import com.taboor.qms.core.model.InvoiceItem;
import com.taboor.qms.core.utils.Currency;
import com.taboor.qms.core.utils.InvoiceStatus;

public class AddInvoicePayload {

	private Long invoiceId;

	private Long bankAccountId;
	private Long serviceCenterSubscriptionId;
	private String centerName;
	private String email;
	private LocalDate issuedOn;
	private LocalDate dueOn;
	private LocalDate expiredOn;
	private Currency currency;
	private Boolean draft;
	private int language;
	private List<InvoiceItem> items;
	private Double totalAmount;
	private String notes;

	private InvoiceStatus invoiceStatus;

	private int updateStatus;

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public Long getServiceCenterSubscriptionId() {
		return serviceCenterSubscriptionId;
	}

	public void setServiceCenterSubscriptionId(Long serviceCenterSubscriptionId) {
		this.serviceCenterSubscriptionId = serviceCenterSubscriptionId;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getIssuedOn() {
		return issuedOn;
	}

	public void setIssuedOn(LocalDate issuedOn) {
		this.issuedOn = issuedOn;
	}

	public LocalDate getDueOn() {
		return dueOn;
	}

	public void setDueOn(LocalDate dueOn) {
		this.dueOn = dueOn;
	}

	public LocalDate getExpiredOn() {
		return expiredOn;
	}

	public void setExpiredOn(LocalDate expiredOn) {
		this.expiredOn = expiredOn;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Boolean getDraft() {
		return draft;
	}

	public void setDraft(Boolean draft) {
		this.draft = draft;
	}

	public int getLanguage() {
		return language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public List<InvoiceItem> getItems() {
		return items;
	}

	public void setItems(List<InvoiceItem> items) {
		this.items = items;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public InvoiceStatus getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(int updateStatus) {
		this.updateStatus = updateStatus;
	}

}
