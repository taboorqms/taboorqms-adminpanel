package com.taboor.qms.admin.panel.serviceImpl;

import java.io.IOException;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.taboor.qms.admin.panel.payload.AddBranchCounterPayload;
import com.taboor.qms.admin.panel.payload.AddBranchPayload;
import com.taboor.qms.admin.panel.payload.AddBranchServicePayload;
import com.taboor.qms.admin.panel.payload.CloneBranchPayload;
import com.taboor.qms.admin.panel.payload.WorkingHours;
import com.taboor.qms.admin.panel.response.GetBranchCompleteDetailsResponse;
import com.taboor.qms.admin.panel.response.GetBranchCompleteDetailsResponse.BranchServicesCompleteObj;
import com.taboor.qms.admin.panel.service.AgentService;
import com.taboor.qms.admin.panel.service.BranchCounterService;
import com.taboor.qms.admin.panel.service.BranchService;
import com.taboor.qms.admin.panel.service.EmpoyeeBranchService;
import com.taboor.qms.admin.panel.service.QueueService;
import com.taboor.qms.admin.panel.service.ServiceCenterEmpService;
import com.taboor.qms.admin.panel.service.ServiceCenterService;
import com.taboor.qms.admin.panel.service.ServiceTABService;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchServiceTABMapper;
import com.taboor.qms.core.model.BranchWorkingHours;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.ServiceCenterEmpBranchMapper;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAgentListResponse.AgentObject;
import com.taboor.qms.core.response.GetBranchCounterListResponse.BranchCounterObject;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetBranchListingResponse;
import com.taboor.qms.core.response.GetBranchListingResponse.BranchListingObject;
import com.taboor.qms.core.response.GetServiceListResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.BranchStatus;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;

@Service
public class BranchServiceImpl implements BranchService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BranchServiceImpl.class);

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Value("${url.microservice.user.management}")
	private String userManagementMicroserviceURL;

	private RestTemplate restTemplate = new RestTemplate();

	@Autowired
	ServiceCenterService serviceCenterService;

	@Autowired
	ServiceCenterEmpService serviceCenterEmpService;

	@Autowired
	ServiceTABService serviceTABService;

	@Autowired
	EmpoyeeBranchService employeeBranchService;

	@Autowired
	QueueService queueService;

	@Autowired
	BranchCounterService branchCounterService;

	@Autowired
	AgentService agentService;

	@Override
	public Branch getBranch(@Valid Long branchId) throws TaboorQMSServiceException, Exception {
		restTemplate = new RestTemplate();
		String uri = dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId=" + branchId;

		ResponseEntity<Branch> getBranchByIdResponse = restTemplate.getForEntity(uri, Branch.class);

		if (!getBranchByIdResponse.getStatusCode().equals(HttpStatus.OK) || getBranchByIdResponse.getBody() == null)
//		Optional<Branch> branch = branchRepository.findById(branchId);
//		if (!branch.isPresent())
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());
		return getBranchByIdResponse.getBody();
	}

	@Override
	public GetServiceListResponse getProvidedServices(Long branchId) throws TaboorQMSServiceException, Exception {

		Branch branch = getBranch(branchId);

		String uri = dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branch?branchId="
				+ branch.getBranchId();
		ResponseEntity<?> getServicesResponse = restTemplate.getForEntity(uri, List.class);
		List<ServiceTAB> serviceList = GenericMapper.convertListToObject(getServicesResponse.getBody(),
				new TypeReference<List<ServiceTAB>>() {
				});

		return new GetServiceListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.SERVICE_FETCHED.getMessage(), serviceList);
	}

	@Override
	public GetValuesResposne addBranch(AddBranchPayload addBranchPayload) throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
				.findByUserId(session.getUser().getUserId());

		Boolean limitAcheived = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/checkBranchLimit?serviceCenterId="
						+ serviceCenterEmployee.getServiceCenter().getServiceCenterId(),
				Boolean.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		if (limitAcheived)
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_LIMIT_EXCEEDS.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCH_LIMIT_EXCEEDS.getErrorDetails());

		Branch branch = new Branch();
		branch.setBranchName(addBranchPayload.getBranchName());
		branch.setBranchNameArabic(addBranchPayload.getBranchNameArabic());
		branch.setCity(addBranchPayload.getCity());
		branch.setAddress(addBranchPayload.getAddress());
		branch.setLatitude(addBranchPayload.getLatitude().doubleValue());
		branch.setLongitude(addBranchPayload.getLongitude().doubleValue());
		branch.setAverageServiceTime(LocalTime.of(0, 0));
		branch.setAverageServiceTimeCount(0);
		branch.setAverageTicketPerDay(0);
		branch.setAverageWaitingTime(LocalTime.of(0, 0));
		branch.setAverageWaitingTimeCount(0);
		branch.setCreatedTime(OffsetDateTime.now().withNano(0));
		branch.setEmailAddress(addBranchPayload.getEmail());
		branch.setPhoneNumber(addBranchPayload.getPhoneNumber());
		branch.setRating(0.0);
		branch.setRatingCount(0);
		branch.setWorkingStatus(true);
		branch.setBranchStatus(BranchStatus.OPEN.getStatus());
		branch.setServiceCenter(serviceCenterEmployee.getServiceCenter());

		branch = saveBranch(branch);

		List<BranchServiceTABMapper> branchServiceTABMappers = new ArrayList<BranchServiceTABMapper>();
		List<Queue> queueList = new ArrayList<Queue>();
		for (ServiceTAB service : addBranchPayload.getServiceList()) {
			BranchServiceTABMapper branchServiceTABMapper = new BranchServiceTABMapper();
			branchServiceTABMapper.setBranch(branch);
			branchServiceTABMapper.setServiceTAB(service);
			branchServiceTABMappers.add(branchServiceTABMapper);
			Queue queue = new Queue();
			queue.setQueueNumber(branch.getBranchName().substring(0, 1).toUpperCase() + branch.getBranchId()
					+ service.getServiceId());
			queue.setAverageServiceTime(LocalTime.of(0, 0));
			queue.setAverageServiceTimeCount(0);
			queue.setAverageWaitTime(LocalTime.of(0, 0));
			queue.setAverageWaitTimeCount(0);
			queue.setBranch(branch);
			queue.setService(service);
			queueList.add(queue);
		}
		branchServiceTABMappers = saveBranchServiceTABMappers(branchServiceTABMappers);
		queueList = queueService.saveQueueList(queueList);

		List<BranchWorkingHours> branchWorkingHoursList = new ArrayList<BranchWorkingHours>();
		for (WorkingHours workingHours : addBranchPayload.getWorkingHoursList()) {
			BranchWorkingHours branchWorkingHours = new BranchWorkingHours();
			branchWorkingHours.setBranch(branch);
			branchWorkingHours.setDay(workingHours.getDay());
			branchWorkingHours.setOpeningTime(workingHours.getOpeningTime());
			branchWorkingHours.setClosingTime(workingHours.getClosingTime());
			branchWorkingHoursList.add(branchWorkingHours);
		}
		branchWorkingHoursList = saveBranchWorkingHours(branchWorkingHoursList);

		// Relate branch with all ServiceCenterAdmin
		List<ServiceCenterEmployee> serviceCenterEmployeeList = serviceCenterEmpService
				.findAdminByServiceCenter(serviceCenterEmployee.getServiceCenter());
		if (!serviceCenterEmployee.getUser().getRoleName().equals(RoleName.Service_Center_Admin.name()))
			serviceCenterEmployeeList.add(serviceCenterEmployee);

		List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMappers = new ArrayList<ServiceCenterEmpBranchMapper>();
		for (ServiceCenterEmployee entity : serviceCenterEmployeeList) {
			ServiceCenterEmpBranchMapper serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
			serviceCenterEmpBranchMapper.setBranch(branch);
			serviceCenterEmpBranchMapper.setServiceCenterEmployee(entity);
			serviceCenterEmpBranchMappers.add(serviceCenterEmpBranchMapper);
		}

		serviceCenterEmpBranchMappers = employeeBranchService
				.saveEmployeeBranchMapperList(serviceCenterEmpBranchMappers, false);

		RSocketPayload payload = new RSocketPayload();
		payload.setNotificationSubject(RSocketNotificationSubject.NEW_BRANCH);
		payload.setRecipientId(branch.getServiceCenter().getServiceCenterId());
		payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
		payload.setInformation(Arrays.asList(branch.getBranchName()));

//		RSocketInitializer.fireAndForget(payload);
		RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
				new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

		Map<String, Object> values = new HashMap<String, Object>();
		values.put("BranchId", String.valueOf(branch.getBranchId()));
		values.put("BranchName", branch.getBranchName());
		values.put("BranchNameArabic", branch.getBranchNameArabic());
		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.BRANCH_ADDED.getMessage(),
				values);
	}

	@Override
	public Branch saveBranch(Branch branch) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/branches/save";
		HttpEntity<Branch> branchEntity = new HttpEntity<>(branch, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<Branch> createBranchResponse = restTemplate.postForEntity(uri, branchEntity, Branch.class);

		if (!createBranchResponse.getStatusCode().equals(HttpStatus.OK) || createBranchResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCH_NOT_CREATED.getErrorDetails());

		return createBranchResponse.getBody();
	}

	private List<BranchWorkingHours> saveBranchWorkingHours(List<BranchWorkingHours> branchWorkingHoursList)
			throws JsonGenerationException, JsonMappingException, IOException, TaboorQMSServiceException {
		String uri = dbConnectorMicroserviceURL + "/tab/branchworkinghours/saveAll";
		HttpEntity<?> branchWorkingHoursEntity = new HttpEntity<>(branchWorkingHoursList,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> createBranchWorkingHoursResponse = restTemplate.postForEntity(uri, branchWorkingHoursEntity,
				List.class);

		if (!createBranchWorkingHoursResponse.getStatusCode().equals(HttpStatus.OK)
				|| createBranchWorkingHoursResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCHWORKINGHOURS_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCHWORKINGHOURS_NOT_CREATED.getErrorDetails());

		branchWorkingHoursList = GenericMapper.convertListToObject(createBranchWorkingHoursResponse.getBody(),
				new TypeReference<List<BranchWorkingHours>>() {
				});

		return branchWorkingHoursList;
	}

	private List<BranchServiceTABMapper> saveBranchServiceTABMappers(
			List<BranchServiceTABMapper> branchServiceTABMappers)
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException {
		String uri = dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/saveAll";
		HttpEntity<?> branchServiceTABMappersEntity = new HttpEntity<>(branchServiceTABMappers,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> createBranchServiceTABMappersResponse = restTemplate.postForEntity(uri,
				branchServiceTABMappersEntity, List.class);

		if (!createBranchServiceTABMappersResponse.getStatusCode().equals(HttpStatus.OK)
				|| createBranchServiceTABMappersResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCHSERVICETABMAPPER_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCHSERVICETABMAPPER_NOT_CREATED.getErrorDetails());

		branchServiceTABMappers = GenericMapper.convertListToObject(createBranchServiceTABMappersResponse.getBody(),
				new TypeReference<List<BranchServiceTABMapper>>() {
				});

		return branchServiceTABMappers;
	}

	@Override
	public GetBranchListingResponse getBranchListing() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
				.findByUserId(session.getUser().getUserId());
		List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

		if (branchList.isEmpty())
			return new GetBranchListingResponse(xyzResponseCode.SUCCESS.getCode(),
					xyzResponseMessage.BRANCH_FETCHED.getMessage(), null);

		String uri = dbConnectorMicroserviceURL + "/tab/branches/getBranchListing/branchList";
		HttpEntity<?> branchListEntity = new HttpEntity<>(branchList, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> getBranchListingResponse = restTemplate.postForEntity(uri, branchListEntity, List.class);

		List<BranchListingObject> branchListing = GenericMapper.convertListToObject(getBranchListingResponse.getBody(),
				new TypeReference<List<BranchListingObject>>() {
				});
//                List<BranchListingObject> updatedBranchListing = new ArrayList<>();
//                Added by awais waheed
//                for(BranchListingObject entity: branchListing){
//                    Branch branch = entity.getBranch();
//                    branch.setServiceCenter(null);
//                    
//                    String uri2 = dbConnectorMicroserviceURL + "/tab/branchworkinghours/get/branch?branchId=" + branch.getBranchId();
//                    ResponseEntity<?> getBranchWorkingHoursResponse = restTemplate.getForEntity(uri2, List.class);
//
//                    List<BranchWorkingHours> branchWorkingHoursList = GenericMapper.convertListToObject(
//                                    getBranchWorkingHoursResponse.getBody(), new TypeReference<List<BranchWorkingHours>>() {
//                                    });
//                    
//                    branch.setBranchStatus(BranchStatus.CLOSED.getStatus());
//                    OffsetDateTime temp = OffsetDateTime.now().withNano(0);
//                    for (BranchWorkingHours item : branchWorkingHoursList)
//                            if (item.getDay() == temp.getDayOfWeek().getValue())
//                                    if (LocalTime.from(temp).isBefore(item.getClosingTime())
//                                                    & LocalTime.from(temp).isAfter(item.getOpeningTime()))
//                                            branch.setBranchStatus(BranchStatus.OPEN.getStatus());
//                    
//                    entity.setBranch(branch);
//                    updatedBranchListing.add(entity);
//                }
		
		return new GetBranchListingResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_FETCHED.getMessage(), branchListing);
	}

	@Override
	public GetBranchListingResponse getBranchDetailsByBranchId(long branchId)
			throws TaboorQMSServiceException, Exception {
		List<Branch> branchList = Arrays.asList(getBranch(branchId));

		String uri = dbConnectorMicroserviceURL + "/tab/branches/getBranchListing/branchListing";
		HttpEntity<?> branchListEntity = new HttpEntity<>(branchList, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> getBranchListingResponse = restTemplate.postForEntity(uri, branchListEntity, List.class);

		List<BranchListingObject> branchListing = GenericMapper.convertListToObject(getBranchListingResponse.getBody(),
				new TypeReference<List<BranchListingObject>>() {
				});
		branchListing.forEach(entity -> entity.getBranch().setServiceCenter(null));
		return new GetBranchListingResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_FETCHED.getMessage(), branchListing);
	}

	@Override
	public GetBranchCompleteDetailsResponse getBranchCompleteDetails(long branchId)
			throws TaboorQMSServiceException, Exception {

		Branch branch = getBranch(branchId);

		String uri = dbConnectorMicroserviceURL + "/tab/branchworkinghours/get/branch?branchId=" + branchId;
		ResponseEntity<?> getBranchWorkingHoursResponse = restTemplate.getForEntity(uri, List.class);

		List<BranchWorkingHours> branchWorkingHoursList = GenericMapper.convertListToObject(
				getBranchWorkingHoursResponse.getBody(), new TypeReference<List<BranchWorkingHours>>() {
				});

		uri = dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branch?branchId=" + branchId;
		ResponseEntity<?> getBranchServicesResponse = restTemplate.getForEntity(uri, List.class);

		List<ServiceTAB> servicesList = GenericMapper.convertListToObject(getBranchServicesResponse.getBody(),
				new TypeReference<List<ServiceTAB>>() {
				});

		List<BranchServicesCompleteObj> branchServicesCompleteObjs = new ArrayList<BranchServicesCompleteObj>();
		for (int i = 0; i < servicesList.size(); i++) {

			BranchServicesCompleteObj branchServicesCompleteObj = new BranchServicesCompleteObj();
			uri = dbConnectorMicroserviceURL
					+ "/tab/servicecenterservicetabmapper/getRequirements/serviceCenterIdAndServiceId?serviceCenterId="
					+ branch.getServiceCenter().getServiceCenterId() + "&serviceId="
					+ servicesList.get(i).getServiceId();
			ResponseEntity<?> getServiceRequiredDocumentsResponse = restTemplate.getForEntity(uri, List.class);

			List<ServiceRequirement> serviceDocumentsList = GenericMapper.convertListToObject(
					getServiceRequiredDocumentsResponse.getBody(), new TypeReference<List<ServiceRequirement>>() {
					});

			branchServicesCompleteObj.setServiceTAB(servicesList.get(i));
			branchServicesCompleteObj.setServiceTABRequiredDocumentMapperList(serviceDocumentsList);
			branchServicesCompleteObjs.add(branchServicesCompleteObj);

		}
		List<BranchCounterObject> branchCounterList = branchCounterService.getByBranch(branchId).getBranchCounterList();
		List<AgentObject> agentList = agentService.getByBranch(branch).getAgentList();

		uri = dbConnectorMicroserviceURL + "/tab/tickets/count/branch";
		HttpEntity<Branch> entity = new HttpEntity<>(branch, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<Integer> getBranchTicketCountResponse = restTemplate.postForEntity(uri, entity, Integer.class);

//		uri = dbConnectorMicroserviceURL + "/tab/tickets/countDayAverage/branch";
//		entity = new HttpEntity<>(branch, HttpHeadersUtils.getApplicationJsonHeader());
//		ResponseEntity<Long> getBranchAverageTicketCountResponse = restTemplate.postForEntity(uri, entity, Long.class);
//
//		if (getBranchAverageTicketCountResponse.getBody() == null)
//			branch.setAverageTicketPerDay(0);
//		else
//			branch.setAverageTicketPerDay(getBranchAverageTicketCountResponse.getBody().intValue());
		branch.setServiceCenter(null);

		branch.setBranchStatus(BranchStatus.CLOSED.getStatus());
		OffsetDateTime temp = OffsetDateTime.now().withNano(0);
		for (BranchWorkingHours item : branchWorkingHoursList)
			if (item.getDay() == temp.getDayOfWeek().getValue())
				if (LocalTime.from(temp).isBefore(item.getClosingTime())
						& LocalTime.from(temp).isAfter(item.getOpeningTime()))
					branch.setBranchStatus(BranchStatus.OPEN.getStatus());

		return new GetBranchCompleteDetailsResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_FETCHED.getMessage(), branch, getBranchTicketCountResponse.getBody(),
				branchServicesCompleteObjs, branchWorkingHoursList, agentList, branchCounterList);
	}

	@Override
	public GenStatusResponse deleteBranch(long branchId) throws TaboorQMSServiceException, Exception {
		Branch branch = getBranch(branchId);

		String uri = dbConnectorMicroserviceURL + "/tab/branches/delete?branchId=" + branch.getBranchId();
		ResponseEntity<Boolean> deleteBranchResponse = restTemplate.getForEntity(uri, Boolean.class);

		if (!deleteBranchResponse.getStatusCode().equals(HttpStatus.OK) || deleteBranchResponse.getBody().equals(false))
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCH_NOT_DELETED.getErrorDetails());
		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.BRANCH_DELETED.getMessage());
	}

	@Override
	public GenStatusResponse changeBranchStatus(long branchId) throws TaboorQMSServiceException, Exception {
		Branch branch = getBranch(branchId);
		if (branch.getWorkingStatus())
			branch.setWorkingStatus(false);
		else
			branch.setWorkingStatus(true);
		branch = saveBranch(branch);
		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_STATUS_CHANGED.getMessage());
	}

	@Override
	public GetValuesResposne cloneBranch(CloneBranchPayload cloneBranchPayload)
			throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
				.findByUserId(session.getUser().getUserId());

		Boolean limitAcheived = RestUtil.getRequest(
				dbConnectorMicroserviceURL + "/tab/servicecentersubscriptions/checkBranchLimit?serviceCenterId="
						+ serviceCenterEmployee.getServiceCenter().getServiceCenterId(),
				Boolean.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
				xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		if (limitAcheived)
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_LIMIT_EXCEEDS.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCH_LIMIT_EXCEEDS.getErrorDetails());

		Branch sampleBranch = getBranch(cloneBranchPayload.getSampleBranchId());

		Branch branch = new Branch();

		branch.setBranchId(null);
		branch.setEmailAddress(cloneBranchPayload.getEmail());
		branch.setPhoneNumber(cloneBranchPayload.getPhoneNumber());
		branch.setBranchName(cloneBranchPayload.getBranchName());
		branch.setBranchNameArabic(cloneBranchPayload.getBranchNameArabic());
		branch.setCity(cloneBranchPayload.getCity());
		branch.setAddress(cloneBranchPayload.getAddress());
		branch.setLongitude(cloneBranchPayload.getLongitude());
		branch.setLatitude(cloneBranchPayload.getLatitude());
		branch.setAverageServiceTime(LocalTime.of(0, 0));
		branch.setAverageServiceTimeCount(0);
		branch.setAverageTicketPerDay(0);
		branch.setAverageWaitingTime(LocalTime.of(0, 0));
		branch.setAverageWaitingTimeCount(0);
		branch.setCreatedTime(OffsetDateTime.now().withNano(0));
		branch.setRating(0.0);
		branch.setRatingCount(0);
		branch.setBranchStatus(sampleBranch.getBranchStatus());
		branch.setWorkingStatus(sampleBranch.getWorkingStatus());
		branch.setBranchStatus(BranchStatus.OPEN.getStatus());
		branch.setServiceCenter(sampleBranch.getServiceCenter());

		branch = saveBranch(branch);

		String uri = dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branch?branchId="
				+ sampleBranch.getBranchId();
		ResponseEntity<?> getServicesResponse = restTemplate.getForEntity(uri, List.class);
		List<ServiceTAB> serviceList = GenericMapper.convertListToObject(getServicesResponse.getBody(),
				new TypeReference<List<ServiceTAB>>() {
				});

		List<BranchServiceTABMapper> branchServiceTABMappers = new ArrayList<BranchServiceTABMapper>();
		List<Queue> queueList = new ArrayList<Queue>();
		for (ServiceTAB service : serviceList) {
			BranchServiceTABMapper branchServiceTABMapper = new BranchServiceTABMapper();
			branchServiceTABMapper.setBranch(branch);
			branchServiceTABMapper.setServiceTAB(service);
			branchServiceTABMappers.add(branchServiceTABMapper);
			Queue queue = new Queue();
			queue.setQueueNumber(branch.getBranchName().substring(0, 1).toUpperCase() + branch.getBranchId()
					+ service.getServiceId());
			queue.setAverageServiceTime(LocalTime.of(0, 0));
			queue.setAverageServiceTimeCount(0);
			queue.setAverageWaitTime(LocalTime.of(0, 0));
			queue.setAverageWaitTimeCount(0);
			queue.setBranch(branch);
			queue.setService(service);
			queueList.add(queue);
		}
		branchServiceTABMappers = saveBranchServiceTABMappers(branchServiceTABMappers);
		queueList = queueService.saveQueueList(queueList);

		uri = dbConnectorMicroserviceURL + "/tab/branchworkinghours/get/branch?branchId=" + sampleBranch.getBranchId();
		ResponseEntity<?> getBranchWorkingHoursResponse = restTemplate.getForEntity(uri, List.class);

		List<BranchWorkingHours> branchWorkingHoursList = GenericMapper.convertListToObject(
				getBranchWorkingHoursResponse.getBody(), new TypeReference<List<BranchWorkingHours>>() {
				});

		for (BranchWorkingHours branchWorkingHours : branchWorkingHoursList) {
			branchWorkingHours.setWorkingHourId(null);
			branchWorkingHours.setBranch(branch);
		}
		branchWorkingHoursList = saveBranchWorkingHours(branchWorkingHoursList);

		uri = dbConnectorMicroserviceURL + "/tab/branchcounters/get/branch?branchId=" + sampleBranch.getBranchId();
		ResponseEntity<?> getBranchCountersResponse = restTemplate.getForEntity(uri, List.class);

		if (!getBranchCountersResponse.getStatusCode().equals(HttpStatus.OK))
			throw new TaboorQMSServiceException(xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode() + "::"
					+ xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

		List<BranchCounterObject> branchCounterObjectList = GenericMapper.convertListToObject(
				getBranchCountersResponse.getBody(), new TypeReference<List<BranchCounterObject>>() {
				});

		for (BranchCounterObject branchCounterObject : branchCounterObjectList)
			branchCounterService.addOrUpdateCounter(new AddBranchCounterPayload(branch.getBranchId(),
					branchCounterObject.getProvidedServices(), false));

		// Relate branch with all ServiceCenterAdmin
		List<ServiceCenterEmployee> serviceCenterEmployeeList = serviceCenterEmpService
				.findAdminByServiceCenter(serviceCenterEmployee.getServiceCenter());
		if (!serviceCenterEmployee.getUser().getRoleName().equals(RoleName.Service_Center_Admin.name()))
			serviceCenterEmployeeList.add(serviceCenterEmployee);

		List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMappers = new ArrayList<ServiceCenterEmpBranchMapper>();
		for (ServiceCenterEmployee entity : serviceCenterEmployeeList) {
			ServiceCenterEmpBranchMapper serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
			serviceCenterEmpBranchMapper.setBranch(branch);
			serviceCenterEmpBranchMapper.setServiceCenterEmployee(entity);
			serviceCenterEmpBranchMappers.add(serviceCenterEmpBranchMapper);
		}

		serviceCenterEmpBranchMappers = employeeBranchService
				.saveEmployeeBranchMapperList(serviceCenterEmpBranchMappers, false);

		RSocketPayload payload = new RSocketPayload();
		payload.setNotificationSubject(RSocketNotificationSubject.NEW_BRANCH);
		payload.setRecipientId(branch.getServiceCenter().getServiceCenterId());
		payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
		payload.setInformation(Arrays.asList(branch.getBranchName()));

//		RSocketInitializer.fireAndForget(payload);
		RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
				new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

		Map<String, Object> values = new HashMap<String, Object>();
		values.put("BranchId", String.valueOf(branch.getBranchId()));
		values.put("BranchName", branch.getBranchName());
		values.put("BranchNameArabic", branch.getBranchNameArabic());
		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.BRANCH_CLONEED.getMessage(),
				values);
	}

	@Override
	public GetBranchListResponse getBranchList() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
				.findByUserId(session.getUser().getUserId());
		List<Branch> branchList = employeeBranchService.getBranchByEmp(serviceCenterEmployee.getEmployeeId());

		branchList.forEach(entity -> entity.setServiceCenter(null));

		return new GetBranchListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.BRANCH_FETCHED.getMessage(), branchList);
	}

	@Override
	public GenStatusResponse addService(AddBranchServicePayload payload) throws TaboorQMSServiceException, Exception {

		Branch branch = getBranch(payload.getBranchId());
		List<BranchServiceTABMapper> branchServiceTABMappers = new ArrayList<BranchServiceTABMapper>();

		List<Queue> queueList = new ArrayList<Queue>();

		for (ServiceTAB service : payload.getServiceList()) {
			BranchServiceTABMapper branchServiceTABMapper = new BranchServiceTABMapper();
			branchServiceTABMapper.setBranch(branch);
			branchServiceTABMapper.setServiceTAB(service);
			branchServiceTABMappers.add(branchServiceTABMapper);

			Queue queue = new Queue();
			queue.setQueueNumber(branch.getBranchName().substring(0, 1).toUpperCase() + branch.getBranchId()
					+ service.getServiceId());
			queue.setAverageServiceTime(LocalTime.of(0, 0));
			queue.setAverageWaitTime(LocalTime.of(0, 0));
			queue.setBranch(branch);
			queue.setService(service);
			queueList.add(queue);
		}

		String uri = dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/saveAll";
		HttpEntity<?> entity = new HttpEntity<>(branchServiceTABMappers, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<?> createBranchServiceTABMapperResponse = restTemplate.postForEntity(uri, entity, List.class);

		if (!createBranchServiceTABMapperResponse.getStatusCode().equals(HttpStatus.OK)
				|| createBranchServiceTABMapperResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.SERVICETAB_NOT_CREATED.getErrorDetails());

		queueService.saveQueueList(queueList);

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.SERVICE_ADDED.getMessage());
	}

	@Override
	public GenStatusResponse deleteService(GetByIdListPayload payload) throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/delete/branchAndService";
		HttpEntity<GetByIdListPayload> entity = new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<Boolean> deleteBranchServiceTABMapperResponse = restTemplate.postForEntity(uri, entity,
				Boolean.class);

		if (!deleteBranchServiceTABMapperResponse.getStatusCode().equals(HttpStatus.OK)
				|| deleteBranchServiceTABMapperResponse.getBody() == false)
			throw new TaboorQMSServiceException(xyzErrorMessage.SERVICETAB_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.SERVICETAB_NOT_DELETED.getErrorDetails());

		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.SERVICE_DELETED.getMessage());
	}

	@Override
	public GenStatusResponse updateWorkingHours(AddBranchPayload payload) throws TaboorQMSServiceException, Exception {

		Branch branch = getBranch(payload.getBranchId());

		String uri = dbConnectorMicroserviceURL + "/tab/branchworkinghours/delete/branch?branchId="
				+ branch.getBranchId();
		ResponseEntity<Boolean> deleteBranchWorkingHoursResponse = restTemplate.getForEntity(uri, Boolean.class);

		if (!deleteBranchWorkingHoursResponse.getStatusCode().equals(HttpStatus.OK)
				|| deleteBranchWorkingHoursResponse.getBody().equals(false))
			throw new TaboorQMSServiceException(xyzErrorMessage.BRANCHWORKINGHOURS_NOT_DELETED.getErrorCode() + "::"
					+ xyzErrorMessage.BRANCHWORKINGHOURS_NOT_DELETED.getErrorDetails());

		List<BranchWorkingHours> branchWorkingHoursList = new ArrayList<BranchWorkingHours>();
		for (WorkingHours workingHours : payload.getWorkingHoursList()) {
			BranchWorkingHours branchWorkingHours = new BranchWorkingHours();
			branchWorkingHours.setBranch(branch);
			branchWorkingHours.setDay(workingHours.getDay());
			branchWorkingHours.setOpeningTime(workingHours.getOpeningTime());
			branchWorkingHours.setClosingTime(workingHours.getClosingTime());
			branchWorkingHoursList.add(branchWorkingHours);
		}
		branchWorkingHoursList = saveBranchWorkingHours(branchWorkingHoursList);
		return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.BRANCH_UPDATED.getMessage());
	}

	@Override
	public GetValuesResposne updateBranch(AddBranchPayload addBranchPayload)
			throws TaboorQMSServiceException, Exception {
		if (addBranchPayload.getBranchId() != null)
			if (addBranchPayload.getBranchId() > 0) {
				Branch branch = getBranch(addBranchPayload.getBranchId());

				branch.setBranchName(addBranchPayload.getBranchName());
				branch.setBranchNameArabic(addBranchPayload.getBranchNameArabic());
				branch.setCity(addBranchPayload.getCity());
				branch.setAddress(addBranchPayload.getAddress());
				branch.setLatitude(addBranchPayload.getLatitude().doubleValue());
				branch.setLongitude(addBranchPayload.getLongitude().doubleValue());
				branch.setEmailAddress(addBranchPayload.getEmail());
				branch.setPhoneNumber(addBranchPayload.getPhoneNumber());

				branch = saveBranch(branch);

				Map<String, Object> values = new HashMap<String, Object>();
				values.put("BranchId", String.valueOf(branch.getBranchId()));
				values.put("BranchName", branch.getBranchName());
				values.put("BranchNameArabic", branch.getBranchNameArabic());

				RSocketPayload payload = new RSocketPayload();
				payload.setNotificationSubject(RSocketNotificationSubject.UPDATE_BRANCH);
				payload.setRecipientId(branch.getServiceCenter().getServiceCenterId());
				payload.setTypes(Arrays.asList(NotificationType.IN_APP, NotificationType.EMAIL));
				payload.setInformation(Arrays.asList(branch.getBranchName()));

//				RSocketInitializer.fireAndForget(payload);
				RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
						new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);

				return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(),
						xyzResponseMessage.BRANCH_UPDATED.getMessage(), values);
			}

		return new GetValuesResposne(xyzResponseCode.ERROR.getCode(),
				xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails(), null);
	}

	@Override
	public GetValuesResposne countBranch() throws TaboorQMSServiceException, Exception {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Session session = myUserDetails.getSession();
		ServiceCenterEmployee serviceCenterEmployee = serviceCenterEmpService
				.findByUserId(session.getUser().getUserId());

		Map<String, Object> values = new HashMap<String, Object>();
		values.put("BranchCount", employeeBranchService.countBranchByEmp(serviceCenterEmployee.getEmployeeId()));

		return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.BRANCH_FETCHED.getMessage(),
				values);
	}

}
