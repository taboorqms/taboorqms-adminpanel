package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterServiceTABMapper;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetServiceCenterProfileResponse extends GenStatusResponse {

	private ServiceCenter serviceCenter;
	private int NoOfBranches;
	private int NoOfCities;
	private int NoOfUsers;
	private int NoOfAgents;
	private List<ServiceCenterServiceTABMapper> servicesList;
	private List<AverageServiceTimeList> averageServiceTimeList;

	public static class AverageServiceTimeList {
		private ServiceTAB service;
		private double ast;

		public ServiceTAB getService() {
			return service;
		}

		public void setService(ServiceTAB service) {
			this.service = service;
		}

		public double getAst() {
			return ast;
		}

		public void setAst(double ast) {
			this.ast = ast;
		}
	}

	public ServiceCenter getServiceCenter() {
		return serviceCenter;
	}

	public void setServiceCenter(ServiceCenter serviceCenter) {
		this.serviceCenter = serviceCenter;
	}

	public int getNoOfBranches() {
		return NoOfBranches;
	}

	public void setNoOfBranches(int noOfBranches) {
		NoOfBranches = noOfBranches;
	}

	public int getNoOfCities() {
		return NoOfCities;
	}

	public void setNoOfCities(int noOfCities) {
		NoOfCities = noOfCities;
	}

	public int getNoOfUsers() {
		return NoOfUsers;
	}

	public void setNoOfUsers(int noOfUsers) {
		NoOfUsers = noOfUsers;
	}

	public int getNoOfAgents() {
		return NoOfAgents;
	}

	public void setNoOfAgents(int noOfAgents) {
		NoOfAgents = noOfAgents;
	}

	public List<ServiceCenterServiceTABMapper> getServicesList() {
		return servicesList;
	}

	public void setServicesList(List<ServiceCenterServiceTABMapper> servicesList) {
		this.servicesList = servicesList;
	}

	public List<AverageServiceTimeList> getAverageServiceTimeList() {
		return averageServiceTimeList;
	}

	public void setAverageServiceTimeList(List<AverageServiceTimeList> averageServiceTimeList) {
		this.averageServiceTimeList = averageServiceTimeList;
	}

	public GetServiceCenterProfileResponse() {
		// TODO Auto-generated constructor stub
	}

}
