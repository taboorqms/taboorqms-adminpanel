package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.SupportTicket;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetSupportTicketListResponse extends GenStatusResponse {

	private List<SupportTicket> supportTickets;

	public List<SupportTicket> getSupportTickets() {
		return supportTickets;
	}

	public void setSupportTickets(List<SupportTicket> supportTickets) {
		this.supportTickets = supportTickets;
	}

	public GetSupportTicketListResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetSupportTicketListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<SupportTicket> supportTickets) {
		super(applicationStatusCode, applicationStatusResponse);
		this.supportTickets = supportTickets;
	}

}
