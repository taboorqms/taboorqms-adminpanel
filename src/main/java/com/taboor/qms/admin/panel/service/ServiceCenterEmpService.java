package com.taboor.qms.admin.panel.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.taboor.qms.admin.panel.payload.RegisterServiceCenterEmpPayload;
import com.taboor.qms.admin.panel.response.GetEmployeeProfileResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetEmployeeListResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface ServiceCenterEmpService {

	ServiceCenterEmployee saveServiceCenterEmp(ServiceCenterEmployee serviceCenterEmployee)
			throws TaboorQMSServiceException;

	ServiceCenterEmployee findByUserId(Long userId) throws TaboorQMSServiceException;

	List<ServiceCenterEmployee> findAdminByServiceCenter(ServiceCenter serviceCenter)
			throws TaboorQMSServiceException, JsonGenerationException, JsonMappingException, IOException;

	GetEmployeeProfileResponse getProfile() throws TaboorQMSServiceException, Exception;

	GetEmployeeListResponse getList() throws TaboorQMSServiceException, Exception;

	GetValuesResposne registerEmp(RegisterServiceCenterEmpPayload registerServiceCenterEmpPayload,
			MultipartFile profileImage) throws TaboorQMSServiceException, Exception;

	GenStatusResponse updateEmp(RegisterServiceCenterEmpPayload registerServiceCenterEmpPayload)
			throws TaboorQMSServiceException, Exception;

	ServiceCenterEmployee createEmp(User user, ServiceCenter serviceCenter, List<Long> branchIds)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne getNextEmpNumber() throws TaboorQMSServiceException, Exception;

	String nextEmpNumber(ServiceCenter serviceCenter) throws RestClientException, TaboorQMSServiceException;
        
        GenStatusResponse deleteEmployee(String userId) throws TaboorQMSServiceException, Exception;

}
