package com.taboor.qms.admin.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.payload.AddBranchCounterPayload;
import com.taboor.qms.admin.panel.service.BranchCounterService;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchCounterListResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@RestController
@RequestMapping("/branchCounter")
@CrossOrigin(origins = "*")
public class BranchCounterController {

	private static final Logger logger = LoggerFactory.getLogger(BranchCounterController.class);

	@Autowired
	BranchCounterService branchCounterService;

	@PostMapping("/get/branch")
	public @ResponseBody GetBranchCounterListResponse getByBranch(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Branch Counters - API");
		// 0-BranchId
		return branchCounterService.getByBranch(getByIdPayload.getId());
	}

	@PostMapping("/addOrUpdate")
	public @ResponseBody GetValuesResposne addBranch(@RequestBody final AddBranchCounterPayload addBranchCounterPayload)
			throws Exception, Throwable {
		logger.info("Calling Add or Update Branch Counter - API");
		return branchCounterService.addOrUpdateCounter(addBranchCounterPayload);
	}

	@PostMapping("/getNextCounterNumber")
	public @ResponseBody GetValuesResposne getNextCounterNumber(@RequestBody final GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Next Branch Counter Number - API");
		// 0-BranchId
		return branchCounterService.getNextCounterNumber(getByIdPayload.getId());
	}

	@PostMapping("/delete")
	public @ResponseBody GenStatusResponse deleteCounter(@RequestBody final GetByIdListPayload getByIdListPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Branch Counter - API");
		return branchCounterService.deleteCounterList(getByIdListPayload.getIds());
	}

	@PostMapping("/status/change")
	public @ResponseBody GenStatusResponse changeCounterStatus(@RequestBody final GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Change Branch Counter Status - API");
		// 0-BranchCounterId
		return branchCounterService.changeCounterStatus(getByIdPayload.getId());
	}
}
