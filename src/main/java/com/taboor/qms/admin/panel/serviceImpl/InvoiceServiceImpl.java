package com.taboor.qms.admin.panel.serviceImpl;

import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.payload.AddInvoicePayload;
import com.taboor.qms.admin.panel.response.GetInvoiceListResponse;
import com.taboor.qms.admin.panel.service.InvoiceService;
import com.taboor.qms.admin.panel.service.TaboorBankAccountService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.TaboorBankAccount;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.payload.SendEmailPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.Currency;
import com.taboor.qms.core.utils.EmailTemplates;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.InvoiceStatus;
import com.taboor.qms.core.utils.InvoiceType;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.core.utils.RestUtil;
import java.util.ArrayList;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    TaboorBankAccountService taboorBankAccountService;

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;
    
    @Value("${url.microservice.email.service}")
    private String emailServiceMicroserviceURL;

    @Override
    public GetInvoiceListResponse getAll() throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/invoices/get/all";
        ResponseEntity<?> getInvoiceListResponse = restTemplate.getForEntity(uri, List.class);

        List<Invoice> invoiceList = GenericMapper.convertListToObject(getInvoiceListResponse.getBody(),
                new TypeReference<List<Invoice>>() {
        });

        for (Invoice item : invoiceList) {
            item.getServiceCenterSubscription().setBankCard(null);
            item.getServiceCenterSubscription().setPaymentPlan(null);
        }

        return new GetInvoiceListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.INVOICE_FETCHED.getMessage(), invoiceList);
    }

    @Override
    public GetInvoiceListResponse getById(long invoiceId) throws TaboorQMSServiceException {
        String uri = dbConnectorMicroserviceURL + "/tab/invoices/get/id?invoiceId=" + invoiceId;
        ResponseEntity<Invoice> getInvoiceResponse = restTemplate.getForEntity(uri, Invoice.class);

        if (!getInvoiceResponse.getStatusCode().equals(HttpStatus.OK) || getInvoiceResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.INVOICE_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.INVOICE_NOT_EXISTS.getErrorDetails());
        }

        return new GetInvoiceListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.INVOICE_FETCHED.getMessage(), Arrays.asList(getInvoiceResponse.getBody()));
    }

    @Override
    public GetInvoiceListResponse getByServiceCenterId(long serviceCenterId)
            throws TaboorQMSServiceException, Exception {
        String uri = dbConnectorMicroserviceURL + "/tab/invoices/get/serviceCenter?serviceCenterId=" + serviceCenterId;
        ResponseEntity<?> getInvoiceListResponse = restTemplate.getForEntity(uri, List.class);

        List<Invoice> invoiceList = GenericMapper.convertListToObject(getInvoiceListResponse.getBody(),
                new TypeReference<List<Invoice>>() {
        });

        return new GetInvoiceListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.INVOICE_FETCHED.getMessage(), invoiceList);
    }

    @Override
    public GetValuesResposne addOrUpdate(AddInvoicePayload addInvoicePayload)
            throws TaboorQMSServiceException, Exception {
        Invoice invoice = null;
        int emailType = 0;

        if (addInvoicePayload.getUpdateStatus() == 2 && addInvoicePayload.getInvoiceId() != null) {
            if (addInvoicePayload.getInvoiceId() > 0) {
                // Update Invoice Status
                invoice = getById(addInvoicePayload.getInvoiceId()).getInvoices().get(0);
                invoice.setUpdatedOn(OffsetDateTime.now().withNano(0));
                invoice.setInvoiceStatus(addInvoicePayload.getInvoiceStatus());
                invoice = saveInvoice(invoice);
            }
        }

        if (addInvoicePayload.getUpdateStatus() == 1 && addInvoicePayload.getInvoiceId() != null) {
            if (addInvoicePayload.getInvoiceId() > 0) {
                // Update Invoice
                emailType = 1;
                invoice = getById(addInvoicePayload.getInvoiceId()).getInvoices().get(0);
                invoice.setUpdatedOn(OffsetDateTime.now().withNano(0));
            }
        }

        if (addInvoicePayload.getUpdateStatus() == 0 && invoice == null) {
            // New Invoice
            emailType = 2;
            invoice = new Invoice();
            invoice.setInvoiceNumber(getNextInvoiceNumber());
            invoice.setCreatedOn(OffsetDateTime.now().withNano(0));
            invoice.setInvoiceType(InvoiceType.MANUAL);
        }

        if (addInvoicePayload.getUpdateStatus() == 0 || addInvoicePayload.getUpdateStatus() == 1) {
            if (addInvoicePayload.getDraft().booleanValue()) {
                invoice.setInvoiceStatus(InvoiceStatus.DRAFT);
            } else {
                invoice.setInvoiceStatus(InvoiceStatus.UNPAID);
            }
            ServiceCenterSubscription serviceCenterSubscription = new ServiceCenterSubscription();
            serviceCenterSubscription.setSubscriptionId(addInvoicePayload.getServiceCenterSubscriptionId());
            invoice.setServiceCenterSubscription(serviceCenterSubscription);
            invoice.setTaboorBankAccount(taboorBankAccountService.getDefaultAccount());
            invoice.setAmount(addInvoicePayload.getTotalAmount());
            invoice.setIssuedOn(OffsetDateTime.of(addInvoicePayload.getIssuedOn(), LocalTime.now(), ZoneOffset.UTC));
            invoice.setCurrency(addInvoicePayload.getCurrency());
            invoice.setDueDate(OffsetDateTime.of(addInvoicePayload.getDueOn(), LocalTime.now(), ZoneOffset.UTC));
            invoice.setExpiredOn(OffsetDateTime.of(addInvoicePayload.getExpiredOn(), LocalTime.now(), ZoneOffset.UTC));
            invoice.setInvoiceItemList(addInvoicePayload.getItems());
            invoice.setLanguage(addInvoicePayload.getLanguage());
            invoice.setLastNotifiedOn(null);
            invoice.setInvoiceItemList(addInvoicePayload.getItems());
            invoice.setNotes(addInvoicePayload.getNotes());

            invoice = saveInvoice(invoice);
        }
        if (addInvoicePayload.getDraft() == false & addInvoicePayload.getEmail() != null) {
            // Sending Email
            String body = EmailTemplates.EMAIL_TEMPLATE_SIMPLE;
            body = body.replaceAll("NAME", addInvoicePayload.getCenterName());
            String notificationData;
            switch (emailType) {
                case 2:
                    body = body.replaceAll("::BODY::",
                            "Invoice with No. '" + invoice.getInvoiceNumber() + "' is Generated and Amount is '"
                            + invoice.getAmount() + " " + invoice.getCurrency().name() + "' ");
                    notificationData = "Invoice with No. '" + invoice.getInvoiceNumber() + "' is Generated and Amount is '"
                            + invoice.getAmount() + " " + invoice.getCurrency().name() + "' ";
                    break;
                case 1:
                    body = body.replaceAll("::BODY::",
                            "Invoice with No. '" + invoice.getInvoiceNumber() + "' is Updated and Amount is '"
                            + invoice.getAmount() + " " + invoice.getCurrency().name() + "' ");
                    notificationData = "Invoice with No. '" + invoice.getInvoiceNumber() + "' is Updated and Amount is '"
                            + invoice.getAmount() + " " + invoice.getCurrency().name() + "' ";
                    break;
                default:
                    body = body.replaceAll("::BODY::",
                            "Invoice with No. '" + invoice.getInvoiceNumber() + "' is " + invoice.getInvoiceStatus().name()
                            + " now and Amount is '" + invoice.getAmount() + " " + invoice.getCurrency().name()
                            + "' ");
                    notificationData = "Invoice with No. '" + invoice.getInvoiceNumber() + "' is " + invoice.getInvoiceStatus().name()
                            + " now and Amount is '" + invoice.getAmount() + " " + invoice.getCurrency().name()
                            + "' ";
                    break;
            }

            //            sending notification
//            logger.info("sending notification");
//          Fetching user
            User user = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/users/get/email?email=" + addInvoicePayload.getEmail(),
                    User.class, xyzErrorMessage.USER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
//            Fetching session
//            logger.info(user.getEmail());
            Session userSession = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                    + user.getUserId(),
                    Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());
            RSocketPayload payload = new RSocketPayload();
            payload.setNotificationSubject(RSocketNotificationSubject.INVOICE_UPDATE);
            payload.setTypes(Arrays.asList(NotificationType.IN_APP));
            List<String> infoList = new ArrayList<>();
            infoList.add(0, String.valueOf(userSession.getSessionId()));
            infoList.add(1, notificationData);
            payload.setInformation(infoList);
            payload.setRecipientId(user.getUserId());
            RestUtil.postRequestNoCheck(dbConnectorMicroserviceURL + "/notification/handle",
                    new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), Boolean.class);
//            logger.info("notification sent");
//            Sending email
            SendEmailPayload sendEmailPayload = new SendEmailPayload();
            sendEmailPayload.setBody(body);
            sendEmailPayload.setEmail(addInvoicePayload.getEmail());
            sendEmailPayload.setSubject("TaboorQMS Invoice");   
            RestUtil.postRequest(emailServiceMicroserviceURL + "/email/send",
                    new HttpEntity<>(sendEmailPayload, HttpHeadersUtils.getApplicationJsonHeader()), GenStatusResponse.class,
                    xyzErrorMessage.EMAIL_NOT_SENT.getErrorCode(), xyzErrorMessage.EMAIL_NOT_SENT.getErrorDetails());
        }
        Map<String, Object> values = new HashMap<String, Object>();
        values.put("Saved Invoice Number", invoice.getInvoiceNumber());
        return new GetValuesResposne(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.INVOICE_ADDED.getMessage(),
                values);
    }

    private Invoice saveInvoice(Invoice invoice) throws TaboorQMSServiceException {
        invoice = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/invoices/save",
                new HttpEntity<>(invoice, HttpHeadersUtils.getApplicationJsonHeader()), Invoice.class,
                xyzErrorMessage.INVOICE_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.INVOICE_NOT_CREATED.getErrorDetails());

        return invoice;
    }

    @Override
    public String getNextInvoiceNumber() throws RestClientException, TaboorQMSServiceException {
        String nextInvoiceNumber = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/invoices/getNextNumber",
                String.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
        return nextInvoiceNumber;
    }

    @Override
    public void createAutoInvoice(Double amount, Currency currency, OffsetDateTime issuedOn, OffsetDateTime dueDate,
            OffsetDateTime expiredOn, ServiceCenterSubscription serviceCenterSubscription,
            TaboorBankAccount taboorBankAccount, InvoiceType invoiceType) throws TaboorQMSServiceException, Exception {

        Invoice invoice = new Invoice();
        Long temp = Math.round(amount);
        invoice.setAmount(temp.doubleValue());
        invoice.setCreatedOn(OffsetDateTime.now().withNano(0));
        invoice.setCurrency(currency);
        invoice.setIssuedOn(issuedOn);
        invoice.setDueDate(dueDate);
        invoice.setExpiredOn(expiredOn);
        invoice.setInvoiceNumber(getNextInvoiceNumber());
        invoice.setInvoiceStatus(InvoiceStatus.UNPAID);
        invoice.setInvoiceType(invoiceType);
        invoice.setLanguage(1);
        invoice.setLastNotifiedOn(null);
        invoice.setServiceCenterSubscription(serviceCenterSubscription);
        invoice.setUpdatedOn(OffsetDateTime.now().withNano(0));
        invoice.setTaboorBankAccount(taboorBankAccount);

        invoice = saveInvoice(invoice);
    }

    @Override
    public GenStatusResponse deleteInvoice(long invoiceId) throws TaboorQMSServiceException, Exception {

        Boolean response = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/invoices/delete/id?invoiceId=" + invoiceId, Boolean.class,
                xyzErrorMessage.INVOICE_NOT_DELETED.getErrorCode(),
                xyzErrorMessage.INVOICE_NOT_DELETED.getErrorDetails());

        if (!response.equals(true)) {
            throw new TaboorQMSServiceException(xyzErrorMessage.INVOICE_NOT_DELETED.getErrorCode() + "::"
                    + xyzErrorMessage.INVOICE_NOT_DELETED.getErrorDetails());
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.INVOICE_DELETED.getMessage());
    }

}
