package com.taboor.qms.admin.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.payload.AddAgentPayload;
import com.taboor.qms.admin.panel.response.GetUserPrivilegesListResponse;
import com.taboor.qms.admin.panel.service.AgentService;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAgentListResponse;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@RestController
@RequestMapping("/agent")
@CrossOrigin(origins = "*")
public class AgentController {

	private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

	@Autowired
	AgentService agentService;

	@GetMapping("/getNextCredentials")
	public @ResponseBody GetValuesResposne getNextAgentCredentials() throws Exception, Throwable {
		logger.info("Calling Get Next Agent Credentials - API");
		return agentService.getNextAgentCredentials();
	}

	@PostMapping("/add")
	public @ResponseBody GetValuesResposne addAgent(@RequestBody final AddAgentPayload addAgentPayload)
			throws Exception, Throwable {
		logger.info("Calling Add Agent - API");
		return agentService.addAgent(addAgentPayload);
	}

	@PostMapping("/update")
	public @ResponseBody GetValuesResposne updateAgent(@RequestBody final AddAgentPayload addAgentPayload)
			throws Exception, Throwable {
		logger.info("Calling Update Agent - API");
		return agentService.updateAgent(addAgentPayload);
	}

	@GetMapping("/getPrivileges")
	public @ResponseBody GetUserPrivilegesListResponse getAgentPrivileges() throws Exception, Throwable {
		logger.info("Calling Get Agent Privileges - API");
		return agentService.getAgentPrivileges();
	}

	@GetMapping("/get/serviceCenter")
	public @ResponseBody GetAgentListResponse getServiceCenterAgents() throws Exception, Throwable {
		logger.info("Calling Get Service Center Agents - API");
		return agentService.getByServiceCenter();
	}

	@GetMapping("/getListing")
	public @ResponseBody GetAgentListResponse getAgentListing() throws Exception, Throwable {
		logger.info("Calling Get Agents Listing - API");
		return agentService.getListing();
	}

	@GetMapping("/count")
	public @ResponseBody GetValuesResposne countAgent() throws Exception, Throwable {
		logger.info("Calling Get Agents Count - API");
		return agentService.countAgent();
	}

	@PostMapping("/getDetails")
	public @ResponseBody GetAgentListResponse getAgentDetails(@RequestBody GetByIdPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Agents Details - API");
		return agentService.getDetails(payload.getId());
	}

	@PostMapping("/getStatistics")
	public @ResponseBody GetStatisticsResponse getAgentStatistics(@RequestBody GetActivitiesPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Agents Statistics - API");
		return agentService.getStatistics(payload);
	}

	@PostMapping("/delete")
	public @ResponseBody GenStatusResponse deleteAgent(@RequestBody final GetByIdListPayload getByIdListPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Agent - API");
		return agentService.deleteAgentList(getByIdListPayload.getIds());
	}

}
