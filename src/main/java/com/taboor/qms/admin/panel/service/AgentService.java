package com.taboor.qms.admin.panel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.taboor.qms.admin.panel.payload.AddAgentPayload;
import com.taboor.qms.admin.panel.response.GetUserPrivilegesListResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAgentListResponse;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface AgentService {

	GetValuesResposne getNextAgentCredentials() throws TaboorQMSServiceException, Exception;

	GetValuesResposne addAgent(AddAgentPayload addAgentPayload) throws TaboorQMSServiceException, Exception;

	GetUserPrivilegesListResponse getAgentPrivileges() throws TaboorQMSServiceException, Exception;

	GetAgentListResponse getByServiceCenter() throws TaboorQMSServiceException, Exception;

	GetAgentListResponse getByBranch(Branch branch) throws TaboorQMSServiceException, Exception;

	GetValuesResposne updateAgent(AddAgentPayload addAgentPayload) throws TaboorQMSServiceException, Exception;

	Agent getById(Long agentId) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteAgentList(List<Long> agentIdList) throws TaboorQMSServiceException, Exception;

	GetAgentListResponse getListing() throws TaboorQMSServiceException, Exception;

	GetAgentListResponse getDetails(long agentId) throws TaboorQMSServiceException, Exception;

	GetStatisticsResponse getStatistics(GetActivitiesPayload payload) throws TaboorQMSServiceException, Exception;

	GetValuesResposne countAgent() throws TaboorQMSServiceException, Exception;

}
