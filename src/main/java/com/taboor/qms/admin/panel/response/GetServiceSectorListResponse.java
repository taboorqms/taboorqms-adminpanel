package com.taboor.qms.admin.panel.response;

import java.util.List;

import com.taboor.qms.core.model.ServiceSector;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetServiceSectorListResponse extends GenStatusResponse {
	List<ServiceSector> serviceSectors;

	public List<ServiceSector> getServiceSectors() {
		return serviceSectors;
	}

	public void setServiceSectors(List<ServiceSector> serviceSectors) {
		this.serviceSectors = serviceSectors;
	}

	public GetServiceSectorListResponse(int applicationStatusCode, String applicationStatusResponse,
			List<ServiceSector> serviceSectors) {
		super(applicationStatusCode, applicationStatusResponse);
		this.serviceSectors = serviceSectors;
	}

}
