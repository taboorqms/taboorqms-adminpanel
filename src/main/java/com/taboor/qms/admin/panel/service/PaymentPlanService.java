package com.taboor.qms.admin.panel.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.PaymentPlan;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetPaymentPlanListResponse;

@Service
public interface PaymentPlanService {

	GetPaymentPlanListResponse getAllPaymentPlans() throws TaboorQMSServiceException, Exception;

	GenStatusResponse addOrUpdatePaymentPlan(PaymentPlan paymentPlan2) throws TaboorQMSServiceException, Exception;

	GetPaymentPlanListResponse getListing() throws TaboorQMSServiceException, Exception;

	GetPaymentPlanListResponse getPlanDetails(Long paymentPlanId) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deletePaymentPlan(long paymentPlanId) throws TaboorQMSServiceException, Exception;

}
