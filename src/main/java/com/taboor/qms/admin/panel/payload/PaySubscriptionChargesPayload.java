package com.taboor.qms.admin.panel.payload;

public class PaySubscriptionChargesPayload {

	private Long serviceCenterSubscriptionId;
	private Long bankCardId;

	public Long getServiceCenterSubscriptionId() {
		return serviceCenterSubscriptionId;
	}

	public void setServiceCenterSubscriptionId(Long serviceCenterSubscriptionId) {
		this.serviceCenterSubscriptionId = serviceCenterSubscriptionId;
	}

	public Long getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(Long bankCardId) {
		this.bankCardId = bankCardId;
	}

}
