package com.taboor.qms.admin.panel.response;

import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetUserConfigurationsResponse extends GenStatusResponse {

	private UserConfiguration userConfiguration;

	public UserConfiguration getUserConfiguration() {
		return userConfiguration;
	}

	public void setUserConfiguration(UserConfiguration userConfiguration) {
		this.userConfiguration = userConfiguration;
	}

	public GetUserConfigurationsResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetUserConfigurationsResponse(int applicationStatusCode, String applicationStatusResponse,
			UserConfiguration userConfiguration) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userConfiguration = userConfiguration;
	}

}
