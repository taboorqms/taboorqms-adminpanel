package com.taboor.qms.admin.panel.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.admin.panel.payload.AddBranchPayload;
import com.taboor.qms.admin.panel.payload.AddBranchServicePayload;
import com.taboor.qms.admin.panel.payload.CloneBranchPayload;
import com.taboor.qms.admin.panel.response.GetBranchCompleteDetailsResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetBranchListingResponse;
import com.taboor.qms.core.response.GetServiceListResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface BranchService {

	GetServiceListResponse getProvidedServices(Long branchId) throws TaboorQMSServiceException, Exception;

	Branch getBranch(@Valid Long branchId) throws TaboorQMSServiceException, Exception;

	GetValuesResposne addBranch(AddBranchPayload addBranchPayload) throws TaboorQMSServiceException, Exception;

	Branch saveBranch(Branch branch) throws TaboorQMSServiceException, Exception;

	GetBranchListingResponse getBranchListing() throws TaboorQMSServiceException, Exception;

	GetBranchListingResponse getBranchDetailsByBranchId(long branchId) throws TaboorQMSServiceException, Exception;

	GetBranchCompleteDetailsResponse getBranchCompleteDetails(long branchId)
			throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteBranch(long branchId) throws TaboorQMSServiceException, Exception;

	GenStatusResponse changeBranchStatus(long branchId) throws TaboorQMSServiceException, Exception;

	GetValuesResposne cloneBranch(CloneBranchPayload cloneBranchPayload) throws TaboorQMSServiceException, Exception;

	GetBranchListResponse getBranchList() throws TaboorQMSServiceException, Exception;

	GenStatusResponse addService(AddBranchServicePayload payload) throws TaboorQMSServiceException, Exception;

	GenStatusResponse deleteService(GetByIdListPayload payload) throws TaboorQMSServiceException, Exception;

	GenStatusResponse updateWorkingHours(AddBranchPayload payload) throws TaboorQMSServiceException, Exception;

	GetValuesResposne updateBranch(AddBranchPayload addBranchPayload) throws TaboorQMSServiceException, Exception;

	GetValuesResposne countBranch() throws TaboorQMSServiceException, Exception;

}
