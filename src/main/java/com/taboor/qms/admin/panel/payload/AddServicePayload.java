package com.taboor.qms.admin.panel.payload;

import java.util.List;

import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;

public class AddServicePayload {

	private ServiceTAB service;
	private Boolean kiosikEnabled;
	private List<ServiceRequirement> requirementList;
	private String serviceCode;

	public ServiceTAB getService() {
		return service;
	}

	public void setService(ServiceTAB service) {
		this.service = service;
	}

	public List<ServiceRequirement> getRequirementList() {
		return requirementList;
	}

	public void setRequirementList(List<ServiceRequirement> requirementList) {
		this.requirementList = requirementList;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public Boolean getKiosikEnabled() {
		return kiosikEnabled;
	}

	public void setKiosikEnabled(Boolean kiosikEnabled) {
		this.kiosikEnabled = kiosikEnabled;
	}

}
