package com.taboor.qms.admin.panel.response;

import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetServiceCenterConfigurationResponse extends GenStatusResponse {

	private ServiceCenterConfiguration serviceCenterConfiguration;

	public ServiceCenterConfiguration getServiceCenterConfiguration() {
		return serviceCenterConfiguration;
	}

	public void setServiceCenterConfiguration(ServiceCenterConfiguration serviceCenterConfiguration) {
		this.serviceCenterConfiguration = serviceCenterConfiguration;
	}

	public GetServiceCenterConfigurationResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetServiceCenterConfigurationResponse(int applicationStatusCode, String applicationStatusResponse,
			ServiceCenterConfiguration serviceCenterConfiguration) {
		super(applicationStatusCode, applicationStatusResponse);
		this.serviceCenterConfiguration = serviceCenterConfiguration;
	}

}
