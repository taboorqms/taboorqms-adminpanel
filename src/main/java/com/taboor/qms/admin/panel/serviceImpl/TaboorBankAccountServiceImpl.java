package com.taboor.qms.admin.panel.serviceImpl;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.admin.panel.response.GetTaboorBankAccountListResponse;
import com.taboor.qms.admin.panel.service.TaboorBankAccountService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.TaboorBankAccount;
import com.taboor.qms.core.utils.GenericMapper;

@Service
public class TaboorBankAccountServiceImpl implements TaboorBankAccountService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public GetTaboorBankAccountListResponse getAll() throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/bankaccounts/get/all";
		ResponseEntity<?> getTaboorBankAccountListResponse = restTemplate.getForEntity(uri, List.class);

		List<TaboorBankAccount> bankAccountList = GenericMapper.convertListToObject(
				getTaboorBankAccountListResponse.getBody(), new TypeReference<List<TaboorBankAccount>>() {
				});
		return new GetTaboorBankAccountListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.TABOOR_BANK_ACCOUNT_FETCHED.getMessage(), bankAccountList);
	}

	@Override
	public TaboorBankAccount getDefaultAccount() throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/bankaccounts/getDefaultAccount";
		ResponseEntity<TaboorBankAccount> getTaboorBankAccountResponse = restTemplate.getForEntity(uri, TaboorBankAccount.class);
		return getTaboorBankAccountResponse.getBody();
	}

}
