package com.taboor.qms.admin.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.admin.panel.service.PaymentPlanService;
import com.taboor.qms.core.model.PaymentPlan;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetPaymentPlanListResponse;

@RestController
@RequestMapping("/paymentPlan")
@CrossOrigin(origins = "*")
public class PaymentPlanController {

	private static final Logger logger = LoggerFactory.getLogger(PaymentPlanController.class);

	@Autowired
	PaymentPlanService PaymentPlanService;

	@GetMapping("/get/all")
	public @ResponseBody GetPaymentPlanListResponse getAllPaymentPlans() throws Exception, Throwable {
		logger.info("Calling Get All Payment Plans - API");
		return PaymentPlanService.getAllPaymentPlans();
	}

	@GetMapping("/getListing")
	public @ResponseBody GetPaymentPlanListResponse getListing() throws Exception, Throwable {
		logger.info("Calling Get Payment Plans Listing - API");
		return PaymentPlanService.getListing();
	}

	@PostMapping("/getDetails")
	public @ResponseBody GetPaymentPlanListResponse getDetails(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Payment Plan Details - API");
		return PaymentPlanService.getPlanDetails(getByIdPayload.getId());
	}

	@PostMapping("/addOrUpdate")
	public @ResponseBody GenStatusResponse addPaymentPlan(@RequestBody PaymentPlan paymentPlan)
			throws Exception, Throwable {
		logger.info("Calling Add Payment Plans - API");
		return PaymentPlanService.addOrUpdatePaymentPlan(paymentPlan);
	}
	
	@PostMapping("/delete")
	public @ResponseBody GenStatusResponse deletePaymentPlan(@RequestBody GetByIdPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Delete Payment Plan - API");
		return PaymentPlanService.deletePaymentPlan(payload.getId());
	}

}
