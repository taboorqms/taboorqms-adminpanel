package com.taboor.qms.admin.panel.response;

import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetUserNotificationTypeResponse extends GenStatusResponse {

	private UserNotificationType userNotificationType;

	public UserNotificationType getUserNotificationType() {
		return userNotificationType;
	}

	public void setUserNotificationType(UserNotificationType userNotificationType) {
		this.userNotificationType = userNotificationType;
	}

	public GetUserNotificationTypeResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetUserNotificationTypeResponse(int applicationStatusCode, String applicationStatusResponse,
			UserNotificationType userNotificationType) {
		super(applicationStatusCode, applicationStatusResponse);
		this.userNotificationType = userNotificationType;
	}

}
