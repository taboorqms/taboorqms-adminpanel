package com.taboor.qms.admin.panel.serviceImpl;

import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.taboor.qms.admin.panel.service.RequiredDocumentService;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.utils.HttpHeadersUtils;

@Service
public class RequiredDocumentServiceImpl implements RequiredDocumentService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

	private RestTemplate restTemplate = new RestTemplate();

	@Value("${url.microservice.db.connector}")
	private String dbConnectorMicroserviceURL;

	@Override
	public ServiceRequirement getRequiredDocument(@Valid Long requiredDocumentId)
			throws TaboorQMSServiceException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceRequirement saveRequiredDocument(@Valid ServiceRequirement requiredDocument)
			throws TaboorQMSServiceException, Exception {
		String uri = dbConnectorMicroserviceURL + "/tab/servicerequirements/save";
		HttpEntity<ServiceRequirement> entity = new HttpEntity<>(requiredDocument,
				HttpHeadersUtils.getApplicationJsonHeader());
		ResponseEntity<ServiceRequirement> createRequiredDocumentResponse = restTemplate.postForEntity(uri, entity,
				ServiceRequirement.class);

		if (!createRequiredDocumentResponse.getStatusCode().equals(HttpStatus.OK)
				|| createRequiredDocumentResponse.getBody() == null)
			throw new TaboorQMSServiceException(xyzErrorMessage.REQUIREDDOCUMENT_NOT_CREATED.getErrorCode() + "::"
					+ xyzErrorMessage.REQUIREDDOCUMENT_NOT_CREATED.getErrorDetails());
		return createRequiredDocumentResponse.getBody();
	}

}
