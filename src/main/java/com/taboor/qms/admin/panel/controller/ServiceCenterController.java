package com.taboor.qms.admin.panel.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.admin.panel.payload.AddPaymentMethodPayload;
import com.taboor.qms.admin.panel.payload.AddServicePayload;
import com.taboor.qms.admin.panel.payload.AddSupportTicketPayload;
import com.taboor.qms.admin.panel.payload.PaySubscriptionChargesPayload;
import com.taboor.qms.admin.panel.payload.RegisterServiceCenterPayload;
import com.taboor.qms.admin.panel.payload.UpdatePaymentMethodPayload;
import com.taboor.qms.admin.panel.payload.UpdateServiceCenterPayload;
import com.taboor.qms.admin.panel.payload.UpdateSubscriptionPlanPayload;
import com.taboor.qms.admin.panel.payload.UpdateSupportTicketStatusPayload;
import com.taboor.qms.admin.panel.response.GetServiceCenterConfigurationResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterProfileResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterServiceListResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterSubscriptionListResponse;
import com.taboor.qms.admin.panel.response.GetServiceCenterSubscriptionResponse;
import com.taboor.qms.admin.panel.response.GetSupportTicketListResponse;
import com.taboor.qms.admin.panel.service.ServiceCenterService;
import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.payload.AddUserBankCardPayload;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetBranchListResponse;
import com.taboor.qms.core.response.GetPaymentPlanListResponse;
import com.taboor.qms.core.response.GetServiceCentreListResponse;
import com.taboor.qms.core.response.GetServiceCentreListingResponse;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.core.response.GetValuesResposne;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.core.utils.GenericMapper;

@RestController
@RequestMapping("/serviceCenter")
@CrossOrigin(origins = "*")
public class ServiceCenterController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceCenterController.class);

	@Autowired
	ServiceCenterService serviceCenterService;

	@Autowired
	FileDataService fileDataService;

	@PostMapping(value = "/register", consumes = { "multipart/form-data" }, produces = "application/json")
	public @ResponseBody GetValuesResposne registerServiceCenter(
			@RequestParam(value = "RegisterServiceCenterPayload", required = true) @Valid final String payload,
			@RequestParam(value = "ServiceCenterProfileImage", required = false) @Valid final MultipartFile profileImage)
			throws Exception, Throwable {
		logger.info("Calling Register Service Center - API");
		RegisterServiceCenterPayload registerServiceCenterPayload = GenericMapper.jsonToObjectMapper(payload,
				RegisterServiceCenterPayload.class);
		return serviceCenterService.registerServiceCenter(registerServiceCenterPayload, profileImage);
	}

	@PostMapping("/getPaymentPlan")
	public @ResponseBody GetPaymentPlanListResponse getSelectedPaymentPlan(@RequestBody GetByIdPayload payload)
			throws Exception, Throwable {
		// 0-ServiceCenterId
		logger.info("Calling Get Selected Payment Plan - API");
		return serviceCenterService.getSelectedPaymentPlan(payload.getId());
	}

	@GetMapping("/getServices")
	public @ResponseBody GetServiceCenterServiceListResponse getServices() throws Exception, Throwable {
		logger.info("Calling Get Service Center Provided Services - API");
		return serviceCenterService.getProvidedServices();
	}

	@CrossOrigin
	@PostMapping(value = "/getProfile")
	public @ResponseBody GetServiceCenterProfileResponse getProfile(
			@RequestBody GetByIdPayload payload) throws Exception, Throwable {
		logger.info("Calling Get Service Center Profile - API");
		return serviceCenterService.getProfile(payload);
	}

	@CrossOrigin
	@PostMapping("/delete")
	public @ResponseBody GenStatusResponse deleteServiceCentre(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Service Centre - API");
		// 0 - serviceCentreId
		return serviceCenterService.deleteServiceCentre(getByIdPayload.getId());
	}

	@CrossOrigin
	@PostMapping("/edit")
	public @ResponseBody GenStatusResponse editServiceCenter(@RequestBody UpdateServiceCenterPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Edit Service Center - API");
		// 0 - serviceCentreId
		return serviceCenterService.updateServiceCentre(payload);
	}

	@CrossOrigin
	@PostMapping("/service/add")
	public @ResponseBody GenStatusResponse addService(@RequestBody final AddServicePayload addServicePayload)
			throws Exception, Throwable {
		logger.info("Calling Add Service - API");
		return serviceCenterService.addService(addServicePayload);
	}

	@CrossOrigin
	@PostMapping("/service/delete")
	public @ResponseBody GenStatusResponse deleteService(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Service of Service  Center - API");
		return serviceCenterService.deleteService(getByIdPayload.getId());
	}

	@PostMapping("/config/get")
	public @ResponseBody GetServiceCenterConfigurationResponse getServiceCentreConfiguration(
			@RequestBody GetByIdPayload payload) throws Exception, Throwable {
		logger.info("Calling Get Service Centre Configuration - API");
		return serviceCenterService.getConfiguration(payload);
	}

	@PostMapping("/config/default/assign")
	public @ResponseBody GetServiceCenterConfigurationResponse assignDefaultServiceCentreConfiguration(
			@RequestBody GetByIdListPayload payload) throws Exception, Throwable {
		logger.info("Calling Get Service Centre Configuration Default - API");
		return serviceCenterService.assignDefaultConfiguration(payload.getIds().get(0).intValue(),
				payload.getIds().get(1));
	}

	@PostMapping("/config/update")
	public @ResponseBody GenStatusResponse updateConfig(
			@RequestBody final ServiceCenterConfiguration serviceCenterConfiguration) throws Exception, Throwable {
		logger.info("Calling Update Service Centre Configuration - API");
		return serviceCenterService.updateConfiguration(serviceCenterConfiguration);
	}

	@CrossOrigin
	@PostMapping("/updatePayment")
	public @ResponseBody UserBankCard updateConfig(@RequestBody final UpdatePaymentMethodPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Update Payment Method - API");
		return serviceCenterService.updatePaymentMethod(payload);
	}

	@CrossOrigin
	@PostMapping("/getPaymentMethods")
	public @ResponseBody List<UserBankCard> getServiceCentrePaymentMethods(
			@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Service Centre Payment Methods - API");
		return serviceCenterService.getPaymentMethods(getByIdPayload);
	}

	@CrossOrigin
	@PostMapping("/paymentMethod/add")
	public @ResponseBody GenStatusResponse addPaymentMethod(@RequestBody final AddPaymentMethodPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Add Payment Method - API");
		return serviceCenterService.addPaymentMethod(payload);
	}

	@CrossOrigin
	@PostMapping("/paymentMethod/delete")
	public @ResponseBody GenStatusResponse deletePaymentMethod(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Service - API");
		// 0 - paymentMethodId
		return serviceCenterService.deletePaymentMethod(getByIdPayload.getId());
	}

	@CrossOrigin
	@PostMapping("/getInvoices")
	public @ResponseBody List<Invoice> getServiceCentreInvoices(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Service Centre Invoices - API");
		// 0 - serviceCentreId
		return serviceCenterService.getInvoices(getByIdPayload.getId());
	}

	@CrossOrigin
	@PostMapping("/getSubscription")
	public @ResponseBody GetServiceCenterSubscriptionResponse getServiceCentreSubscription(
			@RequestBody GetByIdPayload getByIdPayload) throws Exception, Throwable {
		logger.info("Calling Get Service Centre Subscription Details - API");
		return serviceCenterService.getSubscription(getByIdPayload);
	}

	@CrossOrigin
	@PostMapping(value = "/getVotes")
	public @ResponseBody GetStatisticsResponse getVotes(@RequestBody GetActivitiesPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Service Center Votes - API");
		return serviceCenterService.getVotes(payload);
	}

	@CrossOrigin
	@PostMapping("/upgradeSubscription")
	public @ResponseBody GenStatusResponse upgradetServiceCentreSubscription(
			@RequestBody UpdateSubscriptionPlanPayload payload) throws Exception, Throwable {
		logger.info("Calling Update Service Centre Subscription Details - API");
		return serviceCenterService.upgradeSubscription(payload);
	}

	@GetMapping(value = "/getBranches")
	public @ResponseBody GetBranchListResponse getBranches() throws Exception, Throwable {
		logger.info("Calling Get Branches of Service Center - API");
		return serviceCenterService.getBranches();
	}

	@GetMapping(value = "/getListing")
	public @ResponseBody GetServiceCentreListingResponse getAllListing() throws Exception, Throwable {
		logger.info("Calling Get All Service Center Listing - API");
		return serviceCenterService.getAllListing();
	}
	
	@GetMapping(value = "/count")
	public @ResponseBody GetValuesResposne countServiceCenter() throws Exception, Throwable {
		logger.info("Calling Get Service Center Count - API");
		return serviceCenterService.countServiceCenter();
	}

	@PostMapping(value = "/bankCard/add")
	public @ResponseBody GetValuesResposne addBankCard(@RequestBody AddUserBankCardPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Add Service Center Bank Card - API");
		return serviceCenterService.addBankCard(payload);
	}

	@PostMapping(value = "/subscription/pay")
	public @ResponseBody GenStatusResponse paySubscriptionCharges(
			@RequestBody PaySubscriptionChargesPayload paySubscriptionChargesPayload) throws Exception, Throwable {
		logger.info("Calling Pay Service Center Subscription Charges - API");
		return serviceCenterService.paySubscriptionCharges(paySubscriptionChargesPayload);
	}

	@PostMapping("/dashboard/activities")
	public @ResponseBody GetValuesResposne getDashboardActivities(
			@RequestBody GetActivitiesPayload getActivitiesPayload) throws Exception, Throwable {
		logger.info("Calling Get Dashboard Activities - API");
		return serviceCenterService.getDashboardActivities(getActivitiesPayload);
	}

	@PostMapping("/dashboard/statistics")
	public @ResponseBody GetStatisticsResponse getDashboardStatistics(
			@RequestBody GetActivitiesPayload getActivitiesPayload) throws Exception, Throwable {
		logger.info("Calling Get Dashboard Statistics - API");
		return serviceCenterService.getDashboardStatistics(getActivitiesPayload);
	}

	@PostMapping("/dashboard/ratings")
	public @ResponseBody GetValuesResposne getDashboardRatings(@RequestBody GetActivitiesPayload getActivitiesPayload)
			throws Exception, Throwable {
		logger.info("Calling Get Dashboard Ratings - API");
		return serviceCenterService.getDashboardRatings(getActivitiesPayload);
	}

	@RequestMapping(value = "/supportTicket/add", method = RequestMethod.POST, consumes = {
			"multipart/form-data" }, produces = "application/json")
	public @ResponseBody GenStatusResponse addSupportTicket(
			@RequestParam(value = "AddSupportTicketPayload", required = true) @Valid final String payload,
			@RequestParam(value = "Attachements", required = false) @Valid final MultipartFile[] files)
			throws Exception, Throwable {
		logger.info("Calling Add Support Ticket - API");
		AddSupportTicketPayload addSupportTicketPayload = GenericMapper.jsonToObjectMapper(payload,
				AddSupportTicketPayload.class);
		return serviceCenterService.addSupportTicket(addSupportTicketPayload, Arrays.asList(files));
	}

	@GetMapping("/supportTicket/get")
	public @ResponseBody GetSupportTicketListResponse getSupportTickets() throws Exception, Throwable {
		logger.info("Calling Get Support Ticket - API");
		return serviceCenterService.getSupportTickets();
	}

	@PostMapping("/supportTicket/status/update")
	public @ResponseBody GenStatusResponse updateSupportTicketStatus(
			@RequestBody UpdateSupportTicketStatusPayload payload) throws Exception, Throwable {
		logger.info("Calling Update Support Ticket Status- API");
		return serviceCenterService.updateSupportTicketStatus(payload);
	}

	@PostMapping("/supportTicket/employee/assign")
	public @ResponseBody GenStatusResponse assignEmpToSupportTicket(@RequestBody GetByIdListPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Assign Emp to Support Ticket - API");
		// 0-SupportTicketId & 1-TaboorEmpId
		return serviceCenterService.assignEmpToSupportTicket(payload);
	}

	@PostMapping("/supportTicket/delete")
	public @ResponseBody GenStatusResponse deleteSupportTicket(@RequestBody GetByIdPayload getByIdPayload)
			throws Exception, Throwable {
		logger.info("Calling Delete Support Ticket - API");
		// 0 - supportTicketId
		return serviceCenterService.deleteSupportTicket(getByIdPayload.getId());
	}

	@GetMapping("/get/all")
	public @ResponseBody GetServiceCentreListResponse getAll() throws Exception, Throwable {
		logger.info("Calling Get All Service Center - API");
		return serviceCenterService.getAll();
	}

	@GetMapping("/getActiveSubscriptionList")
	public @ResponseBody GetServiceCenterSubscriptionListResponse getSubscriptionList() throws Exception, Throwable {
		logger.info("Calling Get Active Service Center Subscription List - API");
		return serviceCenterService.getActiveSubscriptionList();
	}
}
