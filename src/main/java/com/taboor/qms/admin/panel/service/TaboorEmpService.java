package com.taboor.qms.admin.panel.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;

import com.taboor.qms.admin.panel.payload.RegisterTaboorEmpPayload;
import com.taboor.qms.admin.panel.response.GetEmployeeProfileResponse;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.TaboorEmployee;
import com.taboor.qms.core.response.GetTaboorEmployeeListResponse;
import com.taboor.qms.core.response.GetValuesResposne;

@Service
public interface TaboorEmpService {

	TaboorEmployee saveTaboorEmp(TaboorEmployee taboorEmployee) throws TaboorQMSServiceException;

	TaboorEmployee findByUserId(Long userId) throws TaboorQMSServiceException;

	GetEmployeeProfileResponse getProfile() throws TaboorQMSServiceException, Exception;

	GetValuesResposne registerEmp(RegisterTaboorEmpPayload registerTaboorEmpPayload, MultipartFile profileImage)
			throws TaboorQMSServiceException, Exception;

	GetValuesResposne getNextEmpNumber() throws TaboorQMSServiceException, Exception;

	String nextEmpNumber() throws RestClientException, TaboorQMSServiceException;

	GetTaboorEmployeeListResponse getListing() throws TaboorQMSServiceException, Exception;

	GetTaboorEmployeeListResponse getList() throws TaboorQMSServiceException, Exception;

	GetValuesResposne updateEmp(RegisterTaboorEmpPayload registerTaboorEmpPayload)
			throws TaboorQMSServiceException, Exception;

}
